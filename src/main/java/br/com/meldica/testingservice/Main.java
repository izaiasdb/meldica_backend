package br.com.meldica.testingservice;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

//@SpringBootApplication
public class Main implements CommandLineRunner {
	
	private @Autowired ApplicationContextProvider provider;
	
	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);		
	}

	@Override
	public void run(String... args) throws Exception {
		long start = System.currentTimeMillis();
		ApplicationContext context = provider.getContext();
		
		try {
//			ResponseEntity<byte[]> imprimir = context.getBean(ConfereController.class).imprimir(null);
//			System.out.println(imprimir.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			((ConfigurableApplicationContext) context).close();
		}
		
		long end = System.currentTimeMillis();
		System.out.println(trace(start, end));
	}
	
	public String trace(long start, long end) {
		long diff = (end - start);
		
		long hours   = TimeUnit.MILLISECONDS.toHours(diff);
		long minutes = TimeUnit.MILLISECONDS.toMinutes((diff - TimeUnit.HOURS.toMillis(hours)));
		long seconds = TimeUnit.MILLISECONDS.toSeconds( (diff - (TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes))) );
		long milliseconds = diff - (TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes) + TimeUnit.SECONDS.toMillis(seconds));
				
		return String.format("Estimated time: %dh %dmin %dsecs %dms", hours, minutes, seconds, milliseconds);
	}

}
