package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TipoNotaEnum {
	ABERTA("A","ABERTA"),
	PAGA("P","PAGA"),
	CANCELADA("C","CANCELADA"),
	;

	private String id;
	private String nome;
	private static final Map<String, TipoNotaEnum> lookup = new HashMap<String, TipoNotaEnum>();
	private static final Map<String, TipoNotaEnum> lookup2 = new HashMap<String, TipoNotaEnum>();
	
	static {
        for (TipoNotaEnum d : TipoNotaEnum.values())
            lookup.put(d.getId(), d);
        
        for (TipoNotaEnum d : TipoNotaEnum.values())
        	lookup2.put(d.getDescricao(), d);              
    }

	private TipoNotaEnum(String id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getDescricao() {
		return nome;
	}
	
	public static TipoNotaEnum obterPorId(String id) {
		return lookup.get(id);
	}
	
	public static TipoNotaEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<TipoNotaEnum> listar() {
		return new ArrayList<TipoNotaEnum>(EnumSet.allOf(TipoNotaEnum.class));
	}
	
	/*
	public static ValueObject obterValueObjectPorId(Integer id) {
		PfPjEnum tipoEnum = lookup.get(id);
		return obterValueObject(tipoEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		PfPjEnum tipoEnum = lookup2.get(nome);
		return obterValueObject(tipoEnum);
	}
	
	public static ValueObject obterValueObject(PfPjEnum tipoEnum) {
		if (tipoEnum != null)
			return new ValueObject(tipoEnum.id, tipoEnum.nome);
		
		return null;
	}	
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}*/	
	
	@Override
	public String toString() {
		return nome;
	}

}