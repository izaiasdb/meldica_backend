package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.meldica.model.dto.utils.ValueObject;

public enum TipoEnderecoEnum {
	RESIDENCIAL(1,"RESIDENCIAL"),
	COMERCIAL(2,"COMERCIAL");
	;

	private Integer id;
	private String nome;
	private static final Map<Integer, TipoEnderecoEnum> lookup = new HashMap<Integer, TipoEnderecoEnum>();
	private static final Map<String, TipoEnderecoEnum> lookup2 = new HashMap<String, TipoEnderecoEnum>();
	
	static {
        for (TipoEnderecoEnum d : TipoEnderecoEnum.values())
            lookup.put(d.getId(), d);
        
        for (TipoEnderecoEnum d : TipoEnderecoEnum.values())
        	lookup2.put(d.getNome(), d);          
    }

	private TipoEnderecoEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public static TipoEnderecoEnum obterPorId(Integer id) {
		return lookup.get(id);
	}
	
	public static TipoEnderecoEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<TipoEnderecoEnum> listar() {
		return new ArrayList<TipoEnderecoEnum>(EnumSet.allOf(TipoEnderecoEnum.class));
	}
	
	public static ValueObject obterValueObjectPorId(Integer id) {
		TipoEnderecoEnum tipoEnum = lookup.get(id);
		return obterValueObject(tipoEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		TipoEnderecoEnum tipoEnum = lookup2.get(nome);
		return obterValueObject(tipoEnum);
	}
	
	public static ValueObject obterValueObject(TipoEnderecoEnum tipoEnum) {
		if (tipoEnum != null)
			return new ValueObject(tipoEnum.id, tipoEnum.nome);
		
		return null;
	}	
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}	
	
	@Override
	public String toString() {
		return nome;
	}

}