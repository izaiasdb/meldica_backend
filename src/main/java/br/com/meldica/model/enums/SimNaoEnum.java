package br.com.meldica.model.enums;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum SimNaoEnum {
	S("Sim"),
	N("Não");

	private static Map<String, SimNaoEnum> FORMAT_MAP = Stream
		        .of(SimNaoEnum.values())
		        .collect(Collectors.toMap(s -> s.toString(), Function.identity()));

	private String descricao;

	SimNaoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@JsonCreator
    public static SimNaoEnum fromString(String string) {
        return Optional.ofNullable(FORMAT_MAP.get(string)).orElse(null);
    }
}
