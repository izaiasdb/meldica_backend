package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.meldica.model.dto.utils.ValueObject;

public enum TipoPessoaEnum {
	CLIENTE(1,"CLIENTE","CLI"),
	FUNCIONARIO(2,"FUNCIONÁRIO","FUN"),
	FORNECEDOR(3,"FORNECEDOR","FOR"),
	;

	private Integer id;
	private String nome;
	private String abreviacao;
	private static final Map<Integer, TipoPessoaEnum> lookup = new HashMap<Integer, TipoPessoaEnum>();
	private static final Map<String, TipoPessoaEnum> lookup2 = new HashMap<String, TipoPessoaEnum>();
	
	static {
        for (TipoPessoaEnum d : TipoPessoaEnum.values())
            lookup.put(d.getId(), d);

        for (TipoPessoaEnum d : TipoPessoaEnum.values())
        	lookup2.put(d.getNome(), d);        
    }

	private TipoPessoaEnum(Integer id, String nome, String abreviacao) {
		this.id = id;
		this.nome = nome;
		this.abreviacao = abreviacao;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getAbreviacao() {
		return abreviacao;
	}
	
	public static TipoPessoaEnum obterPorId(int id) {
		return lookup.get(id);
	}
	
	public static TipoPessoaEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static ValueObject obterValueObjectPorId(int id) {
		TipoPessoaEnum tipoPessoaEnum = lookup.get(id);
		return obterValueObject(tipoPessoaEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		TipoPessoaEnum tipoPessoaEnum = lookup2.get(nome);
		return obterValueObject(tipoPessoaEnum);
	}
	
	public static ValueObject obterValueObject(TipoPessoaEnum tipoPessoaEnum) {
		if (tipoPessoaEnum != null)
			return new ValueObject(tipoPessoaEnum.id, tipoPessoaEnum.nome);
		
		return null;
	}	
	
	public static List<TipoPessoaEnum> listar() {
		return new ArrayList<TipoPessoaEnum>(EnumSet.allOf(TipoPessoaEnum.class));
	}
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}	
	
	@Override
	public String toString() {
		return nome;
	}

}