package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TipoProdutoEnum {
	PRODUTO("P","PRODUTO"),
	INSUMO("I","INSUMO"),	
	COMBINADO("C","COMBINADO");
	;

	private String id;
	private String nome;
	private static final Map<String, TipoProdutoEnum> lookup = new HashMap<String, TipoProdutoEnum>();
	
	static {
        for (TipoProdutoEnum d : TipoProdutoEnum.values())
            lookup.put(d.getId(), d);
    }

	private TipoProdutoEnum(String id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public static TipoProdutoEnum obterPorId(String id) {
		return lookup.get(id);
	}
	
	public static List<TipoProdutoEnum> listar() {
		return new ArrayList<TipoProdutoEnum>(EnumSet.allOf(TipoProdutoEnum.class));
	}
	
//	public static List<ValueObject> listarValueObject() {
//		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
//		
//		listar().forEach(c-> {
//			listValueObject.add(new ValueObject(c.id, c.nome));
//		});
//		
//		return listValueObject;
//	}	
	
	@Override
	public String toString() {
		return nome;
	}

}