package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ReceitaDespesaEnum {
	RECEITA("R","RECEITA"),
	DESPESA("D","DESPESA");
	;

	private String id;
	private String nome;
	private static final Map<String, ReceitaDespesaEnum> lookup = new HashMap<String, ReceitaDespesaEnum>();
	private static final Map<String, ReceitaDespesaEnum> lookup2 = new HashMap<String, ReceitaDespesaEnum>();
	
	static {
        for (ReceitaDespesaEnum d : ReceitaDespesaEnum.values())
            lookup.put(d.getId(), d);
        
        for (ReceitaDespesaEnum d : ReceitaDespesaEnum.values())
        	lookup2.put(d.getDescricao(), d);              
    }

	private ReceitaDespesaEnum(String id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getDescricao() {
		return nome;
	}
	
	public static ReceitaDespesaEnum obterPorId(String id) {
		return lookup.get(id);
	}
	
	public static ReceitaDespesaEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<ReceitaDespesaEnum> listar() {
		return new ArrayList<ReceitaDespesaEnum>(EnumSet.allOf(ReceitaDespesaEnum.class));
	}
	
	/*
	public static ValueObject obterValueObjectPorId(Integer id) {
		PfPjEnum tipoEnum = lookup.get(id);
		return obterValueObject(tipoEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		PfPjEnum tipoEnum = lookup2.get(nome);
		return obterValueObject(tipoEnum);
	}
	
	public static ValueObject obterValueObject(PfPjEnum tipoEnum) {
		if (tipoEnum != null)
			return new ValueObject(tipoEnum.id, tipoEnum.nome);
		
		return null;
	}	
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}*/	
	
	@Override
	public String toString() {
		return nome;
	}

}