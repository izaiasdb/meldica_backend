package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.meldica.model.dto.utils.ValueObject;

public enum MenuPermissaoEnum {
	INSERIR(1, "INSERIR"),
	ALTERAR(2, "ALTERAR"),
	EXCLUIR(4, "EXCLUIR"),
	CONSULTAR(8, "CONSULTAR"),
	IMPRIMIR(16, "IMPRIMIR"),
//	AUTORIZAR(32, "AUTORIZAR"),
	;

	private Integer id;
	private String nome;
	private static final Map<Integer, MenuPermissaoEnum> lookup = new HashMap<Integer, MenuPermissaoEnum>();
	
	static {
        for (MenuPermissaoEnum d : MenuPermissaoEnum.values())
            lookup.put(d.getId(), d);
    }

	private MenuPermissaoEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public static MenuPermissaoEnum obterPorId(int id) {
		return lookup.get(id);
	}
	
	public static List<MenuPermissaoEnum> listar() {
		return new ArrayList<MenuPermissaoEnum>(EnumSet.allOf(MenuPermissaoEnum.class));
	}
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}	
	
	public static List<Long> listarIdsPorSomaPermissao(Integer somaPermissao) {
		if (somaPermissao != null && somaPermissao > 0) {
			List<MenuPermissaoEnum> list = listarPorSomaPermissao(somaPermissao);
			List<Integer> ids = list.stream().map(MenuPermissaoEnum::getId).collect(Collectors.toList());
			return ids.stream().mapToLong(Integer::longValue).boxed().collect(Collectors.toList());
//			int sum = list.stream().mapToInt(o -> o.getId()).sum();
		}
		else {
			return new ArrayList<Long>();
		}		
	}
	
	public static List<MenuPermissaoEnum> listarPorSomaPermissao(Integer valor) {
		List<MenuPermissaoEnum> list = new ArrayList<MenuPermissaoEnum>();
		List<Integer> multiplos;
		Integer multiplo = valor;

		while (multiplo >= 1) {
			if (multiplo == 1) {
				list.add(obterPorId(multiplo));
				break;
			} else {
				multiplos = multiplosValor(multiplo);
				Integer max = Collections.max(multiplos);
				list.add(obterPorId(max));
				multiplo = multiplo - max;
			}
		}
		
		if (list.size() > 0) {
			list.sort(Comparator.comparing(MenuPermissaoEnum::getId));
		}

		return list;
	}
	
	private static List<Integer> multiplosValor(Integer valor) {
		List<Integer> multiplos = new ArrayList<Integer>();

		Integer multiplo = 1;

		while (multiplo <= valor) {
			if ((multiplo == 1) | (multiplo % 2 == 0)) {
				multiplos.add(multiplo);
				multiplo = 2 * multiplo;
			}
		}

		return multiplos;
	}
	
	public static void main(String[] args) {
		//listarPorSomaPermissao(17).forEach(System.out::println);
		listarPorSomaPermissao(31).forEach(System.out::println);
	}

	@Override
	public String toString() {
		return nome;
	}

}