package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.meldica.model.dto.utils.ValueObject;

public enum TipoTelefoneEnum {
	FIXO(1,"FIXO"),
	CELULAR(2,"CELULAR");
	;

	private Integer id;
	private String nome;
	private static final Map<Integer, TipoTelefoneEnum> lookup = new HashMap<Integer, TipoTelefoneEnum>();
	private static final Map<String, TipoTelefoneEnum> lookup2 = new HashMap<String, TipoTelefoneEnum>();
	
	static {
        for (TipoTelefoneEnum d : TipoTelefoneEnum.values())
            lookup.put(d.getId(), d);
        
        for (TipoTelefoneEnum d : TipoTelefoneEnum.values())
        	lookup2.put(d.getNome(), d);              
    }

	private TipoTelefoneEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public static TipoTelefoneEnum obterPorId(Integer id) {
		return lookup.get(id);
	}
	
	public static TipoTelefoneEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<TipoTelefoneEnum> listar() {
		return new ArrayList<TipoTelefoneEnum>(EnumSet.allOf(TipoTelefoneEnum.class));
	}
	
	public static ValueObject obterValueObjectPorId(Integer id) {
		TipoTelefoneEnum tipoEnum = lookup.get(id);
		return obterValueObject(tipoEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		TipoTelefoneEnum tipoEnum = lookup2.get(nome);
		return obterValueObject(tipoEnum);
	}
	
	public static ValueObject obterValueObject(TipoTelefoneEnum tipoEnum) {
		if (tipoEnum != null)
			return new ValueObject(tipoEnum.id, tipoEnum.nome);
		
		return null;
	}	
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}	
	
	@Override
	public String toString() {
		return nome;
	}

}