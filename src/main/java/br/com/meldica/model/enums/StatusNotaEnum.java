package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Status do Pedido
public enum StatusNotaEnum {
	//Disponível para o vendedor
	ABERTO("A", "ABERTO"),
	//Concluída pelo vendedor e disponível para a pessoa da lógistica aprovar
	LOGISTICA("L", "LOGÍSTICA"), 
	//Aprovada pela logística, temm que ser separada para entregar e gerado as forma pgto
	FECHADO("F", "FECHADO"),
	//Disponível para o vendedor
	REABERTO("R", "REABERTO"),
	//Aprovada pelo gerente, onde da baixa nos produtos, a mercadoria está em rota, 
	LIBERADO("T", "LIBERADO"), //TRANSPORTE
	ENTREGUE("E", "ENTREGUE"),
	/*
	 * Desfaz tudo, retorna quantidade de produtos e desfaz pagamentos. Qualquer coisa a pessoa faz outra nota.
	 * Se tiver paga não pode ser cancelada, só se estiver parcialmente paga. 
	 */
	CANCELADO("C", "CANCELADO"), 
	GRATIS("G", "GRÁTIS")
	;
	//Concluída pelo funcionário e disponível para o gerente aprovar
//	CONCLUIDA("N", "CONCLUÍDA"), 
	//Aprovada pelo gerente, temm que ser separada para entregar e gerado as forma pgto
//	APROVADA("O", "APROVADA"),
	//Etapa final, quando a nota é paga pelo cliente
//	PAGA("P", "PAGA"), 
	;

	private String id;
	private String nome;
	private static final Map<String, StatusNotaEnum> lookup = new HashMap<String, StatusNotaEnum>();
	private static final Map<String, StatusNotaEnum> lookup2 = new HashMap<String, StatusNotaEnum>();
	
	static {
        for (StatusNotaEnum d : StatusNotaEnum.values())
            lookup.put(d.getId(), d);
        
        for (StatusNotaEnum d : StatusNotaEnum.values())
        	lookup2.put(d.getDescricao(), d);              
    }

	private StatusNotaEnum(String id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getDescricao() {
		return nome;
	}
	
	public static StatusNotaEnum obterPorId(String id) {
		return lookup.get(id);
	}
	
	public static StatusNotaEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<StatusNotaEnum> listar() {
		return new ArrayList<StatusNotaEnum>(EnumSet.allOf(StatusNotaEnum.class));
	}
	
	@Override
	public String toString() {
		return nome;
	}

}