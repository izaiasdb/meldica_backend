package br.com.meldica.model.enums;
public enum ExtensaoEnum {
	
	PDF(1, "pdf"), XLS(2, "xlsx");
	
	private Integer id;
	private String descricao;
	
	private ExtensaoEnum(Integer id, String descricao) {
		this.descricao = descricao;
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getDescricao() {
		return descricao;
	}
}