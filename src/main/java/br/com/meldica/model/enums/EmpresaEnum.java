package br.com.meldica.model.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.meldica.model.dto.utils.ValueObject;

public enum EmpresaEnum {
	NATURAIS(1,"NATURAIS"),
	COSMETICOS(2,"COSMETICOS"),
	ENCAPSULADOS(3,"ENCAPSULADOS"),
	;

	private Integer id;
	private String nome;
	private static final Map<Integer, EmpresaEnum> lookup = new HashMap<Integer, EmpresaEnum>();
	private static final Map<String, EmpresaEnum> lookup2 = new HashMap<String, EmpresaEnum>();
	
	static {
        for (EmpresaEnum d : EmpresaEnum.values())
            lookup.put(d.getId(), d);
        
        for (EmpresaEnum d : EmpresaEnum.values())
        	lookup2.put(d.getNome(), d);              
    }

	private EmpresaEnum(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return this.name();
	}
	
	public String getNome() {
		return nome;
	}
	
	public static EmpresaEnum obterPorId(Integer id) {
		return lookup.get(id);
	}
	
	public static EmpresaEnum obterPorNome(String nome) {
		return lookup2.get(nome);
	}
	
	public static List<EmpresaEnum> listar() {
		return new ArrayList<EmpresaEnum>(EnumSet.allOf(EmpresaEnum.class));
	}
	
	public static ValueObject obterValueObjectPorId(Integer id) {
		EmpresaEnum tipoEnum = lookup.get(id);
		return obterValueObject(tipoEnum); 
	}
	
	public static ValueObject obterValueObjectPorNome(String nome) {
		EmpresaEnum tipoEnum = lookup2.get(nome);
		return obterValueObject(tipoEnum);
	}
	
	public static ValueObject obterValueObject(EmpresaEnum tipoEnum) {
		if (tipoEnum != null)
			return new ValueObject(tipoEnum.id, tipoEnum.nome);
		
		return null;
	}	
	
	public static List<ValueObject> listarValueObject() {
		List<ValueObject> listValueObject = new ArrayList<ValueObject>();
		
		listar().forEach(c-> {
			listValueObject.add(new ValueObject(c.id, c.nome));
		});
		
		return listValueObject;
	}	
	
	@Override
	public String toString() {
		return nome;
	}

}