package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ProducaoProdutoEntity;

@Repository
public interface ProducaoProdutoDao extends JpaRepository<ProducaoProdutoEntity, Long> {
	@Query("select ep from ProducaoProdutoEntity ep where ep.idProducao = :idProducao")
	List<ProducaoProdutoEntity> listarPorProducao(@Param("idProducao") Integer idProducao);
}
