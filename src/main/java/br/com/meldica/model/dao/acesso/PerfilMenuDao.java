package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.PerfilMenuEntity;

@Repository
public interface PerfilMenuDao extends JpaRepository<PerfilMenuEntity, Integer>{
	@Query(" select m from PerfilMenuEntity m where 1 = 1 and idPerfil = :idPerfil ")
	List<PerfilMenuEntity> listarPorPerfil(@Param("idPerfil") Integer idPerfil);
	
	@Query(" select m from PerfilMenuEntity m join m.menu me join me.modulo mo " +
			" where 1 = 1 and m.idPerfil = :idPerfil and mo.id = :idModulo ")
	List<PerfilMenuEntity> listarPorPerfilModulo(@Param("idPerfil") Integer idPerfil, @Param("idModulo") Integer idModulo);	
}
