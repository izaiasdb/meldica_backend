package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FornecedorEnderecoEntity;

@Repository
public interface FornecedorEnderecoDao extends JpaRepository<FornecedorEnderecoEntity, Long> {
	@Query("select ep from FornecedorEnderecoEntity ep where ep.idFornecedor = :idFornecedor")
	List<FornecedorEnderecoEntity> listarPorFornecedor(@Param("idFornecedor") Integer idFornecedor);
}
