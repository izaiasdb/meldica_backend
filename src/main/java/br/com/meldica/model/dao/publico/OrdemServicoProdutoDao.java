package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;

@Repository
public interface OrdemServicoProdutoDao extends JpaRepository<OrdemServicoProdutoEntity, Long> {
	@Query("select ep from OrdemServicoProdutoEntity ep where ep.idOrdemServico = :idOrdemServico")
	List<OrdemServicoProdutoEntity> listarPorOrdemServico(@Param("idOrdemServico") Integer idOrdemServico);
}
