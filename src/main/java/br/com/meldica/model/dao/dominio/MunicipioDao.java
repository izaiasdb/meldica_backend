package br.com.meldica.model.dao.dominio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.dominio.MunicipioEntity;

@Repository
public interface MunicipioDao extends JpaRepository<MunicipioEntity, Integer>{
	List<MunicipioEntity> findByNomeLike(String nome);
}
