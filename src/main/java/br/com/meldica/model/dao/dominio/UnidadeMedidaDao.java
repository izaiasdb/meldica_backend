package br.com.meldica.model.dao.dominio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.dominio.UnidadeMedidaEntity;

@Repository
public interface UnidadeMedidaDao extends JpaRepository<UnidadeMedidaEntity, Integer>{
	@Query("from UnidadeMedidaEntity order by nome ")
	public List<UnidadeMedidaEntity> listar();
	
	@Query("select s from UnidadeMedidaEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by nome ")
	List<UnidadeMedidaEntity> listar(@Param("nome") String nome);
	
	@Query("select t from UnidadeMedidaEntity t where t.id = :id")
	public UnidadeMedidaEntity obter(@Param("id") Integer id);
}
