package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FormaPagamentoEntity;

@Repository
public interface FormaPagamentoDao extends JpaRepository<FormaPagamentoEntity, Integer>{
	@Query("from FormaPagamentoEntity where cancelado = false order by nome ")
	public List<FormaPagamentoEntity> listar();
	
	@Query("from FormaPagamentoEntity where cancelado = false and ativo = true order by nome ")
	public List<FormaPagamentoEntity> listarAtivos();
	
	
	@Query("select s from FormaPagamentoEntity s where cancelado = false and upper(s.nome) like '%'|| upper(:nome) || '%' order by nome ")
	List<FormaPagamentoEntity> listar(@Param("nome") String nome);
	
	@Query("select t from FormaPagamentoEntity t where t.id = :id")
	public FormaPagamentoEntity obter(@Param("id") Integer id);
	
	@Modifying
	@Query("update FormaPagamentoEntity c set c.cancelado = :cancelado where c.id = :id")
	void cancelar(@Param("id") Integer id, @Param("cancelado") Boolean cancelado);
}
