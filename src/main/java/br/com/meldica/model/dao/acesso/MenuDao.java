package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.MenuEntity;

@Repository
public interface MenuDao extends JpaRepository<MenuEntity, Integer>{
	@Query("select m from MenuEntity m where upper(m.nome) like '%'|| upper(:nome) || '%'")
	List<MenuEntity> listar(@Param("nome") String nome);
	
	@Query(" select m from MenuEntity m where 1 = 1"
		+ " and (:#{#nome} is null or normalizar_string(m.nome) like '%' || normalizar_string(:#{#nome ?: ''}) || '%') "
		+ " and (:#{#idModulo} is null or m.modulo.id = :#{#idModulo}) "
		)
	List<MenuEntity> listarPorNomeModulo(@Param("idModulo") Integer idModulo, @Param("nome") String nome);
	
	@Query(" select m from MenuEntity m where 1 = 1 and m.modulo.id = :idModulo order by nivel, menu.id, ordem ")
	List<MenuEntity> listarPorModulo(@Param("idModulo") Integer idModulo);	
	
	@Query("select m from MenuEntity m where m.nivel =:nivel")
	List<MenuEntity> listarPorNivel(@Param("nivel") Integer nivel);	
}
