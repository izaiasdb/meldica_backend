package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FornecedorEntity;

@Repository
public interface FornecedorDao extends JpaRepository<FornecedorEntity, Integer>{
	@Query("select s from FornecedorEntity s order by s.nome ")
	List<FornecedorEntity> listar();
	
	@Query("select s from FornecedorEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<FornecedorEntity> listar(@Param("nome") String nome);
	
}
