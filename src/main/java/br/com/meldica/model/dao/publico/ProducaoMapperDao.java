package br.com.meldica.model.dao.publico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ProducaoEntity;

@Repository
public class ProducaoMapperDao {
	
	private final SqlSession sqlSession;

	public ProducaoMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}
	
	public List<ProducaoEntity> listar(ProducaoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<ProducaoEntity>();
		}
		
		return sqlSession.selectList("ProducaoMapper.listar", filtro);
	}
	
}
