package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.PagarReceberItemEntity;

@Repository
public interface PagarReceberItemDao extends JpaRepository<PagarReceberItemEntity, Long> {
	@Query("select ep from PagarReceberItemEntity ep where ep.idPagarReceber = :idPagarReceber")
	List<PagarReceberItemEntity> listarPorPagarReceber(@Param("idPagarReceber") Integer idPagarReceber);
}
