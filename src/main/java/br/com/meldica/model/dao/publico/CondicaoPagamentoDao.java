package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.CondicaoPagamentoEntity;

@Repository
public interface CondicaoPagamentoDao extends JpaRepository<CondicaoPagamentoEntity, Integer>{
	@Query("from CondicaoPagamentoEntity where cancelado = false order by nome ")
	public List<CondicaoPagamentoEntity> listar();
	
	@Query("from CondicaoPagamentoEntity where ativo = true and cancelado = false order by nome ")
	public List<CondicaoPagamentoEntity> listarAtivos();
	
	
	@Query("select s from CondicaoPagamentoEntity s where s.cancelado = false and upper(s.nome) like '%'|| upper(:nome) || '%' order by nome ")
	List<CondicaoPagamentoEntity> listar(@Param("nome") String nome);
	
	@Query("select t from CondicaoPagamentoEntity t where t.id = :id")
	public CondicaoPagamentoEntity obter(@Param("id") Integer id);
	
	@Modifying
	@Query("update CondicaoPagamentoEntity c set c.cancelado = :cancelado where c.id = :id")
	void cancelar(@Param("id") Integer id, @Param("cancelado") Boolean cancelado);
}
