package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ClienteEnderecoEntity;

@Repository
public interface ClienteEnderecoDao extends JpaRepository<ClienteEnderecoEntity, Long> {
	@Query("select ep from ClienteEnderecoEntity ep where ep.idCliente = :idCliente")
	List<ClienteEnderecoEntity> listarPorCliente(@Param("idCliente") Integer idCliente);
}
