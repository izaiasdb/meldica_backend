package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.TabelaPrecoProdutoEntity;

@Repository
public interface TabelaPrecoProdutoDao extends JpaRepository<TabelaPrecoProdutoEntity, Integer>{
	@Query(" select m from TabelaPrecoProdutoEntity m where 1 = 1"
			+ " and (:#{#idTabelaPreco} is null or m.idTabelaPreco = :#{#idTabelaPreco}) "
			)
	List<TabelaPrecoProdutoEntity> listarPorTabelaPreco (@Param("idTabelaPreco") Integer idTabelaPreco);	

}
