package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FuncionarioTelefoneEntity;

@Repository
public interface FuncionarioTelefoneDao extends JpaRepository<FuncionarioTelefoneEntity, Long> {
	@Query("select ep from FuncionarioTelefoneEntity ep where ep.idFuncionario = :idFuncionario")
	List<FuncionarioTelefoneEntity> listarPorFuncionario(@Param("idFuncionario") Integer idFuncionario);
}
