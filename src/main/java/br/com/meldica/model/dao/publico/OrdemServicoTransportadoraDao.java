package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoTransportadoraEntity;

@Repository
public interface OrdemServicoTransportadoraDao extends JpaRepository<OrdemServicoTransportadoraEntity, Long> {
	@Query("select ep from OrdemServicoTransportadoraEntity ep where ep.idOrdemServico = :idOrdemServico")
	List<OrdemServicoTransportadoraEntity> listarPorOrdemServico(@Param("idOrdemServico") Integer idOrdemServico);
}
