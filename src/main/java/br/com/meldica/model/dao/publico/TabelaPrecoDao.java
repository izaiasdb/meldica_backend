package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.TabelaPrecoEntity;

@Repository
public interface TabelaPrecoDao extends JpaRepository<TabelaPrecoEntity, Integer>{
	@Query("select s from TabelaPrecoEntity s order by s.nome ")
	List<TabelaPrecoEntity> listar();
	
	@Query("select s from TabelaPrecoEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<TabelaPrecoEntity> listar(@Param("nome") String nome);
	
	@Query("select s from TabelaPrecoEntity s where s.ativo = true order by s.nome ")
	List<TabelaPrecoEntity> listarAtivos();
	
}
