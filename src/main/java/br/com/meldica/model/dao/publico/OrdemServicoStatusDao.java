package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoStatusEntity;

@Repository
public interface OrdemServicoStatusDao extends JpaRepository<OrdemServicoStatusEntity, Long> {
	@Query("select ep from OrdemServicoStatusEntity ep where ep.idOrdemServico = :idOrdemServico")
	List<OrdemServicoStatusEntity> listarPorOrdemServico(@Param("idOrdemServico") Integer idOrdemServico);
}
