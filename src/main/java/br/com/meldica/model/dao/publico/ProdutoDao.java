package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ProdutoEntity;

@Repository
public interface ProdutoDao extends JpaRepository<ProdutoEntity, Integer>{
	@Query("select s from ProdutoEntity s order by s.nome ")
	List<ProdutoEntity> listar();
	
	@Query("select s from ProdutoEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<ProdutoEntity> listar(@Param("nome") String nome);
	
	@Query(" select m from ProdutoEntity m where 1 = 1"
		//+ " and (:#{#nome} is null or normalizar_string(m.nome) like '%' || normalizar_string(:#{#nome ?: ''}) || '%') "
		+ " and (:#{#nome} is null or m.nome like '%' || :#{#nome ?: ''} || '%') "
		+ " and (:#{#tipo} is null or m.tipo = :#{#tipo}) "
		+ "  and (:#{#ativo} is null or m.ativo = :#{#ativo}) "
		+ "  order by m.nome "
		)
	List<ProdutoEntity> listar(@Param("tipo") String tipo, @Param("nome") String nome, @Param("ativo") Boolean ativo);

}
