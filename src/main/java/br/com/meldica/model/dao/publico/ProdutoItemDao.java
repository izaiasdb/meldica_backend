package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ProdutoItemEntity;

@Repository
public interface ProdutoItemDao extends JpaRepository<ProdutoItemEntity, Integer>{
	@Query(" select m from ProdutoItemEntity m where 1 = 1"
			+ " and (:#{#idProduto} is null or m.idProdutoPai = :#{#idProduto}) "
			)
	List<ProdutoItemEntity> listarPorProduto (@Param("idProduto") Integer idProduto);	

}
