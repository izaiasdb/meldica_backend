package br.com.meldica.model.dao.dominio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.dominio.CargoEntity;

@Repository
public interface CargoDao extends JpaRepository<CargoEntity, Integer>{
	@Query("from CargoEntity order by nome ")
	public List<CargoEntity> listar();
	
	@Query("select s from CargoEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by nome ")
	List<CargoEntity> listar(@Param("nome") String nome);
	
	@Query("select t from CargoEntity t where t.id = :id")
	public CargoEntity obter(@Param("id") Integer id);
}
