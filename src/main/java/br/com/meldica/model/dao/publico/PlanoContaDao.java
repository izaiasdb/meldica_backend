package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.PlanoContaEntity;

@Repository
public interface PlanoContaDao extends JpaRepository<PlanoContaEntity, Integer>{
	@Query("select m from PlanoContaEntity m where m.id = :id ")
	PlanoContaEntity obter(@Param("id") Integer id);
	
//	@Query("select m from PlanoContaEntity m where upper(m.nome) like '%'|| upper(:nome) || '%' order by numeroConta ")
//	List<PlanoContaEntity> listar(@Param("nome") String nome);
	
	@Query("select p from PlanoContaEntity p where 1 = 1 "
	//+ "  and (:#{#filtro.nome} is null or normalizar_string(p.nome) like '%' || normalizar_string(:#{#filtro.nome ?: ''}) || '%') "
	+ "  and (:#{#filtro.nome} is null or p.nome like '%' || :#{#filtro.nome ?: ''} || '%') "
	+ "  and ((:#{#filtro.receitaDespesa} is null or :#{#filtro.receitaDespesa} = '') or p.receitaDespesa = :#{#filtro.receitaDespesa}) "
	+ "  order by numeroConta " )
	List<PlanoContaEntity> listar(@Param("filtro") PlanoContaEntity filtro);
	
	@Query("select m from PlanoContaEntity m where m.nivel =:nivel  order by numeroConta")
	List<PlanoContaEntity> listarPorNivel(@Param("nivel") Integer nivel);
	
	@Query("select m from PlanoContaEntity m where m.nivel =:nivel and planoContaPai.id = :idPlanoContaPai order by numeroConta ")
	List<PlanoContaEntity> listarPorNivel(@Param("nivel") Integer nivel, @Param("idPlanoContaPai") Integer idPlanoContaPai);	
}
