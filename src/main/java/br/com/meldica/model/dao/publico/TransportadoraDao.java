package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.TransportadoraEntity;

@Repository
public interface TransportadoraDao extends JpaRepository<TransportadoraEntity, Integer>{
	
	@Query("select s from TransportadoraEntity s where id = :id")
	TransportadoraEntity obter(@Param("id") Integer id);	
	
	@Query("select s from TransportadoraEntity s order by s.nome ")
	List<TransportadoraEntity> listar();
	
	@Query("select s from TransportadoraEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<TransportadoraEntity> listar(@Param("nome") String nome);
	
}
