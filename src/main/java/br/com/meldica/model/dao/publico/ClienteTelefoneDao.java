package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ClienteTelefoneEntity;

@Repository
public interface ClienteTelefoneDao extends JpaRepository<ClienteTelefoneEntity, Long> {
	@Query("select ep from ClienteTelefoneEntity ep where ep.idCliente = :idCliente")
	List<ClienteTelefoneEntity> listarPorCliente(@Param("idCliente") Integer idCliente);
}
