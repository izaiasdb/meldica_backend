package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.EmpresaEntity;

@Repository
public interface EmpresaDao extends JpaRepository<EmpresaEntity, Integer>{
	@Query("select s from EmpresaEntity s order by s.nome ")
	List<EmpresaEntity> listar();
	
	@Query("select s from EmpresaEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<EmpresaEntity> listar(@Param("nome") String nome);
	
}
