package br.com.meldica.model.dao.acesso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.enums.MenuPermissaoEnum;

@Repository
public class UsuarioMenuMapperDao {
	
	private final SqlSession sqlSession;

	public UsuarioMenuMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}

	public List<String> litarPermissoesUsuario(FiltroUsuarioMenuDto usuarioMenu) {
		if (Objects.isNull(usuarioMenu) || Objects.isNull(usuarioMenu.getIdUsuario())) {
			return new ArrayList<String>();
		}
		
		List<String> permissoes = new ArrayList<String>();
		List<FiltroUsuarioMenuDto> usuarioMenuList = sqlSession.selectList("UsuarioMenuMapper.litarPermissoesUsuario", usuarioMenu);
		
		usuarioMenuList.forEach(menu -> {
			MenuPermissaoEnum.listarPorSomaPermissao(menu.getSomaPermissao()).forEach(menuPermissao ->{
				permissoes.add(menu.getPermissao() + "_" + menuPermissao.getNome());
			});
		});		
		
		return permissoes;
	}

}
