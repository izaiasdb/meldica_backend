package br.com.meldica.model.dao.utils;

import java.util.List;

import org.hibernate.Criteria;

public interface GenericDAO<E, K> {	
	E save(E entity) throws Exception;
	
	E update(E entity) throws Exception;
	
	void delete(E entity) throws Exception;

	E find(K key) throws Exception;
	
	List<E> list() throws Exception;
	
	List<E> list(Criteria criteria) throws Exception;	
	
	List<E> listPorNome(Object parametro) throws Exception;
	
	List<E> listPorNome(String nomeCampo, Object parametro) throws Exception;
}