package br.com.meldica.model.dao.publico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.relatorios.RelatorioEstoqueProdutoAcabadoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;

@Repository
public class OrdemServicoMapperDao {
	
	private final SqlSession sqlSession;

	public OrdemServicoMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}
	
	public List<OrdemServicoEntity> listar(OrdemServicoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<OrdemServicoEntity>();
		}
		
		return sqlSession.selectList("OrdemServicoMapper.listar", filtro);
	}
	
	// Apagar map depois
//	public List<OrdemServicoEntity> listarDashboard(OrdemServicoEntity filtro) {
//		if (Objects.isNull(filtro)) {
//			return new ArrayList<OrdemServicoEntity>();
//		}
//		
//		return sqlSession.selectList("OrdemServicoMapper.listarDashboard", filtro);
//	}
	
	public List<OrdemServicoEntity> relatorioResumoMensal(OrdemServicoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<OrdemServicoEntity>();
		}
		
		return sqlSession.selectList("OrdemServicoMapper.relatorioResumoMensal", filtro);
	}
	
	public List<OrdemServicoEntity> relatorioListagemVenda(OrdemServicoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<OrdemServicoEntity>();
		}
		
		return sqlSession.selectList("OrdemServicoMapper.relatorioListagemVenda", filtro);
	}
	
	public List<OrdemServicoEntity> relatorioAProduzir(OrdemServicoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<OrdemServicoEntity>();
		}
		
		return sqlSession.selectList("OrdemServicoMapper.relatorioAProduzir", filtro);
	}
	
	public List<OrdemServicoEntity> relatorioDashboard(OrdemServicoEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<OrdemServicoEntity>();
		}
		
		return sqlSession.selectList("OrdemServicoMapper.relatorioDashboard", filtro);
	}
	
	public List<RelatorioEstoqueProdutoAcabadoDto> relatorioEstoqueProdutoAcabado(Integer idEmpresa) {
		return sqlSession.selectList("OrdemServicoMapper.relatorioEstoqueProdutoAcabado", idEmpresa);
	}
	
	public void gerarFinanceiro(Integer id) {
		sqlSession.update("OrdemServicoMapper.gerarFinanceiro", id);
	}
	
	public void deletarFinanceiro(Integer id) {
		sqlSession.update("OrdemServicoMapper.deletarFinanceiro", id);
	}
	
}
