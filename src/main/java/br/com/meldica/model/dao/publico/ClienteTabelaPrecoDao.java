package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ClienteTabelaPrecoEntity;

@Repository
public interface ClienteTabelaPrecoDao extends JpaRepository<ClienteTabelaPrecoEntity, Long> {
	@Query("select ep from ClienteTabelaPrecoEntity ep where ep.idCliente = :idCliente")
	List<ClienteTabelaPrecoEntity> listarPorCliente(@Param("idCliente") Integer idCliente);
	
	@Query("select ep from ClienteTabelaPrecoEntity ep ")
	List<ClienteTabelaPrecoEntity> listar();
}
