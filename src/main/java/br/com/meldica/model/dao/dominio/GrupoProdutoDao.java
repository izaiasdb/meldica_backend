package br.com.meldica.model.dao.dominio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;

@Repository
public interface GrupoProdutoDao extends JpaRepository<GrupoProdutoEntity, Integer>{
	@Query("from GrupoProdutoEntity order by nome ")
	public List<GrupoProdutoEntity> listar();
	
	@Query("select s from GrupoProdutoEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by nome ")
	List<GrupoProdutoEntity> listar(@Param("nome") String nome);
	
	@Query("select t from GrupoProdutoEntity t where t.id = :id")
	public GrupoProdutoEntity obter(@Param("id") Integer id);
}
