package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;

@Repository
public interface OrdemServicoFormaPagamentoDao extends JpaRepository<OrdemServicoFormaPagamentoEntity, Long> {
	@Query("select ep from OrdemServicoFormaPagamentoEntity ep where ep.idOrdemServico = :idOrdemServico")
	List<OrdemServicoFormaPagamentoEntity> listarPorOrdemServico(@Param("idOrdemServico") Integer idOrdemServico);
}
