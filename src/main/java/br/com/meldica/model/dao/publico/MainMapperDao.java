package br.com.meldica.model.dao.publico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.publico.FichaDto;

@Repository
public class MainMapperDao {
	
	private final SqlSession sqlSession;

	public MainMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}
	
	public List<FichaDto> listarBuscaRapida(FichaDto filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<FichaDto>();
		}
		
		return sqlSession.selectList("MainMapper.listarBuscaRapida", filtro);
	}
	
}
