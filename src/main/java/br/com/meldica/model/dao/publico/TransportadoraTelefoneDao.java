package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.TransportadoraTelefoneEntity;

@Repository
public interface TransportadoraTelefoneDao extends JpaRepository<TransportadoraTelefoneEntity, Long> {
	@Query("select ep from TransportadoraTelefoneEntity ep where ep.idTransportadora = :idTransportadora")
	List<TransportadoraTelefoneEntity> listarPorTransportadora(@Param("idTransportadora") Integer idTransportadora);
}
