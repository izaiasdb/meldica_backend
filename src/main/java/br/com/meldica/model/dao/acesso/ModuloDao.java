package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.ModuloEntity;

@Repository
public interface ModuloDao extends JpaRepository<ModuloEntity, Integer>{
	@Query("select s from ModuloEntity s where upper(s.nome) like '%'|| upper(:nome) || '%'")
	List<ModuloEntity> listar(@Param("nome") String nome);
	
	@Query(" select m from ModuloEntity m where 1 = 1"
		+ " and (:#{#nome} is null or normalizar_string(m.nome) like '%' || normalizar_string(:#{#nome ?: ''}) || '%') "
		+ " and (:#{#idSistema} is null or m.sistema.id = :#{#idSistema}) "
		)
	List<ModuloEntity> listarPorNomeSistema(@Param("idSistema") Integer idSistema, @Param("nome") String nome);	
}
