package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FornecedorTelefoneEntity;

@Repository
public interface FornecedorTelefoneDao extends JpaRepository<FornecedorTelefoneEntity, Long> {
	@Query("select ep from FornecedorTelefoneEntity ep where ep.idFornecedor = :idFornecedor")
	List<FornecedorTelefoneEntity> listarPorFornecedor(@Param("idFornecedor") Integer idFornecedor);
}
