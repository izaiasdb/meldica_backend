package br.com.meldica.model.dao.dominio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.dominio.UfEntity;

@Repository
public interface UfDao extends JpaRepository<UfEntity, String>{
}
