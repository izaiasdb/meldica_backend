package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FormaCondicaoPagamentoEntity;

@Repository
public interface FormaCondicaoPagamentoDao extends JpaRepository<FormaCondicaoPagamentoEntity, Integer>{
	@Query("select fc from FormaCondicaoPagamentoEntity fc join fc.formaPagamento f where fc.cancelado = false order by f.nome ") 
	public List<FormaCondicaoPagamentoEntity> listar();
	
	@Query("select fc from FormaCondicaoPagamentoEntity fc join fc.formaPagamento f where fc.cancelado = false and fc.ativo = true order by f.nome") 
	public List<FormaCondicaoPagamentoEntity> listarAtivos();
	
	
	@Query("select fc from FormaCondicaoPagamentoEntity fc where fc.id = :id")
	public FormaCondicaoPagamentoEntity obter(@Param("id") Integer id);
	
	@Query("select fc from FormaCondicaoPagamentoEntity fc join fc.condicaoPagamento c join fc.formaPagamento f where 1=1 and fc.cancelado = false "
	+ "  and (:#{#filtro.formaPagamento?.id} is null or fc.formaPagamento.id = :#{#filtro.formaPagamento?.id}) "
	+ "  and (:#{#filtro.condicaoPagamento?.id} is null or fc.condicaoPagamento.id = :#{#filtro.condicaoPagamento?.id}) ")
	List<FormaCondicaoPagamentoEntity> listar(@Param("filtro") FormaCondicaoPagamentoEntity filtro);	
	
	@Modifying
	@Query("update FormaCondicaoPagamentoEntity c set c.cancelado = :cancelado where c.id = :id")
	void cancelar(@Param("id") Integer id, @Param("cancelado") Boolean cancelado);
}
