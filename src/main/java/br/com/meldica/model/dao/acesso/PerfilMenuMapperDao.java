package br.com.meldica.model.dao.acesso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.enums.MenuPermissaoEnum;

@Repository
public class PerfilMenuMapperDao {
	
	private final SqlSession sqlSession;

	public PerfilMenuMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}

	public List<String> litarPermissoesPerfil(FiltroUsuarioMenuDto filtro) {
		if (Objects.isNull(filtro) || Objects.isNull(filtro.getIdPerfil())) {
			return new ArrayList<String>();
		}
		
		List<String> permissoes = new ArrayList<String>();
		List<FiltroUsuarioMenuDto> perfilMenuList = sqlSession.selectList("PerfilMenuMapper.litarPermissoesPerfil", filtro);
		
		perfilMenuList.forEach(menu -> {
			MenuPermissaoEnum.listarPorSomaPermissao(menu.getSomaPermissao()).forEach(menuPermissao ->{
				permissoes.add(menu.getPermissao() + "_" + menuPermissao.getNome());
			});
		});		
		
		return permissoes;
	}

}
