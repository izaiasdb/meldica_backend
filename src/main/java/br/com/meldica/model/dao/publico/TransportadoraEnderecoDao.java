package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.TransportadoraEnderecoEntity;

@Repository
public interface TransportadoraEnderecoDao extends JpaRepository<TransportadoraEnderecoEntity, Long> {
	@Query("select ep from TransportadoraEnderecoEntity ep where ep.idTransportadora = :idTransportadora")
	List<TransportadoraEnderecoEntity> listarPorTransportadora(@Param("idTransportadora") Integer idTransportadora);
}
