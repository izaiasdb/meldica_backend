package br.com.meldica.model.dao.publico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.relatorios.PagarReceberDto;
import br.com.meldica.model.entity.publico.PagarReceberEntity;

@Repository
public class PagarReceberMapperDao {
	
	private final SqlSession sqlSession;

	public PagarReceberMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}
	
	public List<PagarReceberEntity> listar(PagarReceberEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<PagarReceberEntity>();
		}
		
		return sqlSession.selectList("PagarReceberMapper.listar", filtro);
	}
	
	public List<PagarReceberEntity> listarPorOrdemServico(Integer idOrdemServico) {
		return sqlSession.selectList("PagarReceberMapper.listarPorOrdemServico", idOrdemServico);
	}
	
	public List<PagarReceberDto> listarPagarReceber(PagarReceberDto filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<PagarReceberDto>();
		}
		
		return sqlSession.selectList("RelatorioPagarReceberMapper.listarBalancetePagarReceber", filtro);
	}
	
	public List<PagarReceberDto> listarPagoRecebido(PagarReceberDto filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<PagarReceberDto>();
		}
		
		return sqlSession.selectList("RelatorioPagarReceberMapper.listarBalancetePagoRecebido", filtro);
	}
//	
//	public List<LogTerminalEventoEntity> listarDoDia(Integer idTerminal) {
//		if (Objects.isNull(idTerminal)) {
//			return new ArrayList<LogTerminalEventoEntity>();
//		}
//		
//		return sqlSession.selectList("LogTerminalEventoMapper.listarDoDia", idTerminal);
//	}

}
