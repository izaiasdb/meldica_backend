package br.com.meldica.model.dao.utils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.meldica.model.entity.utils.AbstractEntity;

/**
 * 
 * @author izaias.barreto
 *
 * @param <E>
 * @param <K>
 * Ref: http://stackoverflow.com/questions/1219893/difference-between-abstract-and-generic-code
 * 		http://stackoverflow.com/questions/1096959/generic-types-vs-abstract-class-interfaces
 * 
 * @Transactional
 * @Transactional(value=TransactionNameConst.SISGRAPH, readOnly = true)
 */
public class AbstractDAOJpa<E extends AbstractEntity, K extends Serializable>
	implements GenericDAO<E, K> {
	
	protected Class<? extends E> entity;
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public AbstractDAOJpa() {
		entity = (Class<E>) ((ParameterizedType) getClass()
			.getGenericSuperclass()).getActualTypeArguments()[0];		
	}
	
	/**
	 * https://altieresm.wordpress.com/2011/06/30/criteria-com-hibernate-parte1/
	 * http://stackoverflow.com/questions/4148231/how-can-i-get-the-session-object-if-i-have-the-entitymanager
	 * @return
	 */
	public Session getSession(){
//		org.hibernate.Session session = ((org.hibernate.Session)entityManager.getDelegate()).getSessionFactory();
//		org.hibernate.Session session = (Session) entityManager.getDelegate(); //JPA 1.0
		return (Session)entityManager.unwrap(Session.class);  //JPA 2.0
	}

	public void flushAndClear() throws Exception {
		Session sessao = (Session) this.getEntityManager().getDelegate();

		if (sessao.isDirty()) {
			sessao.flush();
			sessao.clear();
		}
	}

	public E save(E entity) {
		this.getEntityManager().persist(entity);
		return entity;
	}

	public E update(E entity) {
		this.getEntityManager().merge(entity);
		return entity;
	}

	public void delete(E entity) {
//		this.getEntityManager().remove(this.getEntityManager().getReference(entity.getClass(), entity.getId()));
//		this.getEntityManager().remove(entity); // Estava assim
		Object entityRemover = this.getEntityManager().merge(entity);
		this.getEntityManager().remove(entityRemover);		
	}
	
	public List<E> insert(List<E> list) throws Exception {
		for (E entity : list)
			this.getEntityManager().persist(entity);

		return list;
	}

	public List<E> update(List<E> list) throws Exception {
		for (E entity : list)
			this.getEntityManager().merge(entity);

		return list;
	}
	
	 public void delete(List<E> list) throws Exception {
		 for (E entity : list)
			 this.getEntityManager().remove(entity);
	 }
	
	@SuppressWarnings("unchecked")
	public List<E> list() {
		return getEntityManager().createQuery("SELECT p FROM " + entity.getName() + " p").getResultList();
	}

	public E find(K key) {
		return getEntityManager().find(entity, key);		
	}

	@SuppressWarnings("unchecked")
	public List<E> list(Criteria criteria) {
		return criteria.list();
	}
	
	@Override
	public List<E> listPorNome(Object parametro) throws Exception {
		return listPorNome("nome", parametro);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<E> listPorNome(String nomeCampo, Object parametro) throws Exception {
		try {
			Criteria criteria = getSession().createCriteria(entity);
			criteria.add(Restrictions.ilike(nomeCampo, (String) parametro, MatchMode.ANYWHERE));

			return criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}