package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FuncionarioEnderecoEntity;

@Repository
public interface FuncionarioEnderecoDao extends JpaRepository<FuncionarioEnderecoEntity, Long> {
	@Query("select ep from FuncionarioEnderecoEntity ep where ep.idFuncionario = :idFuncionario")
	List<FuncionarioEnderecoEntity> listarPorFuncionario(@Param("idFuncionario") Integer idFuncionario);
}
