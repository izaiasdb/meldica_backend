package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.FuncionarioEntity;

@Repository
public interface FuncionarioDao extends JpaRepository<FuncionarioEntity, Integer>{
	@Query("select s from FuncionarioEntity s order by s.nome ")
	List<FuncionarioEntity> listar();
	
	@Query("select s from FuncionarioEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<FuncionarioEntity> listar(@Param("nome") String nome);
	
}
