package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ClienteRazaoEntity;

@Repository
public interface ClienteRazaoDao extends JpaRepository<ClienteRazaoEntity, Long> {
	@Query("select s from ClienteRazaoEntity s where id = :id")
	ClienteRazaoEntity obter(@Param("id") Integer id);
	
	@Query("select ep from ClienteRazaoEntity ep where ep.idCliente = :idCliente")
	List<ClienteRazaoEntity> listarPorCliente(@Param("idCliente") Integer idCliente);
}
