package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.PagarReceberEntity;

@Repository
public interface PagarReceberDao extends JpaRepository<PagarReceberEntity, Integer>{
	@Query("select s from PagarReceberEntity s where s.cancelado is false and s.id = :id")
	PagarReceberEntity obter(@Param("id") Integer id);	
	
	@Query("select s from PagarReceberEntity s where s.cancelado is false and s.idOrdemServicoFormaCondicaoPagamento = :idOsForma order by s.dataVencimento desc ")
	List<PagarReceberEntity> listarPorOrdemServicoForma(@Param("idOsForma") Integer idOsForma);	
	
//	@Query("select s from PagarReceberEntity s where s.cancelado is false order by s.dataVencimento desc ")
//	List<PagarReceberEntity> listar();
	
	/*
	@Query("select s from PagarReceberEntity s " 
	//s join s.cliente c where 1=1 "
	+ " where 1 = 1 and s.cancelado is false "  
	+ " and (:#{#filtro.receitaDespesa} is null or s.receitaDespesa = :#{#filtro.receitaDespesa}) "
	+ "  and (:#{#filtro.cliente?.id} is null or s.cliente.id = :#{#filtro.cliente?.id}) "
	+ "  and (:#{#filtro.fornecedor?.id} is null or s.fornecedor.id = :#{#filtro.fornecedor?.id}) "
	+ "  and (:#{#filtro.planoConta?.id} is null or s.planoConta.id = :#{#filtro.planoConta?.id}) "
	+ "  and (  s.dataVencimento BETWEEN to_date(:#{#filtro.periodo[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodo[1] ?: ''},'yyyy-MM-dd')) " 
//	+ " and (case when :#{#filtro.status} is null then entity1.attribute2 is null else 1=1 end) "
	+ " order by s.dataVencimento desc " )	
	List<PagarReceberEntity> listar(@Param("filtro") PagarReceberEntity filtro);
	*/
	
}
