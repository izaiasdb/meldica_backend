package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ClienteEntity;

@Repository
public interface ClienteDao extends JpaRepository<ClienteEntity, Integer>{
	@Query("select s from ClienteEntity s where id = :id")
	ClienteEntity obter(@Param("id") Integer id);	
	
	@Query("select s from ClienteEntity s where s.cancelado = false order by s.nome ")
	List<ClienteEntity> listar();
	
	@Query("select s from ClienteEntity s where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome ")
	List<ClienteEntity> listar(@Param("nome") String nome);
	
	@Modifying
	@Query("update ClienteEntity c set c.cancelado = :cancelado where c.id = :id")
	void cancelar(@Param("id") Integer id, @Param("cancelado") Boolean cancelado);
}
