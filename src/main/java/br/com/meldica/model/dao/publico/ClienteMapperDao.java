package br.com.meldica.model.dao.publico;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.dto.publico.FichaDto;
import br.com.meldica.model.entity.publico.ClienteEntity;

@Repository
public class ClienteMapperDao {
	
	private final SqlSession sqlSession;

	public ClienteMapperDao(SqlSession sqlSession) {
	    this.sqlSession = sqlSession;
	}
	
	public List<ClienteEntity> listar(ClienteEntity filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<ClienteEntity>();
		}
		
		return sqlSession.selectList("ClienteMapper.listar", filtro);
	}
	
	public List<ClienteEntity> listar2(FichaDto filtro) {
		if (Objects.isNull(filtro)) {
			return new ArrayList<ClienteEntity>();
		}
		
		return sqlSession.selectList("ClienteMapper.listar", filtro);
	}
	
}
