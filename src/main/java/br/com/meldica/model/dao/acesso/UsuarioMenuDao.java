package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.UsuarioMenuEntity;

@Repository
public interface UsuarioMenuDao extends JpaRepository<UsuarioMenuEntity, Integer>{
	@Query(" select m from UsuarioMenuEntity m where 1 = 1 and idUsuario = :idUsuario ")
	List<UsuarioMenuEntity> listarPorUsuario(@Param("idUsuario") Integer idUsuario);
}
