package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ProducaoEntity;

@Repository
public interface ProducaoDao extends JpaRepository<ProducaoEntity, Integer>{
	@Query("select s from ProducaoEntity s where s.cancelado is false order by s.dataProducao desc ")
	List<ProducaoEntity> listar();
	
	@Query("select s from ProducaoEntity s where s.cancelado is false and id = :id")
	ProducaoEntity obter(@Param("id") Integer id);	
	
	@Query("select s from ProducaoEntity s where 1=1 and s.cancelado is false "
	+ "  and ((:#{#filtro.status} is null or :#{#filtro.status} = '') or s.status = :#{#filtro.status}) "
	+ "  and (  s.dataProducao BETWEEN to_date(:#{#filtro.periodoProducao[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoProducao[1] ?: ''},'yyyy-MM-dd')) "
	+ " order by s.dataProducao desc " )
	List<ProducaoEntity> listar(@Param("filtro") ProducaoEntity filtro);
	
	@Query("select s from ProducaoEntity s where s.cancelado is false and s.status = :status  order by s.dataProducao desc ")
	List<ProducaoEntity> listarPorStatus(@Param("status") String status);
	
}
