package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.ConfiguracaoEntity;

@Repository
public interface ConfiguracaoDao extends JpaRepository<ConfiguracaoEntity, Integer>{
	@Query("select m from ConfiguracaoEntity m where m.id = :id ")
	ConfiguracaoEntity obter(@Param("id") Integer id);
	
	@Query("select m from ConfiguracaoEntity m ")
	List<ConfiguracaoEntity> listar();
}
