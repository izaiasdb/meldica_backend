package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;

@Repository
public interface OrdemServicoKitDao extends JpaRepository<OrdemServicoKitEntity, Long> {
	@Query("select ep from OrdemServicoKitEntity ep where ep.idOrdemServico = :idOrdemServico")
	List<OrdemServicoKitEntity> listarPorOrdemServico(@Param("idOrdemServico") Integer idOrdemServico);
}
