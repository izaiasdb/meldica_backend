package br.com.meldica.model.dao.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository<T, ID extends Serializable>
  extends CrudRepository<T, Long> {

//	@Query("select e from #{#entityName} e where ?1 member of p.categories")
//    Iterable<T> findByCategory(String category);
	
//    <S extends T> S save(S entity);

	@Query("select e from #{#entityName} as e where e.id = ?1 ")
	T obter(ID id);
	
	@Query("select e from #{#entityName} e ")
	List<T> listar();
	
//	@Query("select e from #{#entityName} e order by e.descricao ")
//	List<T> listarOrdenadoNome();
}