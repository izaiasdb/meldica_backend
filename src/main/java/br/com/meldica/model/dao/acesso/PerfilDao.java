package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.PerfilEntity;

@Repository
public interface PerfilDao extends JpaRepository<PerfilEntity, Integer>{
	@Query("select s from PerfilEntity s where upper(s.nome) like '%'|| upper(:nome) || '%'")
	List<PerfilEntity> listar(@Param("nome") String nome);	
}
