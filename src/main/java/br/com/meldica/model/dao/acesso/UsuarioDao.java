package br.com.meldica.model.dao.acesso;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.acesso.UsuarioEntity;

@Repository
public interface UsuarioDao extends JpaRepository<UsuarioEntity, Integer> {
	
	@Query("select u from UsuarioEntity u "
			+ "where 1 = 1 "
			//+ "and (:#{#usuario.nome} is null or normalizar_string(u.nome) like '%' || normalizar_string(:#{#usuario.nome ?: ''}) || '%' ) "
			//+ "and (:#{#usuario.login} is null or normalizar_string(u.login) like '%' || normalizar_string(:#{#usuario.login ?: ''}) || '%' ) "
			+ "and (:#{#usuario.nome} is null or upper(u.nome) like '%' || upper(:#{#usuario.nome ?: ''}) || '%' ) "
			+ "and (:#{#usuario.login} is null or upper(u.login) like '%' || upper(:#{#usuario.login ?: ''}) || '%' ) "			
			+ "and (:#{#usuario.perfil?.id} is null or u.perfil.id = :#{#usuario.perfil?.id}) "
			+ "order by u.nome")	
	List<UsuarioEntity> listar(@Param("usuario") UsuarioEntity usuario);
	
	@Query("select s from UsuarioEntity s where upper(s.login) = upper(:login)")
	UsuarioEntity obterPorLogin(@Param("login") String login);
	
	@Query("select s from UsuarioEntity s where upper(s.login) = upper(:login) and upper(s.email) = upper(:email) ")
	UsuarioEntity obterPorLoginEmail(@Param("login") String login, @Param("email") String email);
	
	@Query("select s from UsuarioEntity s where upper(s.email) = upper(:email)")
	UsuarioEntity obterPorEmail(@Param("email") String email);
	
	@Query("select s from UsuarioEntity s where upper(s.login) = upper(:login) and upper(s.codigoVerificacao) = upper(:codigoVerificacao) ")
	UsuarioEntity obterPorLoginCodigoVerificacao(@Param("login") String login, @Param("codigoVerificacao") String codigoVerificacao);	
	
}
