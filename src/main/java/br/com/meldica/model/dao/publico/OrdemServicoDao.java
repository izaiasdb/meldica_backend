package br.com.meldica.model.dao.publico;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.meldica.model.entity.publico.OrdemServicoEntity;

@Repository
public interface OrdemServicoDao extends JpaRepository<OrdemServicoEntity, Integer>{
	@Query("select s from OrdemServicoEntity s where s.cancelado is false order by s.dataVenda desc ")
	List<OrdemServicoEntity> listar();
	
	@Query("select s from OrdemServicoEntity s where s.cancelado is false and id = :id")
	OrdemServicoEntity obter(@Param("id") Integer id);	
	
	//  where upper(s.nome) like '%'|| upper(:nome) || '%' order by s.nome
	@Query("select s from OrdemServicoEntity s join s.cliente c where 1=1 and s.cancelado is false "
	//+ "  and (:#{#filtro.cliente.nome} is null or normalizar_string(c.nome) like '%' || normalizar_string(:#{#filtro.cliente.nome ?: ''}) || '%') "
	//+ "  and ((:#{#filtro.cancelado} is null or s.cancelado = :#{#filtro.cancelado}) "
	+ "  and (:#{#filtro.cliente?.id} is null or s.cliente.id = :#{#filtro.cliente?.id}) "
	+ "  and (:#{#filtro.funcionario?.id} is null or s.funcionario.id = :#{#filtro.funcionario?.id}) "
	+ "  and (:#{#filtro.tabelaPreco?.id} is null or s.tabelaPreco.id = :#{#filtro.tabelaPreco?.id}) "
	+ "  and (:#{#filtro.planoConta?.id} is null or s.planoConta.id = :#{#filtro.planoConta?.id}) "
	+ "  and ((:#{#filtro.statusNota} is null or :#{#filtro.statusNota} = '') or s.statusNota = :#{#filtro.statusNota}) "
	//+ "  and ((:#{#filtro.nfMeldica} is null or :#{#filtro.nfMeldica} = '') or s.nfMeldica = :#{#filtro.nfMeldica}) "
	//+ "  and ((:#{#filtro.nfCosmetico} is null or :#{#filtro.nfCosmetico} = '') or s.nfCosmetico = :#{#filtro.nfCosmetico}) "
	+ "  and (  s.dataVenda BETWEEN to_date(:#{#filtro.periodoVenda[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoVenda[1] ?: ''},'yyyy-MM-dd')) "
	//+ "  and (  s.dataLiberacao BETWEEN to_date(:#{#filtro.periodoLiberacao[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoLiberacao[1] ?: ''},'yyyy-MM-dd')) "
	//+ "  and (  s.dataPrevisaoEntrega BETWEEN to_date(:#{#filtro.periodoPrevisaoEntrega[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoPrevisaoEntrega[1] ?: ''},'yyyy-MM-dd')) "
	//+ "  and (  s.dataEntrega BETWEEN to_date(:#{#filtro.periodoEntrega[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoEntrega[1] ?: ''},'yyyy-MM-dd')) "
	
	+ " order by s.dataVenda desc " )
	//+ "  and ((:#{#filtro.periodoVenda} is null or :#{#filtro.periodoVenda} IS EMPTY or cast(:#{#filtro.periodoVenda[0]} as date) is null or cast(:#{#filtro.periodoVenda[1]} as date) is null) or s.dataVenda BETWEEN to_date(:#{#filtro.periodoVenda[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoVenda[1] ?: ''},'yyyy-MM-dd')) " )
	//+ "  and ((:#{#filtro.periodoVenda} is null or size(filtro.periodoVenda) = 0 or cast(:#{#filtro.periodoVenda[0]} as date) is null or cast(:#{#filtro.periodoVenda[1]} as date) is null) or s.dataVenda BETWEEN to_date(:#{#filtro.periodoVenda[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoVenda[1] ?: ''},'yyyy-MM-dd')) " )
	//+ "  and ((:#{#filtro.periodoVenda} is null or cast(:#{#filtro.periodoVenda[0]} as date) is null or cast(:#{#filtro.periodoVenda[1]} as date) is null) or s.dataVenda BETWEEN to_date(:#{#filtro.periodoVenda[0] ?: ''},'yyyy-MM-dd') and to_date(:#{#filtro.periodoVenda[1] ?: ''},'yyyy-MM-dd')) " )
	//+ "  and (:#{#filtro.dataNascimento} is null or ps.dataNascimento = to_date(:#{#filtro.dataNascimento ?: ''},'yyyy-MM-dd')) "
	List<OrdemServicoEntity> listar(@Param("filtro") OrdemServicoEntity filtro);
	
	@Query("select s from OrdemServicoEntity s where s.cancelado is false and s.statusNota = :statusNota  order by s.dataVenda desc ")
	List<OrdemServicoEntity> listarPorStatus(@Param("statusNota") String statusNota);
	
	@Query("select s from OrdemServicoEntity s join s.cliente c  where s.cancelado is false and c.id = :idCliente order by s.dataInclusao desc ")
	List<OrdemServicoEntity> listarPorCliente(@Param("idCliente") Integer idCliente);
	
}
