package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.entity.utils.AbstractEntity;
import br.com.meldica.model.enums.ReceitaDespesaEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import br.com.meldica.utils.StringUtils;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "plano_conta", catalog = SistemaConst.DATABASE, schema = "public")
public class PlanoContaEntity implements AbstractEntity, Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_plano_conta", sequenceName="public.seq_plano_conta", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_plano_conta")
    @Column(name = "id")
    private Integer id;
	    
    @ManyToOne
    @JoinColumn(name = "id_Plano_conta")
    private PlanoContaEntity planoContaPai;
    
	@Column(name = "nivel")
	private Integer nivel;    
	
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "numero_conta")
    private String numeroConta;    
    
    @Column(name = "ativo")
    private boolean ativo;

    @Column(name = "tipo_plano_conta")
    private String tipoPlanoConta;      

    @Column(name = "receita_despesa")
    private String receitaDespesa;
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAlteracao;
	
	public ReceitaDespesaEnum getTipoReceitaDespesa() {
		if (StringUtils.isNullOrEmpty(receitaDespesa)) {
			return ReceitaDespesaEnum.obterPorId(receitaDespesa);
		}
		
		return null;
	}   

	public PlanoContaEntity() {
	}
	
	public PlanoContaEntity(Integer nivel, String nome, String numeroConta) {
		super();
		this.nivel = nivel;
		this.nome = nome;
		this.numeroConta = numeroConta;
	}
	

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanoContaEntity)) {
            return false;
        }
        PlanoContaEntity other = (PlanoContaEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.PlanoContaEntity[ id=" + id + " ]";
    }


}
