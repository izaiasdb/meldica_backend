package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.dto.utils.ValueObject;
import br.com.meldica.model.enums.TipoEnderecoEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "fornecedor_endereco", catalog = SistemaConst.DATABASE, schema = "public")
public class FornecedorEnderecoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_fornecedor_endereco", sequenceName="public.seq_fornecedor_endereco", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_fornecedor_endereco")
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "id_fornecedor")
    private Integer idFornecedor;
    
    @Column(name = "id_tipo_endereco")
    private Integer idTipoEndereco;    
	
    @Column(name = "ativo")
    private boolean ativo;
    
    @Column(name = "logradouro")
    private String logradouro;
    
    @Column(name = "bairro")
    private String bairro;    
    
    @Column(name = "cidade")
    private String cidade;      

    @Column(name = "uf")
    private String uf;
    
    @Column(name = "cep")
    private String cep;
    
    @Column(name = "complemento")
    private String complemento;
    
    @Column(name = "numero")
    private String numero;    
    
	public ValueObject getTipoEndereco() {
		if (idTipoEndereco != null) {
			return TipoEnderecoEnum.obterValueObjectPorId(idTipoEndereco);
		}
		
		return null;
	}     
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FornecedorEnderecoEntity)) {
            return false;
        }
        FornecedorEnderecoEntity other = (FornecedorEnderecoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.FornecedorEnderecoEntity[ id=" + id + " ]";
    }
    
}
