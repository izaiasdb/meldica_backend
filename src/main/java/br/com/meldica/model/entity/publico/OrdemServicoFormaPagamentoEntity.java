package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "ordem_servico_forma_condicao_pagamento", catalog = SistemaConst.DATABASE, schema = "public")
public class OrdemServicoFormaPagamentoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_ordem_servico_forma_condicao_pagamento", sequenceName="public.seq_ordem_servico_forma_condicao_pagamento", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ordem_servico_forma_condicao_pagamento")
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_ordem_servico")
    private Integer idOrdemServico;
    
    @Column(name = "id_empresa")
    private Integer idEmpresa;	    
	
    @ManyToOne
    @JoinColumn(name = "id_forma_condicao_pagamento")
    private FormaCondicaoPagamentoEntity formaCondicaoPagamento;  		
    
    // P-roduto - F-rete 
    @Column(name = "tipo_forma")
    private String tipoForma;  
    
    @Column(name = "cancelado")
    private boolean cancelado;
    
    @Column(name = "gerado")
    private boolean gerado;    
    
	//@Column(name = "valor", precision = 12, scale = 2)
    @Column(name = "valor")
	private BigDecimal valor;
	
//	@Column(name = "valor_recebido", precision = 12, scale = 2)
//	private BigDecimal valorRecebido;
	
	@Column(name = "nome_forma_pagamento")
	private String nomeFormaPagamento;
	
	@Column(name = "nome_condicao_pagamento")
	private String nomeCondicaoPagamento;	
	
	@Column(name = "perc_desconto", precision = 12, scale = 2)
	private BigDecimal percDesconto;
	
	@Column(name = "desconto", precision = 12, scale = 2)
	private BigDecimal desconto;	
	
	@Column(name = "perc_desconto_forma_condicao", precision = 12, scale = 2)
	private BigDecimal percDescontoFormaCondicao;
	
	@Column(name = "desconto_forma_condicao", precision = 12, scale = 2)
	private BigDecimal descontoFormaCondicao;
	
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;
	
	@Transient
	public BigDecimal getTotal() {
		return this.valor.subtract(this.desconto);
	}
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdemServicoFormaPagamentoEntity)) {
            return false;
        }
        OrdemServicoFormaPagamentoEntity other = (OrdemServicoFormaPagamentoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.OrdemServicoFormaEntity[ id=" + id + " ]";
    }
    
}
