package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "producao", catalog = SistemaConst.DATABASE, schema = "public")
public class ProducaoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_producao", sequenceName="public.seq_producao", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_producao")
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "status", nullable = false, length = 1)
    private String status;
    
    @Column(name = "cancelado", nullable = false)
    private boolean cancelado;
    
	@Column(name = "observacao")
	private String observacao;	
	
	@Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_producao", nullable = false)
	private Date dataProducao;
    
	@Column(name = "id_usuario_inclusao", updatable = false)
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
//	@Transient
//	public String getStatusNovaDescricao(){	 
//		if (!StringUtils.isNullOrEmpty(statusNota)) {
//			return StatusNotaEnum.obterPorId(statusNota).getDescricao();	
//		}
//		
//		return "";
//    } 
//	
//	@Transient
//	private String dataInicio;
	
	@Transient
	private List<Date> periodoProducao = new ArrayList<>();
	
	@Transient
	private List<ProducaoProdutoEntity> produtoItemsList = new ArrayList<>();

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProducaoEntity)) {
            return false;
        }
        ProducaoEntity other = (ProducaoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
