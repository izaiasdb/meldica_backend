package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.enums.PfPjEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import br.com.meldica.utils.StringUtils;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "cliente", catalog = SistemaConst.DATABASE, schema = "public")
public class ClienteEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_cliente", sequenceName="public.seq_cliente", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cliente")
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "nome_fantasia")
    private String nomeFantasia;    
    
    @Column(name = "ativo")
    private boolean ativo;
    
    @Column(name = "cancelado")
    private boolean cancelado;

    @Column(name = "fisica_juridica")
    private String fisicaJuridica;      

    @Column(name = "cpf_cnpj")
    private String cpfCnpj;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "observacao")
    private String observacao;
    
    @Column(name = "site")
    private String site;
    
    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual; 
    
	@Column(name = "limite_credito", precision = 12, scale = 2)
	private BigDecimal limiteCredito = BigDecimal.ZERO;
	
    @Column(name = "prazo_pagamento")
    private String prazoPagamento;    
    
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataNascimento;    
    
    // Início que o cliente começou a comprar na méldica, cliente desde...
    @Column(name = "data_inicio")
    @Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataInicio;
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;
	
	public PfPjEnum getTipoFisicaJuridica() {
		if (StringUtils.isNullOrEmpty(fisicaJuridica)) {
			return PfPjEnum.obterPorId(fisicaJuridica);
		}
		
		return null;
	}   

//	@Transient
//	private TipoProdutoEnum getTipoProdutoEnum(){	 
//        switch (tipo)
//        {
//            case "P":
//                return TipoProdutoEnum.PRODUTO;
//            case "I":
//                return TipoProdutoEnum.INSUMO;
//            case "C":
//                return TipoProdutoEnum.COMBINADO;                    
//            default:
//                return TipoProdutoEnum.PRODUTO;
//        } 
//    }   
	
	@Transient
	private String cidade;
	
	@Transient
	private String uf;
	
	@Transient
	private String razaoSocial;
	
	@Transient
	private List<ClienteEnderecoEntity> clienteEnderecoList = new ArrayList<ClienteEnderecoEntity>();
	
	@Transient
	private List<ClienteTelefoneEntity> clienteTelefoneList = new ArrayList<ClienteTelefoneEntity>();
    
	@Transient
	private List<ClienteRazaoEntity> clienteRazaoList = new ArrayList<ClienteRazaoEntity>();

	@Transient
	private List<ClienteTabelaPrecoEntity> clienteTabelaPrecoList = new ArrayList<ClienteTabelaPrecoEntity>();
	
	@Transient
	private List<OrdemServicoEntity> ordemServicoList = new ArrayList<OrdemServicoEntity>();
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteEntity)) {
            return false;
        }
        ClienteEntity other = (ClienteEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.ClienteEntity[ id=" + id + " ]";
    }
    
}
