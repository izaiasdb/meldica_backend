package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.dto.utils.ValueObject;
import br.com.meldica.model.enums.TipoTelefoneEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "cliente_telefone", catalog = SistemaConst.DATABASE, schema = "public")
public class ClienteTelefoneEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_cliente_telefone", sequenceName="public.seq_cliente_telefone", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cliente_telefone")
    @Column(name = "id")
    private Integer id;
	
    @Column(name = "id_cliente")
    private Integer idCliente;
    
    @Column(name = "id_tipo_telefone")
    private Integer idTipoTelefone;    
	
    @Column(name = "ativo")
    private boolean ativo;
    
    @Column(name = "ddd")
    private String ddd;
    
    @Column(name = "numero")
    private String numero;    
    
	public ValueObject getTipoTelefone() {
		if (idTipoTelefone != null) {
			return TipoTelefoneEnum.obterValueObjectPorId(idTipoTelefone);
		}
		
		return null;
	}     
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteTelefoneEntity)) {
            return false;
        }
        ClienteTelefoneEntity other = (ClienteTelefoneEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.ClienteTelefoneEntity[ id=" + id + " ]";
    }
    
}
