package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;
import br.com.meldica.model.entity.dominio.UnidadeMedidaEntity;
import br.com.meldica.model.enums.TipoProdutoEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import br.com.meldica.utils.StringUtils;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "produto", catalog = SistemaConst.DATABASE, schema = "public")
public class ProdutoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_produto", sequenceName="public.seq_produto", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_produto")
    @Column(name = "id")
    private Integer id;
	
    @ManyToOne
    @JoinColumn(name = "id_unidade_medida")
    private UnidadeMedidaEntity unidadeMedida;  	
    
    @ManyToOne
    @JoinColumn(name = "id_grupo_produto")
    private GrupoProdutoEntity grupoProduto;  
    
    @ManyToOne
    @JoinColumn(name = "id_empresa")
    private EmpresaEntity empresa;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "ativo")
    private boolean ativo;
    
    //I-NSUMO -P-RODUTO -C-OMBINADO
	@Column(name = "TIPO", nullable = false, length = 1)
	private String tipo;
	
	@Column(name = "fracionado", nullable = true, length = 1)
	private boolean fracionado;
	
	@Column(name = "ATUALIZA_ESTOQUE", nullable = true, length = 1)
	private boolean atualizaEstoque;
	
	@Column(name = "quantidade_caixa")
	private BigDecimal quantidadeCaixa = BigDecimal.ZERO;
	
	@Column(name = "perc_desconto_maximo", precision = 12, scale = 2)
	private BigDecimal percDescontoMaximo = BigDecimal.ZERO;	
	
	@Column(name = "valor_compra_unidade", precision = 12, scale = 2)
	private BigDecimal valorCompraUnidade = BigDecimal.ZERO;
	
	@Column(name = "valor_compra_caixa", precision = 12, scale = 2)
	private BigDecimal valorCompraCaixa = BigDecimal.ZERO;
	
	@Column(name = "valor_venda_unidade", precision = 12, scale = 2)
	private BigDecimal valorVendaUnidade = BigDecimal.ZERO;
	
	@Column(name = "valor_venda_caixa", precision = 12, scale = 2)
	private BigDecimal valorVendaCaixa = BigDecimal.ZERO;
	
	@Column(name = "valor_producao_unidade", precision = 12, scale = 2)
	private BigDecimal valorProducaoUnidade = BigDecimal.ZERO;
	
	@Column(name = "valor_producao_caixa", precision = 12, scale = 2)
	private BigDecimal valorProducaoCaixa = BigDecimal.ZERO;
	
	@Column(name = "quantidade_estoque_unidade", precision = 12, scale = 2)
	private BigDecimal qtdEstoqueUnidade = BigDecimal.ZERO;
	
	@Column(name = "quantidade_estoque_caixa", precision = 12, scale = 2)
	private BigDecimal qtdEstoqueCaixa = BigDecimal.ZERO;
	
	@Column(name = "estoque_minimo_unidade", precision = 12, scale = 2)
	private BigDecimal estoqueMinimoUnidade = BigDecimal.ZERO;
	
	@Column(name = "estoque_minimo_caixa", precision = 12, scale = 2)
	private BigDecimal estoqueMinimoCaixa = BigDecimal.ZERO;
	
	@Column(name = "valor_nf_unidade", precision = 12, scale = 2)
	private BigDecimal valorNfUnidade = BigDecimal.ZERO;
	
	@Column(name = "valor_nf_caixa", precision = 12, scale = 2)
	private BigDecimal valorNfCaixa = BigDecimal.ZERO;
	
	@Column(name = "peso_unidade", precision = 12, scale = 2)
	private BigDecimal pesoUnidade = BigDecimal.ZERO;
	
	@Column(name = "peso_caixa", precision = 12, scale = 2)
	private BigDecimal pesoCaixa = BigDecimal.ZERO;

	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
	@Transient
	public String getTipoProdutoEnum(){
		if (!StringUtils.isNullOrEmpty(tipo)) {
	        switch (tipo)
	        {
	            case "P":
	                return TipoProdutoEnum.PRODUTO.getNome();
	            case "I":
	                return TipoProdutoEnum.INSUMO.getNome();
	            case "C":
	                return TipoProdutoEnum.COMBINADO.getNome();             
	            default:
	                return TipoProdutoEnum.PRODUTO.getNome();
	        } 
		}
		
		return "";
    }   
	
	@Transient
	private List<ProdutoItemEntity> produtoItemsList;
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutoEntity)) {
            return false;
        }
        ProdutoEntity other = (ProdutoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.ProdutoEntity[ id=" + id + " ]";
    }
    
}
