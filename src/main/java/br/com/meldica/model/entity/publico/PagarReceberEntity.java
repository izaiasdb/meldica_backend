package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "pagar_receber", catalog = SistemaConst.DATABASE, schema = "public")
public class PagarReceberEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_pagar_receber", sequenceName="public.seq_pagar_receber", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_pagar_receber")
    @Column(name = "id")
    private Integer id;
	
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;  
    
    @ManyToOne
    @JoinColumn(name = "id_fornecedor")
    private FornecedorEntity fornecedor;  
    
    @ManyToOne
    @JoinColumn(name = "id_plano_conta")
    private PlanoContaEntity planoConta;    
    
    @Column(name = "id_ordem_servico_forma_condicao_pagamento")
    private Integer idOrdemServicoFormaCondicaoPagamento;
    
    @Column(name = "id_ordem_pagamento_forma_condicao_pagamento")
    private Integer idOrdemPagamentoFormaCondicaoPagamento;
    
    @Column(name = "receita_despesa", nullable = false, length = 1)
    private String receitaDespesa;
    
    @Column(name = "selecionado")
    private boolean selecionado;
    
    @Column(name = "cancelado")
    private boolean cancelado;
    
    @Column(name = "competencia")
    private String competencia;
    
    @Column(name = "documento")
    private String documento;    

    @Column(name = "descricao")
    private String descricao; 
    
    @Column(name = "observacao")
    private String observacao;    

	@Column(name = "valor", precision = 12, scale = 4)
	private BigDecimal valor;
	
	@Column(name = "valor_pago", precision = 12, scale = 4)
	private BigDecimal valorPago;
	
//	@Temporal(TemporalType.TIMESTAMP)
	@Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_vencimento", insertable = true, updatable = true)
	private Date dataVencimento;	
	
	@Column(name = "id_usuario_inclusao", insertable = true, updatable = false)
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
	@Transient
	private String ano;
	
	@Transient
	private String mes;
	
	@Transient
	private List<Date> periodoVencimento = new ArrayList<>();
	
//	@Transient
//	private List<Date> periodo;
	
	//A-BERTA, P-AGA
	@Transient
	private String status; 
	
	@Transient
	private Integer idOrdemServico;	
//	@Transient
//	public String getAno() {
//		try {
//			return this.competencia.substring(2, 6);
//		} catch (Exception e) {
//		}
//		
//		return "";
//	}
//	
//	@Transient
//	public String getMes() {
//		try {
//			return this.competencia.substring(0, 2);
//		} catch (Exception e) {
//		}
//		
//		return "";
//	}
	
//	public void setCompetencia(String competencia) {
//		if (!StringUtils.isNullOrEmpty(getMes()) && !StringUtils.isNullOrEmpty(getAno())) {
//			this.competencia = StringUtils.lpad(getMes(), 2, '0') + getAno();	
//		}
//	}
	
	/*
	 * 0 -> Lançamento Normal
	 * 1 -> Lançamento á vista
	 * 2 -> Lançamento várias parcelas
	 */
	@Transient
	private Integer tipoLancamento;
	
	@Transient
	private Integer parcelas;
	
	@Transient
	private FormaCondicaoPagamentoEntity formaCondicaoPagamento;  	
		
	@Transient
	private List<PagarReceberItemEntity> pagarReceberItemsList = new ArrayList<>();
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagarReceberEntity)) {
            return false;
        }
        PagarReceberEntity other = (PagarReceberEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.PagarReceberEntity[ id=" + id + " ]";
    }
    
    
}
