package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.entity.dominio.CargoEntity;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "funcionario", catalog = SistemaConst.DATABASE, schema = "public")
public class FuncionarioEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_funcionario", sequenceName="public.seq_funcionario", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario")
    @Column(name = "id")
    private Integer id;
	
    @ManyToOne
    @JoinColumn(name = "id_cargo")
    private CargoEntity cargo;  		
	
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "ativo")
    private boolean ativo;

    @Column(name = "email")
    private String email;
    
    @Column(name = "cpf")
    private String cpf;    
    
    @Column(name = "observacao")
    private String observacao;
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;
	
	@Transient
	private List<FuncionarioEnderecoEntity> funcionarioEnderecoList;
	
	@Transient
	private List<FuncionarioTelefoneEntity> funcionarioTelefoneList;	
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FuncionarioEntity)) {
            return false;
        }
        FuncionarioEntity other = (FuncionarioEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.FuncionarioEntity[ id=" + id + " ]";
    }
    
}
