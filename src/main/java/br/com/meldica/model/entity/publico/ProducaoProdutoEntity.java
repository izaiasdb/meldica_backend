package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "producao_produto", catalog = SistemaConst.DATABASE, schema = "public")
public class ProducaoProdutoEntity implements Serializable, Comparable<ProducaoProdutoEntity>{
    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_producao_produto", sequenceName="public.seq_producao_produto", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_producao_produto")
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_producao")
    private Integer idProducao;	
	
    @ManyToOne
    @JoinColumn(name = "id_produto")
    private ProdutoEntity produto;  
    
//    @Column(name = "id_empresa_produto")
//    private Integer idEmpresaProduto;

    @Column(name = "cancelado")
    private boolean cancelado;
    
    @Column(name = "quantidade")
    private BigDecimal quantidade;
	
	@Column(name = "quantidade_na_caixa")
	private BigDecimal quantidadeNaCaixa;
    
//	@Column(name = "valor_producao", precision = 12, scale = 4)
//	private BigDecimal valorProducao;
	
	@Column(name = "fracionado")
	private boolean fracionado;
	
	@Column(name = "nome_produto")
	private String nomeProduto;		
	
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
	public ProducaoProdutoEntity() {
		
	}
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProducaoProdutoEntity)) {
            return false;
        }
        ProducaoProdutoEntity other = (ProducaoProdutoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(ProducaoProdutoEntity o) {
        return this.getId().compareTo(o.getId());
    }
}
