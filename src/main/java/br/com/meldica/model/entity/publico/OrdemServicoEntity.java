package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.enums.EmpresaEnum;
import br.com.meldica.model.enums.StatusNotaEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import br.com.meldica.utils.StringUtils;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "ordem_servico", catalog = SistemaConst.DATABASE, schema = "public")
public class OrdemServicoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_ordem_servico", sequenceName="public.seq_ordem_servico", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ordem_servico")
    @Column(name = "id")
    private Integer id;
	
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;  	
    
    @ManyToOne
    @JoinColumn(name = "id_funcionario")
    private FuncionarioEntity funcionario;  
    
    @ManyToOne
    @JoinColumn(name = "id_tabela_preco")
    private TabelaPrecoEntity tabelaPreco;
    
    @ManyToOne
    @JoinColumn(name = "id_plano_conta")
    private PlanoContaEntity planoConta;
    
    @Column(name = "id_cliente_endereco")
    private Integer idClienteEndereco;
    
    @Column(name = "status_nota", nullable = false, length = 1)
    private String statusNota;
    
    @Column(name = "cancelado")
    private boolean cancelado;
    
	@Column(name = "valor_pago", precision = 12, scale = 4)
	private BigDecimal valorPago;
	
	@Column(name = "desconto", precision = 12, scale = 4)
	private BigDecimal desconto;
	
	@Column(name = "observacao")
	private String observacao;	
	
	//@Temporal(TemporalType.TIMESTAMP)
	@Temporal(TemporalType.DATE)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_venda")
	private Date dataVenda;
	
	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_previsao_entrega")
	private Date dataPrevisaoEntrega;

	/*
	 * Data liberação da mercadoria para entrega
	 */
	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_liberacao")
	private Date dataLiberacao;
	
	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_entrega")
	private Date dataEntrega;
	
	@Column(name = "id_tipo_endereco")
    private Integer idTipoEndereco;    
	
    @Column(name = "logradouro")
    private String logradouro;
    
    @Column(name = "bairro")
    private String bairro;    
    
    @Column(name = "cidade")
    private String cidade;      

    @Column(name = "uf")
    private String uf;
    
    @Column(name = "cep")
    private String cep;
    
    @Column(name = "complemento")
    private String complemento;
    
    @Column(name = "numero")
    private String numero;  
    
	@Column(name = "id_cliente_tabela_preco")
	private Integer idClienteTabelaPreco;	
	
	@Column(name = "id_cliente_razao")
	private Integer idClienteRazao;	
    
	@Column(name = "id_usuario_inclusao", updatable = false)
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
    @Column(name = "estoque_gerado")
    private boolean estoqueGerado;
	
    @Column(name = "forma_gerada")
    private boolean formaGerada;
    
    @Column(name = "nf_meldica")
    private String nfMeldica;    
    
    @Column(name = "nf_cosmetico")
    private String nfCosmetico; 
    
    // mudar para statusDescricao
	@Transient
	public String getStatusNovaDescricao(){	 
		if (!StringUtils.isNullOrEmpty(statusNota)) {
			return StatusNotaEnum.obterPorId(statusNota).getDescricao();	
		}
		
		return "";
    } 
	
	@Transient
	private String dataInicio;
	
	@Transient
	private List<Date> periodoVenda = new ArrayList<>();
	
	@Transient
	private List<Date> periodoLiberacao = new ArrayList<>();
	
	@Transient
	private List<Date> periodoPrevisaoEntrega = new ArrayList<>();
	
	@Transient
	private List<Date> periodoEntrega = new ArrayList<>();
	
	@Transient
	private List<String> statusNotaList = new ArrayList<>();	
	
	@Transient
	private List<OrdemServicoStatusEntity> ordemServicoStatusList = new ArrayList<>();
	
	@Transient
	private List<OrdemServicoProdutoEntity> produtoItemsList = new ArrayList<>();
	
	@Transient
	private List<OrdemServicoProdutoEntity> produtoCosmeticoList = new ArrayList<>();
	
	/*
	 * Usado no pedido, para separar dos produtos.
	 */
	@Transient
	private List<OrdemServicoProdutoEntity> bonificacaoItemsList = new ArrayList<>();
	
	@Transient
	private List<OrdemServicoFormaPagamentoEntity> formaItemsList = new ArrayList<>();
	
	@Transient
	private List<OrdemServicoTransportadoraEntity> transportadoraItemsList = new ArrayList<>();
    
	@Transient
	private List<OrdemServicoKitEntity> kitList = new ArrayList<>();

	@Transient
	private List<OrdemServicoKitProdutoEntity> kitProdutoList = new ArrayList<>();
	
	@Transient
	private List<PagarReceberEntity> pagarReceberList = new ArrayList<>();
	
	@Transient
    private ClienteRazaoEntity clienteRazao;
	
//	@Transient
//	private BigDecimal totalProdutos = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalProdutoDesconto = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal totalProdutoBonificacao = BigDecimal.ZERO;	
	
//	@Transient
//	private BigDecimal totalFrete = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalForma = BigDecimal.ZERO;
	
	// Total de forma informado de frete
//	@Transient
//	private BigDecimal totalFormaFrete = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal totalFaltaForma = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalFormaDesconto = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalFormaDescontoCartao = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalFormaFreteDescontoCartao = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalPedido = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalPedidoComDesconto = BigDecimal.ZERO;	
	
//	@Transient
//	private BigDecimal totalPeso = BigDecimal.ZERO;
//	
//	@Transient
//	private BigDecimal totalVolume = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalDuzias = BigDecimal.ZERO;	
//	
//	@Transient
//	private BigDecimal totalCaixa = BigDecimal.ZERO;
	
//	@Transient
//	private BigDecimal totalNF = BigDecimal.ZERO;

	@Transient
	private BigDecimal totalGerado = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal totalPago = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal faltaReceber = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal totalGeradoFrete = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal totalPagoFrete = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal faltaReceberFrete = BigDecimal.ZERO;
	
	// Filtro do resumo
	@Transient
	private String tipoRelatorio;
	
//	public void setarTotais() {
//		BigDecimal totProd = BigDecimal.ZERO;
//		BigDecimal totPesoProd = BigDecimal.ZERO;
//		BigDecimal totPesoKit = BigDecimal.ZERO;
//		BigDecimal totVolumeProd = BigDecimal.ZERO;
////		BigDecimal totCaixaProd = BigDecimal.ZERO;
//		Integer totVolumeKit = kitList.size();
//		BigDecimal totFrete = BigDecimal.ZERO;
//		BigDecimal totNFCaixa = BigDecimal.ZERO;
//		BigDecimal totNFUnidade = BigDecimal.ZERO;
//		BigDecimal totNFKit = BigDecimal.ZERO;
//		totalNF = BigDecimal.ZERO;
//		totalDuzias = getTotalDuzias();
		
//		try {
//			List<OrdemServicoKitProdutoEntity> tempList = kitProdutoList.stream().filter(o -> !o.isBonificacao()).collect(Collectors.toList()); 
//			
//			if (tempList != null) {
//				for (OrdemServicoKitProdutoEntity item : tempList) {
//					totProd = totProd.add(item.getTotal()); 
//					totalProdutoDesconto = totalProdutoDesconto.add(item.getDesconto().multiply(item.getQuantidadeUnidade()));	
//				}
//			}
//		} catch (Exception e) {
//			System.out.println("Falha ao calcular total produtos");
//		}
		
//		this.totalProdutos = totProd; // temp
//		this.totalCaixa = totCaixaProd; // temp
//		this.totalFrete = totFrete; // temp
//		this.totalForma = totForma; // temp
//		this.totalPedido = totProd.add(totFrete); // temp
//		this.totalPedidoComDesconto = totalPedido.subtract(totalFormaDesconto);  // temp
		//this.faltaReceber = totalPedido - (this.valorPago - totalFormaDescontos); // já tava coment
		
//		this.faltaReceber = totalPedido.subtract((this.valorPago.subtract(totalFormaDesconto)));   // temp
//		this.faltaReceber = faltaReceber.subtract(totalFormaDescontoCartao);   // temp
		//this.totalNF = totNFUnidade + totNFCaixa + totNFKit; // já tava coment
//		this.totalNF = totalNF.add(totNFUnidade); // temp
//		this.totalNF = totalNF.add(totNFCaixa); // temp
//		this.totalNF = totalNF.add(totNFKit); // temp
//        this.totalFaltaForma = totalPedido.subtract(totalForma); // temp
//        this.totalVolume = totVolumeProd.add(BigDecimal.valueOf(totVolumeKit));
//        this.totalPeso = totPesoProd.add(totPesoKit);
//	}
	
	public BigDecimal getTotalPedido() {
		BigDecimal total = BigDecimal.ZERO;
		total = total.add(getTotalProdutos());
		total = total.add(getTotalFrete());
		
		return total;
	}
	
	public BigDecimal getTotalPedidoComDesconto() {
		BigDecimal total = BigDecimal.ZERO;
		total = total.add(getTotalPedido());
		total = total.subtract(getTotalFormaDesconto());
		
		return total;
	}
	
	public BigDecimal getTotalVolume() {
		BigDecimal total = BigDecimal.ZERO;
		total = total.add(getTotalVolumeNaturalEncapsulado());
		total = total.add(getTotalVolumeCosmetico());
		total = total.add(getTotalVolumeBonificacao());
		
		if (this.kitList != null && this.kitList.size() > 0) {
			total = total.add(new BigDecimal(kitList.size()));
		}			

		return total;
	}
	
	/**
	 * Se necessário fica com 8.5 cx
	 * @return
	 */
	public BigDecimal getTotalVolumeNaturalEncapsulado() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
			BigDecimal sum = produtoItemsList.stream()
					.filter(c-> c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())
							|| c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
//			BigDecimal sum = produtoItemsList.stream()
//					.filter(c-> !c.isFracionado() && c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())
//							|| c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
//					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO,
//					BigDecimal::add);
			
//			BigDecimal sumFracionado = produtoItemsList.stream()
//					.filter(c-> c.isFracionado() && c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())
//							|| c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
////					.map(x -> x.getQuantidadeUnidade().multiply(x.getQuantidadeNaCaixa())).reduce(BigDecimal.ZERO,
//					.map(x -> x.getQuantidadeNaCaixa().divide(x.getQuantidadeUnidade())).reduce(BigDecimal.ZERO,
//					BigDecimal::add);		
			
			total = total.add(sum);
//			total = total.add(sum).add(sumFracionado);
		}
		
		return total;
	}
	
	public BigDecimal getTotalVolumeCosmetico() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal sum = produtoCosmeticoList.stream()
					.filter(c-> c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(sum);
		}
		
		return total;
	}
	
	public BigDecimal getTotalVolumeBonificacao() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.bonificacaoItemsList != null && this.bonificacaoItemsList.size() > 0) {
			BigDecimal sum = bonificacaoItemsList.stream()					
					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO, BigDecimal::add);
			
			total = total.add(sum);
		}
		
		return total;
	}
	
	/**
	 * Retorna o peso dos produtos que é feito apenas em unidades.
	 * @return
	 */
	public BigDecimal getTotalPeso() {
		BigDecimal total = BigDecimal.ZERO;
		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
			BigDecimal sum = produtoItemsList.stream()
//					.filter(c-> c.isFracionado())
					.map(x -> x.getQuantidadeUnidade().multiply(x.getPesoUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal sum = produtoCosmeticoList.stream()
//					.filter(c-> c.isFracionado())
					.map(x -> x.getQuantidadeUnidade().multiply(x.getPesoUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);			
			
			total = total.add(sum);
		}
		
		if (this.bonificacaoItemsList != null && this.bonificacaoItemsList.size() > 0) {
			BigDecimal sum = bonificacaoItemsList.stream()
//					.filter(c-> c.isFracionado())
					.map(x -> x.getQuantidadeUnidade().multiply(x.getPesoUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);			
			
			total = total.add(sum);
		}		
		
		if (this.kitProdutoList != null && this.kitList.size() > 0) {
			BigDecimal sum = kitProdutoList.stream()
					.map(x -> x.getQuantidadeUnidade().multiply(x.getPesoUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}	
		
		return total;
	}
	
	public BigDecimal getTotalFrete() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.transportadoraItemsList != null && this.transportadoraItemsList.size() > 0) {
			total = total.add(transportadoraItemsList.stream().map(x -> x.getValorFrete()).reduce(BigDecimal.ZERO,
					BigDecimal::add));
		}		

		return total;
	}
	
	public BigDecimal getTotalNF() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
			BigDecimal count = produtoItemsList.stream()
					.filter(c-> !c.isFracionado())
					.map(x -> x.getQuantidadeCaixa().multiply(x.getValorNfCaixa())).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			BigDecimal countFracionado = produtoItemsList.stream()
					.filter(c-> c.isFracionado())
					.map(x -> x.getQuantidadeUnidade().multiply(x.getValorNfUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);			
			
			total = total.add(count).add(countFracionado);
		}
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal count = produtoCosmeticoList.stream()
					.filter(c-> !c.isFracionado())
					.map(x -> x.getQuantidadeCaixa().multiply(x.getValorNfCaixa())).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			BigDecimal countFracionado = produtoCosmeticoList.stream()
					.filter(c-> c.isFracionado())
					.map(x -> x.getQuantidadeUnidade().multiply(x.getValorNfUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);			
			
			total = total.add(count).add(countFracionado);
		}	
		
		if (this.kitList != null && this.kitList.size() > 0) {
			BigDecimal count = kitList.stream()
					.map(x -> x.getValorNf()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(count);
		}		

		return total;
	}
	
	// 09/09/2021 -> Tentar deixar o código aqui
//	public BigDecimal getFaltaReceber() {
//		BigDecimal total = BigDecimal.ZERO;
//		BigDecimal valorPago = this.valorPago != null ? this.valorPago : BigDecimal.ZERO;
//		
//		total = getTotalPedido().subtract((valorPago.subtract(getTotalFormaDesconto())));
//		total = total.subtract(getTotalFormaDescontoCartao());
////		this.faltaReceber = totalPedido.subtract((this.valorPago.subtract(totalFormaDesconto)));
////		this.faltaReceber = faltaReceber.subtract(totalFormaDescontoCartao);
//		
//		return total;
//	}
	
	public BigDecimal getTotalForma() {
		BigDecimal total = BigDecimal.ZERO;

		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("P"))
					.map(x -> x.getValor()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}		
		
		return total;
	}
	
	public BigDecimal getTotalFormaFrete() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("F"))
					.map(x -> x.getValor()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}		
		
		return total;
	}	
	
	public BigDecimal getTotalFormaFreteDesconto() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("F"))
					.map(x -> x.getDesconto()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}		
		
		return total;
	}		
	
	public BigDecimal getTotalFormaFreteDescontoCartao() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("F"))
					.map(x -> x.getDescontoFormaCondicao()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}		
		
		return total;
	}	
	
	public BigDecimal getTotalFormaDesconto() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("P"))
					.map(x -> x.getDesconto()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}	
		
		return total;
	}
	
	public BigDecimal getTotalFormaDescontoCartao() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.formaItemsList != null && this.formaItemsList.size() > 0) {
			return formaItemsList.stream()
					.filter(o -> o.getTipoForma().equals("P"))
					.map(x -> x.getDescontoFormaCondicao()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
		}	
		
		return total;
	}
	
	/**
	 * As bonificações não entra na somatória das dúzias. 
	 * @return
	 */
	public BigDecimal getTotalDuzias() {
		BigDecimal total = BigDecimal.ZERO;

		total = total.add(getTotalDuziasNaturais());
		total = total.add(getTotalDuziasCosmeticos());
		
		return total;
		
//			if (this.bonificacaoItemsList != null && this.bonificacaoItemsList.size() > 0) {
//				BigDecimal count = bonificacaoItemsList.stream().map(x -> x.getQuantidadeUnidade()).reduce(BigDecimal.ZERO,
//						BigDecimal::add);
//				
//				duzias = duzias.add(count.divide(BigDecimal.valueOf(12)));
//			}			
	}
	
	public BigDecimal getTotalDuziasNaturais() {
		BigDecimal duzias = BigDecimal.ZERO;

		try {
			if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
				//long count = produtoItemsList.stream().mapToLong(OrdemServicoProdutoEntity::getQuantidadeUnidade).
				BigDecimal count = produtoItemsList.stream().map(x -> x.getQuantidadeUnidade()).reduce(BigDecimal.ZERO,
						BigDecimal::add);
				
				duzias = duzias.add(count.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return duzias;
	}
	
	public BigDecimal getTotalDuziasCosmeticos() {
		BigDecimal duzias = BigDecimal.ZERO;

		try {
			if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
				BigDecimal count = produtoCosmeticoList.stream().map(x -> x.getQuantidadeUnidade()).reduce(BigDecimal.ZERO,
						BigDecimal::add);
				
				duzias = duzias.add(count.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP));
			}		
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return duzias;
	}
	
	/**
	 * Se for bonificação não soma, pq é de graça. Já com o valor do desconto.
	 * @return
	 */
	public BigDecimal getTotalProdutos() {
		BigDecimal total = BigDecimal.ZERO;

		total = total.add(getTotalProdutosNaturaisEncapsulados());
		total = total.add(getTotalProdutosCosmeticos());
		total = total.add(getTotalProdutosKit());
		
		return total;
	}
	
	public BigDecimal getTotalProdutosNaturaisEncapsulados() {
		BigDecimal total = BigDecimal.ZERO;

		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
			BigDecimal sum = produtoItemsList.stream().map(x -> x.getTotal()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(sum);
		}
		
		return total;
	}
	
	public BigDecimal getTotalProdutosCosmeticos() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal sum = produtoCosmeticoList.stream().map(x -> x.getTotal()).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(sum);
		}	
		
		return total;
	}
	
	public BigDecimal getTotalProdutosKit() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.kitProdutoList != null && this.kitProdutoList.size() > 0) {
			BigDecimal sum = kitProdutoList.stream()
					.filter(c -> !c.isBonificacao() )
					.map(x -> x.getTotal()).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}
		
		return total;
	}
	
	/**
	 * Pega o total de desconto que foi dado dos produtos
	 * @return
	 */
	public BigDecimal getTotalProdutoDesconto() {
		BigDecimal total = BigDecimal.ZERO;
		
		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
//			totalProdutoDesconto = totalProdutoDesconto.add(item.getDesconto().multiply(item.getQuantidadeUnidade()));
//			totalProdutoDesconto = totalProdutoDesconto.add(item.getDesconto().multiply(item.getQuantidadeCaixa()));

			BigDecimal sum = produtoItemsList.stream()
					.filter(c -> !c.isBonificacao() && !c.getProduto().isFracionado())
					.map(x -> x.getDesconto().multiply(x.getQuantidadeCaixa())).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
		
			BigDecimal sumFracionado = produtoItemsList.stream()
					.filter(c -> !c.isBonificacao() && c.getProduto().isFracionado())
					.map(x -> x.getDesconto().multiply(x.getQuantidadeUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(sum).add(sumFracionado);
		}
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal sum = produtoCosmeticoList.stream()
					.filter(c -> !c.isBonificacao() && !c.getProduto().isFracionado())
					.map(x -> x.getDesconto().multiply(x.getQuantidadeCaixa())).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
		
			BigDecimal sumFracionado = produtoCosmeticoList.stream()
					.filter(c -> !c.isBonificacao() && c.getProduto().isFracionado() && c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
					.map(x -> x.getDesconto().multiply(x.getQuantidadeUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);
			
			total = total.add(sum).add(sumFracionado);
		}
		
		if (this.kitProdutoList != null && this.kitProdutoList.size() > 0) {
			BigDecimal sum = kitProdutoList.stream()
					.filter(c -> !c.isBonificacao() )
					.map(x -> x.getDesconto().multiply(x.getQuantidadeUnidade())).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}	
		
		return total;
	}
	
	public BigDecimal getTotalCaixa() {
		BigDecimal total = BigDecimal.ZERO;
		
		// totCaixaProd = totCaixaProd.add(item.getQuantidadeCaixa());
		if (this.produtoItemsList != null && this.produtoItemsList.size() > 0) {
			BigDecimal sum = produtoItemsList.stream()
					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}
		
		if (this.produtoCosmeticoList != null && this.produtoCosmeticoList.size() > 0) {
			BigDecimal sum = produtoCosmeticoList.stream()
					.filter(c -> c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
					.map(x -> x.getQuantidadeCaixa()).reduce(BigDecimal.ZERO,
					BigDecimal::add);	
			
			total = total.add(sum);
		}
		
		return total;
	}
	
	public List<OrdemServicoProdutoEntity> getProdutosNaturaisEncapsulados(List<OrdemServicoProdutoEntity> ordemServicoProdutoList) {
		if (ordemServicoProdutoList != null && ordemServicoProdutoList.size() > 0) {
			List<OrdemServicoProdutoEntity> produtoList = ordemServicoProdutoList.stream()
					.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())
							|| c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
					.collect(Collectors.toList());
					
			produtoList = produtoList.stream()
		            .sorted(Comparator.<OrdemServicoProdutoEntity, String> comparing(c-> c.getProduto().getNome()))
		            		.collect(Collectors.toList());
			
			return produtoList;
		} 
		
		return new ArrayList<>();
	}
	
	public List<OrdemServicoProdutoEntity> getProdutosCosmeticos(List<OrdemServicoProdutoEntity> ordemServicoProdutoList) {
		if (ordemServicoProdutoList != null && ordemServicoProdutoList.size() > 0) {
			List<OrdemServicoProdutoEntity> produtoList = ordemServicoProdutoList.stream()
					.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
					.collect(Collectors.toList());	
			
			produtoList = produtoList.stream()
		            .sorted(Comparator.<OrdemServicoProdutoEntity, String> comparing(c-> c.getProduto().getNome()))
		            		.collect(Collectors.toList());
			
			return produtoList;			
		} 
		
		return new ArrayList<>();
	}
	
	public List<OrdemServicoProdutoEntity> getProdutosBonificacao(List<OrdemServicoProdutoEntity> ordemServicoProdutoList) {
		if (ordemServicoProdutoList != null && ordemServicoProdutoList.size() > 0) {
			List<OrdemServicoProdutoEntity> produtoList = ordemServicoProdutoList.stream()
					.filter(c -> c.isBonificacao())
					.collect(Collectors.toList());
			
			produtoList = produtoList.stream()
		            .sorted(Comparator.<OrdemServicoProdutoEntity, String> comparing(c-> c.getProduto().getNome()))
		            		.collect(Collectors.toList());
			
			return produtoList;				
		} 
		
		return new ArrayList<>();
	}	
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdemServicoEntity)) {
            return false;
        }
        OrdemServicoEntity other = (OrdemServicoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.OrdemServicoEntity[ id=" + id + " ]";
    }
    
}
