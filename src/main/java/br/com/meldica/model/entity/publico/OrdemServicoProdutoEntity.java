package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "ordem_servico_produto", catalog = SistemaConst.DATABASE, schema = "public")
public class OrdemServicoProdutoEntity implements Serializable, Comparable<OrdemServicoProdutoEntity>{
    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_ordem_servico_produto", sequenceName="public.seq_ordem_servico_produto", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ordem_servico_produto")
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_ordem_servico")
    private Integer idOrdemServico;	
	
    @ManyToOne
    @JoinColumn(name = "id_produto")
    private ProdutoEntity produto;  
    
    @Column(name = "id_tabela_preco")
    private Integer idTabelaPreco;
    
    @Column(name = "id_empresa_produto")
    private Integer idEmpresaProduto;

    @Column(name = "cancelado")
    private boolean cancelado;
    
    @Column(name = "bonificacao")
    private boolean bonificacao;
    
    @Column(name = "quantidade_unidade")
    private BigDecimal quantidadeUnidade;
	
	// Quantidade que de unidades em uma caixa de produto
	@Column(name = "quantidade_caixa")
	private BigDecimal quantidadeCaixa;
	
	@Column(name = "quantidade_na_caixa")
	private BigDecimal quantidadeNaCaixa;
    
	@Column(name = "valor_unidade", precision = 12, scale = 4)
	private BigDecimal valorUnidade;
	
	@Column(name = "valor_caixa", precision = 12, scale = 4)
	private BigDecimal valorCaixa;
	
	@Column(name = "valor_compra_unidade", precision = 12, scale = 4)
	private BigDecimal valorCompraUnidade;
	
	@Column(name = "valor_compra_caixa", precision = 12, scale = 4)
	private BigDecimal valorCompraCaixa;
	
	@Column(name = "valor_producao_unidade", precision = 12, scale = 4)
	private BigDecimal valorProducaoUnidade;
	
	@Column(name = "valor_producao_caixa", precision = 12, scale = 4)
	private BigDecimal valorProducaoCaixa;
	
	@Column(name = "valor_venda_unidade", precision = 12, scale = 4)
	private BigDecimal valorVendaUnidade;

	@Column(name = "valor_venda_caixa", precision = 12, scale = 4)
	private BigDecimal valorVendaCaixa;
	
	@Column(name = "valor_nf_unidade", precision = 12, scale = 4)
	private BigDecimal valorNfUnidade;
	
	@Column(name = "valor_nf_caixa", precision = 12, scale = 4)
	private BigDecimal valorNfCaixa;		
	
	@Column(name = "peso_unidade", precision = 12, scale = 4)
	private BigDecimal pesoUnidade;
	
	@Column(name = "peso_caixa", precision = 12, scale = 4)
	private BigDecimal pesoCaixa;

	// Desconto no produto
	@Column(name = "desconto", precision = 12, scale = 4)
	private BigDecimal desconto;

	@Column(name = "perc_desconto", precision = 12, scale = 4)
	private BigDecimal percDesconto;	
	
	@Column(name = "total", precision = 12, scale = 4)
	private BigDecimal total;	
	
	@Column(name = "fracionado")
	private boolean fracionado;
	
	@Column(name = "nome_produto")
	private String nomeProduto;		
	
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Column(name = "id_usuario_alteracao")
	private Integer idUsuarioAlteracao;	

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_alteracao")
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
    private Date dataAtualizacao;	
	
	public OrdemServicoProdutoEntity() {
		
	}
	
	@Transient
	public String getBonificacaoDescricao() {
		return this.bonificacao ? "SIM" : "NÃO";
	}
	
	public BigDecimal getDuzias() {
		if (quantidadeUnidade != null) {
			try {
				return quantidadeUnidade.divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP);	
			} catch (Exception e) {
				return BigDecimal.ZERO;
			}
		}
		
		return BigDecimal.ZERO;
	}
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdemServicoProdutoEntity)) {
            return false;
        }
        OrdemServicoProdutoEntity other = (OrdemServicoProdutoEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(OrdemServicoProdutoEntity o) {
        return this.getId().compareTo(o.getId());
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.OrdemServicoProdutoEntity[ id=" + id + " ]";
    }
    
}
