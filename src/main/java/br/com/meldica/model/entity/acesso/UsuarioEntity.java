package br.com.meldica.model.entity.acesso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.utils.JsonDateTimeDeserializer;
import br.com.meldica.utils.JsonDateTimeSerializer;
import lombok.Data;

@Data
@Entity
@Table(name = "USUARIO", catalog = SistemaConst.DATABASE, schema = "acesso")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioEntity implements Serializable {

	private static final long serialVersionUID = -4676847477552095546L;
	
	@Id
	@SequenceGenerator(name="seq_usuario", sequenceName="acesso.seq_usuario", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario")
    @Column(name = "id", unique=true, nullable = false)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "id_perfil")
    private PerfilEntity perfil;
    
    @ManyToOne
    @JoinColumn(name = "id_funcionario")
    private FuncionarioEntity funcionario;
    
    @Column(name = "nome", nullable = false)
    private String nome;
    
    @Column(name = "login", nullable = false)
    private String login;
    
    @Column(name = "senha", nullable = false)
    private String senha;
    
    @Column(name = "ativo", nullable = false)
    private Boolean ativo;

    @Column(name = "trocar_senha", nullable = false)
    private Boolean trocaSenha;      
    
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)    
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
    
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)    
	@Temporal(TemporalType.DATE)
	@Column(name = "data_alteracao")
	private Date dataAlteracao;
    
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)    
	@Temporal(TemporalType.DATE)
	@Column(name = "data_ultimo_acesso")
	private Date dataUltimoAcesso;

    @Column(name = "id_usuario_inclusao", nullable = false)
    private Integer idUsuarioInclusao;
    
    @Column(name = "id_usuario_alteracao")
    private Integer idUsuarioAlteracao;
    
    @Column(name = "desenvolvedor", nullable = false)
    private Boolean desenvolvedor;

    @Column(name = "email")
    private String email;

    @Column(name = "codigo_verificacao")
    private String codigoVerificacao;
    
    @Transient
    private String senhaAtual;
    
    @Transient
    private List<String> permissoes = new ArrayList<String>();
    
    @Transient
    private List<Long> unidadeIds = new ArrayList<Long>();
    
    public UsuarioEntity() {
    	this.desenvolvedor = true;
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioEntity other = (UsuarioEntity) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (dataInclusao == null) {
			if (other.dataInclusao != null)
				return false;
		} else if (!dataInclusao.equals(other.dataInclusao))
			return false;
		if (dataUltimoAcesso == null) {
			if (other.dataUltimoAcesso != null)
				return false;
		} else if (!dataUltimoAcesso.equals(other.dataUltimoAcesso))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idUsuarioAlteracao == null) {
			if (other.idUsuarioAlteracao != null)
				return false;
		} else if (!idUsuarioAlteracao.equals(other.idUsuarioAlteracao))
			return false;
		if (idUsuarioInclusao == null) {
			if (other.idUsuarioInclusao != null)
				return false;
		} else if (!idUsuarioInclusao.equals(other.idUsuarioInclusao))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (trocaSenha == null) {
			if (other.trocaSenha != null)
				return false;
		} else if (!trocaSenha.equals(other.trocaSenha))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((dataInclusao == null) ? 0 : dataInclusao.hashCode());
		result = prime * result + ((dataUltimoAcesso == null) ? 0 : dataUltimoAcesso.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idUsuarioAlteracao == null) ? 0 : idUsuarioAlteracao.hashCode());
		result = prime * result + ((idUsuarioInclusao == null) ? 0 : idUsuarioInclusao.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((trocaSenha == null) ? 0 : trocaSenha.hashCode());
		return result;
	}
    
}
