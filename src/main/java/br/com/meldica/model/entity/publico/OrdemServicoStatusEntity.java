package br.com.meldica.model.entity.publico;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.enums.StatusNotaEnum;
import br.com.meldica.utils.JsonDateDeserializer;
import br.com.meldica.utils.JsonDateSerializer;
import lombok.Data;

@Data
@Entity
@JsonInclude(Include.NON_EMPTY)
@Table(name = "ordem_servico_status", catalog = SistemaConst.DATABASE, schema = "public")
public class OrdemServicoStatusEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	@Id
	@SequenceGenerator(name="seq_ordem_servico_status", sequenceName="public.seq_ordem_servico_status", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ordem_servico_status")
    @Column(name = "id")
    private Integer id;

	@Column(name = "id_ordem_servico")
	private Integer idOrdemServico;
	
    @Column(name = "status_anterior")
    private String statusAnterior;
    
    @Column(name = "status_atual")
    private String statusAtual;
    
	@Column(name = "id_usuario_inclusao")
	private Integer idUsuarioInclusao;

	@Temporal(TemporalType.TIMESTAMP)
    @JsonSerialize(using = JsonDateSerializer.class)
	@JsonDeserialize(using = JsonDateDeserializer.class)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
	
	@Transient
	public String getStatusAnteriorDescricao(){	 
		return StatusNotaEnum.obterPorId(statusAnterior).getDescricao();
    }
	
	@Transient
	public String getStatusAtualDescricao(){	 
		return StatusNotaEnum.obterPorId(statusAtual).getDescricao();
    }
	
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdemServicoStatusEntity)) {
            return false;
        }
        OrdemServicoStatusEntity other = (OrdemServicoStatusEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.meldica.model.entity.OrdemServicoStatusEntity[ id=" + id + " ]";
    }
    
}
