package br.com.meldica.model.entity.acesso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.constants.SistemaConst;
import br.com.meldica.model.dto.utils.ValueObject;
import br.com.meldica.utils.JsonDateTimeDeserializer;
import br.com.meldica.utils.JsonDateTimeSerializer;
import lombok.Data;

@Data
@Entity
@Table(name = "PERFIL", catalog = SistemaConst.DATABASE, schema = "acesso")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerfilEntity implements Serializable {
	
	private static final long serialVersionUID = 3710235293478519828L;

	@Id
	@SequenceGenerator(name="seq_perfil", sequenceName="acesso.seq_perfil", allocationSize=1)
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil")
    @Column(name = "id", unique=true, nullable = false)
    private Integer id;

    @Column(name = "id_usuario_inclusao")
    private Integer idUsuarioInclusao;
    
    @Column(name = "id_usuario_alteracao")
    private Integer idUsuarioAlteracao;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "ativo")
    private Boolean ativo;
    
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)    
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inclusao", insertable = false, updatable = false)
	private Date dataInclusao;
    
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class)    
	@Temporal(TemporalType.DATE)
	@Column(name = "data_alteracao", insertable = false, updatable = false)
	private Date dataAlteracao;       
    
    @Transient
    private List<String> permissoes = new ArrayList<String>();    
    
    public PerfilEntity() {
    	
    }
    
	public PerfilEntity(Integer id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}	
    
    public ValueObject getPerfilDto() {
    	return new ValueObject(this.id, this.nome);
    }   
    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilEntity other = (PerfilEntity) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (dataInclusao == null) {
			if (other.dataInclusao != null)
				return false;
		} else if (!dataInclusao.equals(other.dataInclusao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idUsuarioInclusao == null) {
			if (other.idUsuarioInclusao != null)
				return false;
		} else if (!idUsuarioInclusao.equals(other.idUsuarioInclusao))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((dataInclusao == null) ? 0 : dataInclusao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idUsuarioInclusao == null) ? 0 : idUsuarioInclusao.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
    
    @Override
    public String toString() {
        return "br.gov.ce.sap.model.sca.acesso.PerfilEntity[ id = " + id + " ]";
    }

}
