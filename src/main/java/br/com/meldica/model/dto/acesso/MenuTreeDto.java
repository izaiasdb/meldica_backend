package br.com.meldica.model.dto.acesso;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;
import br.com.meldica.model.entity.acesso.UsuarioMenuEntity;
import br.com.meldica.model.enums.MenuPermissaoEnum;
import lombok.Data;

@Data
public class MenuTreeDto {
	
	private String title;
	private String key;
	private List<MenuTreeDto> children = new ArrayList<MenuTreeDto>();
	
	public MenuTreeDto(String key, String title) {
		this.key = key;
		this.title = title;
	}
	
	public static List<MenuTreeDto> obterMenuPorUsuarioMenu(List<UsuarioMenuEntity> usuarioMenus, Integer nivel){
		List<MenuEntity> menus = new ArrayList<MenuEntity>();
		
		if (usuarioMenus != null && usuarioMenus.size() > 0) {
			usuarioMenus.forEach(menuUsuario ->{
				menus.add(menuUsuario.getMenu());
			});
			
			return obterMenu(menus, null, 1);
		}
		
		return null;
	}
	
	public static List<MenuTreeDto> obterMenuPorPerfilMenu(List<PerfilMenuEntity> perfilMenus, Integer nivel){
		List<MenuEntity> menus = new ArrayList<MenuEntity>();
		
		if (perfilMenus != null && perfilMenus.size() > 0) {
			perfilMenus.forEach(menuUsuario ->{
				menus.add(menuUsuario.getMenu());
			});
			
			return obterMenu(menus, null, 1);
		}
		
		return null;
	}
	
	public static List<MenuTreeDto> obterMenu(List<MenuEntity> menuCompleto, List<MenuEntity> menus,  Integer nivel){
		List<MenuTreeDto> menuTree = new ArrayList<MenuTreeDto>();
		List<MenuEntity> menusNivel = new ArrayList<MenuEntity>();
		
		// Fazer o foreach do nível 1
		if (nivel == 1) {
			menus = new ArrayList<>();
			menus.addAll(new ArrayList<>(menuCompleto));
			menusNivel = menus.stream().filter(c-> c.getNivel() == nivel).collect(Collectors.toList());
		}
		else {
			menusNivel.addAll(new ArrayList<>(menus));
		}
		
		if (menusNivel.size() > 0) {
			for (MenuEntity c : menusNivel) {
//				if (c.getNome().equals("Ficha Advogado")) {
//					System.out.println("");
//				}
				
				MenuTreeDto menu = new MenuTreeDto(c.getId().toString(), c.getNome());
//				MenuTreeDto menu = new MenuTreeDto(c.getId().toString(), c.getAuthority());
				List<MenuEntity> menusFilhos = menuCompleto.stream().filter(f->
						f.getMenu() != null && 
						f.getMenu().getId() == c.getId()  && 
						f.getNivel() == nivel + 1).collect(Collectors.toList());
				
				if (c.getSomaPermissao() != null){
					MenuPermissaoEnum.listarPorSomaPermissao(c.getSomaPermissao()).forEach(p ->{
						menu.children.add(new MenuTreeDto(c.getId().toString()+"-"+p.getId(), p.getNome()));
					});					
				}
				
				if (menusFilhos.size() > 0) {
					menu.children.addAll(obterMenu(menuCompleto, menusFilhos, c.getNivel() + 1)); //Tava ok
				}
				
				menuTree.add(menu);
			};
		}
		
		return menuTree;
	}
	
	public static List<String> listarUsuarioPermissoes(List<UsuarioMenuEntity> usuarioMenus){
		List<String> permissoes = new ArrayList<String>();
		
		usuarioMenus.forEach(c-> {
			if (c.getSomaPermissao() > 0) {
				List<MenuPermissaoEnum> menuPermissoes = MenuPermissaoEnum.listarPorSomaPermissao(c.getSomaPermissao());
				
				menuPermissoes.forEach(m-> {
					permissoes.add(c.getMenu().getId() + "-" + m.getId());
				});
			
			} else {
//				permissoes.add(c.getMenu().getId().toString());
			}
		});
		
		return permissoes;
	}
	
	public static List<String> listarPerfilPermissoes(List<PerfilMenuEntity> perfilMenus){
		List<String> permissoes = new ArrayList<String>();
		
		perfilMenus.forEach(c-> {
			if (c.getSomaPermissao() > 0) {
				List<MenuPermissaoEnum> menuPermissoes = MenuPermissaoEnum.listarPorSomaPermissao(c.getSomaPermissao());
				
				menuPermissoes.forEach(m-> {
					permissoes.add(c.getMenu().getId() + "-" + m.getId());
				});
			
			} else {
//				permissoes.add(c.getMenu().getId().toString());
			}
		});
		
		return permissoes;
	}		
}
