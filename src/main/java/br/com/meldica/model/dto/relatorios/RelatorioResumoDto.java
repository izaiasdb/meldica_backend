package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import lombok.Data;

@Data
public class RelatorioResumoDto {

	private Integer id;
	private BigDecimal totalVenda = BigDecimal.ZERO;
	private BigDecimal totalProdutos = BigDecimal.ZERO;
	private BigDecimal totalDuzias = BigDecimal.ZERO;
	private BigDecimal totalDuziasEncapsulados = BigDecimal.ZERO;
	private BigDecimal totalFrete = BigDecimal.ZERO;
//	private BigDecimal comissao = BigDecimal.ZERO;
	// Gerado financeiro
	private BigDecimal totalGerado = BigDecimal.ZERO;
	private BigDecimal totalPago = BigDecimal.ZERO;
	private BigDecimal totalReceber = BigDecimal.ZERO;
	private BigDecimal totalGeradoFrete = BigDecimal.ZERO;
	private BigDecimal totalPagoFrete = BigDecimal.ZERO;
	private BigDecimal totalReceberFrete = BigDecimal.ZERO;
	private BigDecimal descontoProduto = BigDecimal.ZERO;
	private BigDecimal descontoForma = BigDecimal.ZERO;
	private BigDecimal descontoCartao = BigDecimal.ZERO;
	private BigDecimal descontoFormaFrete = BigDecimal.ZERO;
	private BigDecimal descontoCartaoFrete = BigDecimal.ZERO;
	private String campanha;
	private String tipoRelatorio;
    private String dataInicio;
    private String dataFim;
    private List<RelatorioResumoProdutoDto> produtoList = new ArrayList<>();
    private List<RelatorioResumoFormaDto> formaList = new ArrayList<>();
    
    private List<GrupoProdutoEntity> grupoProdutoList = new ArrayList<>();
    private List<OrdemServicoProdutoEntity> produtoItemList = new ArrayList<>();
    private List<OrdemServicoKitProdutoEntity> kitItemList = new ArrayList<>();
    private List<OrdemServicoFormaPagamentoEntity> formaItemList = new ArrayList<>();
    private List<PagarReceberEntity> pagarReceberList = new ArrayList<>();
    private List<PagarReceberEntity> pagarReceberFreteList = new ArrayList<>();
	
}
