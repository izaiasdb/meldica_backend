package br.com.meldica.model.dto.publico;

import java.util.Date;
import java.util.List;

import javax.persistence.Convert;

import br.com.meldica.model.enums.SexoEnum;
import br.com.meldica.utils.converter.SexoEnumEmptyConverter;
import lombok.Data;

@Data
public class FichaDto  {
	private Integer id;
	private String nome;
	private String mae;
	private String rg;
	private String cpf;	
	private Date dataNascimento;
	@Convert(converter = SexoEnumEmptyConverter.class)
	private SexoEnum sexo;
	private String tipoPessoa;
	private List<String> ufList;
	private List<String> naturalidade;
	private List<Long> nacionalidade;
					
}
