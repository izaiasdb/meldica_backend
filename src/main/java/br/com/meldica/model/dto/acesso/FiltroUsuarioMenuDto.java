package br.com.meldica.model.dto.acesso;

import lombok.Data;

@Data
public class FiltroUsuarioMenuDto {
	
	private Integer idUsuario;

	private Integer idPerfil;	
	
	private Integer idModulo;
	
	private Integer somaPermissao;
	
	private String permissao;
	
	public FiltroUsuarioMenuDto() {
		
	}

	public FiltroUsuarioMenuDto(Integer idUsuario, Integer idModulo) {
		super();
		this.idUsuario = idUsuario;
		this.idModulo = idModulo;
	}
	
	public FiltroUsuarioMenuDto(Integer idUsuario, Integer idModulo, Integer idPerfil) {
		super();
		this.idUsuario = idUsuario;
		this.idModulo = idModulo;
		this.idPerfil = idPerfil;		
	}	
}
