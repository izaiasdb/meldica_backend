package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RelatorioResumoFormaDto {

	private Integer id;
	private Integer idForma;
	private String forma;
	private BigDecimal valor = BigDecimal.ZERO;
	
	public RelatorioResumoFormaDto() {
		
	}
	
}
