package br.com.meldica.model.dto.publico;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.utils.JsonDateTimeDeserializer;
import br.com.meldica.utils.JsonDateTimeSerializer;
import lombok.Data;

@Data
public class DashboardDto {

	private Long id;
	
	// 1 - Empresa, 2 - Funcionário
	private Integer idTipoDashboard;
	
	private String descricao;
	
	private Integer idEmpresa;
	
	private String empresa;
	
	private Integer idFuncionario;
	
	private String funcionario;
	
	private BigDecimal valor = BigDecimal.ZERO;
	
	private BigDecimal caixa = BigDecimal.ZERO;
	
	//Quantidade de vendas
	private Integer venda = 0;
	
//    @JsonSerialize(using = JsonDateTimeSerializer.class)
//	@JsonDeserialize(using = JsonDateTimeDeserializer.class) 	
//	private Date periodo;
    
    private String data;
	
	public DashboardDto() {
		
	}
	
	public DashboardDto(Long id, String descricao, BigDecimal valor, BigDecimal caixa, Integer venda) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
		this.caixa = caixa;
		this.venda = venda;
	}
}
