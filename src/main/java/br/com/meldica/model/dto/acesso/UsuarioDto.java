package br.com.meldica.model.dto.acesso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.meldica.model.entity.acesso.PerfilEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;
import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.model.entity.publico.FuncionarioEntity;
import lombok.Data;

@Data
@JsonInclude(Include.NON_EMPTY)
public class UsuarioDto {
	private Integer id;
	private Integer sispenUsuarioid;
	private String nome;
	private String nomeAbreviado;
	private String login;
	private String matricula;
	private String email;
	private String token;
	private boolean	ativo;
	private Date dataUltimoAcesso;
	private boolean	desenvolvedor;
	private PerfilEntity perfil;
	private FuncionarioEntity funcionario;
	private List<SimpleGrantedAuthority> authorities;
//	private List<UsuarioMenuEntity> usuarioMenuList;
	private List<PerfilMenuEntity> perfilMenuList;
	
	/*
     * Montar o menu no cadastro de usuário, de acordo com o menu que o usuário tem.
     */
	private List<MenuTreeDto> menus = new ArrayList<MenuTreeDto>();	

	public UsuarioDto() {}
	
	@SuppressWarnings("unchecked")
	public UsuarioDto(UsuarioEntity usuario, String token, Authentication auth) {
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.login = usuario.getLogin();
		this.email = usuario.getEmail();
		this.token = token;
		this.desenvolvedor = usuario.getDesenvolvedor();
		this.authorities = (List<SimpleGrantedAuthority>) auth.getAuthorities();
		this.funcionario = usuario.getFuncionario();
	}
	
//	@SuppressWarnings("unchecked")
//	public UsuarioDto(UsuarioEntity usuario, 
//			String token, Authentication auth, List<UsuarioMenuEntity> usuarioMenuList) {
//		this.id = usuario.getId();
//		this.nome = usuario.getNome();
//		this.login = usuario.getLogin();
//		this.perfil = usuario.getPerfil();
//		this.token = token;
//		this.desenvolvedor = usuario.getDesenvolvedor();
//		this.authorities = (List<SimpleGrantedAuthority>) auth.getAuthorities();
//		this.usuarioMenuList = usuarioMenuList;
//	}
	
	@SuppressWarnings("unchecked")
	public UsuarioDto(UsuarioEntity usuario, 
			String token, Authentication auth, List<PerfilMenuEntity> perfilMenuList) {
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.login = usuario.getLogin();
		this.perfil = usuario.getPerfil();
		this.token = token;
		this.desenvolvedor = usuario.getDesenvolvedor();
		this.authorities = (List<SimpleGrantedAuthority>) auth.getAuthorities();
		this.perfilMenuList = perfilMenuList;
		this.menus =  MenuTreeDto.obterMenuPorPerfilMenu(perfilMenuList, 1);
		this.ativo = usuario.getAtivo();
		this.dataUltimoAcesso = usuario.getDataUltimoAcesso();
		this.funcionario = usuario.getFuncionario();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioDto other = (UsuarioDto) obj;
		if (authorities == null) {
			if (other.authorities != null)
				return false;
		} else if (!authorities.equals(other.authorities))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomeAbreviado == null) {
			if (other.nomeAbreviado != null)
				return false;
		} else if (!nomeAbreviado.equals(other.nomeAbreviado))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorities == null) ? 0 : authorities.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nomeAbreviado == null) ? 0 : nomeAbreviado.hashCode());
		return result;
	}

}
