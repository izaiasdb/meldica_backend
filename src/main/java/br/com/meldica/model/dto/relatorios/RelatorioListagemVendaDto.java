package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RelatorioListagemVendaDto {

	private Integer id;
	private BigDecimal totalProdutos = BigDecimal.ZERO;
	private BigDecimal totalDuzias = BigDecimal.ZERO;
	private BigDecimal totalDuziasEncapsulados = BigDecimal.ZERO;
	private String campanha;
	private String tipoRelatorio;
	private String cliente;
    private String dataInicio;
    private String dataFim;
}
