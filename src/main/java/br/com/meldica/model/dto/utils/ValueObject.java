package br.com.meldica.model.dto.utils;

import java.io.Serializable;

import lombok.Data;

@Data
public class ValueObject implements Serializable{
	private static final long serialVersionUID = 6274321419975372896L;

	private Integer id;
	private String descricao;
	
	public ValueObject() { }
	
	public ValueObject(Integer id) {
		this.id = id;		
	}
	
	public ValueObject(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;		
	}
	
}
