package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RelatorioEstoqueProdutoAcabadoDto {

	private String data;
	private Integer id;
	private String nome;
	private BigDecimal quantidade = BigDecimal.ZERO;
	private BigDecimal valor = BigDecimal.ZERO;
	private BigDecimal total = BigDecimal.ZERO;
	
}
