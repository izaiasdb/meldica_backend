package br.com.meldica.model.dto.acesso;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CredenciaisDto implements Serializable{

	private static final long serialVersionUID = 3822599940262561661L;
	
	private String username;
	private String password;
	private String email;
	private String novaSenha;
	private String confirmarSenha;
	private String codigoVerificacao;
	
//	public String getPassword() {
//		return password;
//	}
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	public String getUsername() {
//		return username;
//	}
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	
//	public String getEmail() {
//		return email;
//	}
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	
//	public String getNovaSenha() {
//		return novaSenha;
//	}
//	public void setNovaSenha(String novaSenha) {
//		this.novaSenha = novaSenha;
//	}
//	
//	public String getConfirmarSenha() {
//		return confirmarSenha;
//	}
//	public void setConfirmarSenha(String confirmarSenha) {
//		this.confirmarSenha = confirmarSenha;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CredenciaisDto other = (CredenciaisDto) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
}
