package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RelatorioResumoProdutoDto {

	private Integer id;
	private String tipoRelatorio;
	private Integer idGrupoProduto;
	private String grupoProduto;
	private Integer idProduto;
	private String produto;	
//	private BigDecimal quantidadeCaixa;
//	private BigDecimal quantidadeUnidade;
//	private BigDecimal valorUnidade;
//	private BigDecimal valorCaixa;
	private BigDecimal quantidade;
	private BigDecimal valor;
	private BigDecimal total;
	private BigDecimal estoque;
	private BigDecimal produzir;
	
	
	public RelatorioResumoProdutoDto() {
		
	}
	
}
