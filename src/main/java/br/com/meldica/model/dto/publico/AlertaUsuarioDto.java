package br.com.meldica.model.dto.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.utils.JsonDateTimeDeserializer;
import br.com.meldica.utils.JsonDateTimeSerializer;
import lombok.Data;

@Data
public class AlertaUsuarioDto {

	private Long id;
	
    @JsonSerialize(using = JsonDateTimeSerializer.class)
	@JsonDeserialize(using = JsonDateTimeDeserializer.class) 	
	private Date dataAlerta;
	
	private Long idUsuario;
	
	private String descricao;
	
	//TipoAlertaEnum
	private Integer idTipoAlerta;
	
	private String anexo;
	
	public AlertaUsuarioDto() {
		
	}
	
	public AlertaUsuarioDto(Long id, Date dataAlerta, Integer idTipoAlerta) {
		super();
		this.id = id;
		this.dataAlerta = dataAlerta;
		this.idTipoAlerta = idTipoAlerta;
	}
	
	public AlertaUsuarioDto(Long id, Date dataAlerta, String descricao) {
		super();
		this.id = id;
		this.dataAlerta = dataAlerta;
		this.descricao = descricao;
	}
	
	public AlertaUsuarioDto(Long id, Date dataAlerta, Integer idTipoAlerta, String descricao, String anexo) {
		super();
		this.id = id;
		this.dataAlerta = dataAlerta;
		this.idTipoAlerta = idTipoAlerta;
		this.descricao = descricao;
		this.anexo = anexo;
	}
	
	
	public static List<AlertaUsuarioDto> obterListAlertaOrdem(List<OrdemServicoEntity> entityList){
		List<AlertaUsuarioDto> list = new ArrayList<AlertaUsuarioDto>();
		Date date = new Date();
		
		if (entityList != null && entityList.size() > 0) {
			for (OrdemServicoEntity entity : entityList) {
				String descricao = "PEDIDO: " + entity.getId() + "-  CLIENTE: " + entity.getCliente().getNome() + " Status: " +  
						entity.getStatusNovaDescricao();
				
				list.add(new AlertaUsuarioDto(entity.getId().longValue(),date, descricao));				
			}
		}
		
		return list;
	}
//	
//	public static List<AlertaUsuarioDto> obterListAnexosNaoLidos(List<PessoaObjetoEntity> entityList){
//		List<AlertaUsuarioDto> list = new ArrayList<AlertaUsuarioDto>();
//		
//		if (entityList != null && entityList.size() > 0) {
//			for (PessoaObjetoEntity anexo : entityList) {
//				String descricao = (anexo.getTipoArquivo() != null && anexo.getTipoArquivo().getTipoClassificacaoArquivo() != null ? 
//						anexo.getTipoArquivo().getTipoClassificacaoArquivo().getDescricao() : "") +  
//						(anexo.getTipoArquivo() != null ? "| " + anexo.getTipoArquivo().getDescricao() : "") + 
//						(anexo.getPessoa() != null ? "| Pessoa: " + anexo.getPessoa().getNome() : "");
//
//				list.add(new AlertaUsuarioDto(anexo.getId(), anexo.getDataInclusao(), 
//						TipoAlertaEnum.DOCUMENTOS_NAO_LIDOS.getId(), descricao, anexo.getNome()));				
//			}
//		}
//		
//		return list;
//	}
	
}
