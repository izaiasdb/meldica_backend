package br.com.meldica.model.dto.relatorios;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class PagarReceberDto {

	private Long id;
	
	// Pago Recebido (P), A Pagar Receber(A)
	private String tipoRelatorio;
	
	private String receitaDespesa;
	
	private String numeroConta;
	
	private String planoConta;
	
	private BigDecimal valor = BigDecimal.ZERO;
	
	private BigDecimal valorFalta= BigDecimal.ZERO;
	
	private BigDecimal valorPago= BigDecimal.ZERO;
	
    private List<Date> periodo;	
    
    private String dataInicio;
    
    private String dataFim;
	
	public PagarReceberDto() {
		
	}
//	
//	public PagarReceberDto(Long id, String descricao, Float valor) {
//		super();
//		this.id = id;
//		this.valor = valor;
//	}
}
