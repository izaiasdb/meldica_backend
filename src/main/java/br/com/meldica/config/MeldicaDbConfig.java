package br.com.meldica.config;

import java.net.URISyntaxException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", 
					   transactionManagerRef = "transactionManager",
					   basePackages = {"br.com.meldica.model.dao"})
// Alterar SistemaConst
public class MeldicaDbConfig {
	@Value("${spring.datasource.meldica.jndi-name}")
    private String scaJndiName;

	private JndiDataSourceLookup lookup = new JndiDataSourceLookup();

	// Local host
	/*
	@Primary
    @Bean(name = "dataSource", destroyMethod = "")
	public DataSource dataSource() {
		return lookup.getDataSource(scaJndiName);
	}
	*/
	 
	// Heroku produção
	//*
	@Primary
    @Bean(name = "dataSource", destroyMethod = "")
	public DataSource dataSource() throws URISyntaxException {
		String host = "ec2-54-204-84-137.compute-1.amazonaws.com";
		String port = "5432";
		String path = "d82616uoonjtq4";
		String username = "ucjabtf4gvl2dv";
		String password = "p73cf82615c055a77ce4adeb3a07f69cd8b997c5ddeae1db864e3967c6ebdbffa";
		
		String dbUrl = "jdbc:postgresql://" + host + ':' + port +"/" + path + "?sslmode=require";

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("org.postgresql.Driver");
        basicDataSource.setUrl(dbUrl);
        basicDataSource.setUsername(username);
        basicDataSource.setPassword(password);

        return basicDataSource;
	}
	//*/
	
	// Heroku produção (old)
//	@Primary
//    @Bean(name = "dataSource", destroyMethod = "")
//	public DataSource dataSource() throws URISyntaxException {
//		String host = "ec2-100-25-231-126.compute-1.amazonaws.com";
//		String port = "5432";
//		String path = "d8spumnd4d12fj";
//		String username = "anfgqpuqselqiv";
//		String password = "ad48a38c418199f3fa88e93f419f05953860e914fa7a94e855bb66138943a4a8";
//		
//		String dbUrl = "jdbc:postgresql://" + host + ':' + port +"/" + path + "?sslmode=require";
//
//        BasicDataSource basicDataSource = new BasicDataSource();
//        basicDataSource.setDriverClassName("org.postgresql.Driver");
//        basicDataSource.setUrl(dbUrl);
//        basicDataSource.setUsername(username);
//        basicDataSource.setPassword(password);
//
//        return basicDataSource;
//	}
	
//	 Exemplo do Heroku (Não está funcionando)
//	@Primary
//	@Bean(name = "dataSource", destroyMethod = "")
//	public DataSource dataSource() throws URISyntaxException {
//		URI dbUri = new URI(System.getenv("DATABASE_URL"));
//
//        String username = dbUri.getUserInfo().split(":")[0];
//        String password = dbUri.getUserInfo().split(":")[1];
//        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + "/" + dbUri.getPath() + "?sslmode=require";
//
//        BasicDataSource basicDataSource = new BasicDataSource();
//        basicDataSource.setDriverClassName("org.postgresql.Driver");
//        basicDataSource.setUrl(dbUrl);
//        basicDataSource.setUsername(username);
//        basicDataSource.setPassword(password);
//
//        return basicDataSource;
//	}

	@Primary
	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("dataSource") DataSource dataSource) {
		return builder.dataSource(dataSource).packages("br.com.meldica.model.entity").persistenceUnit("meldica").build();
	}

	@Primary
	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager(
			@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
