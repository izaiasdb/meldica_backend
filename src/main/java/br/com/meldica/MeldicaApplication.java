package br.com.meldica;

import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MeldicaApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MeldicaApplication.class);
	}
	
    public static void main(String[] args) {
		 SpringApplication.run(MeldicaApplication.class, args);
    }

//    https://stackoverflow.com/questions/54316667/how-do-i-force-a-spring-boot-jvm-into-utc-time-zone/54316725    
    @PostConstruct
    public void init(){
//      TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
//    	TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
//    	TimeZone.setDefault( TimeZone.getTimeZone("GMT-3"));
		// Setting Spring Boot SetTimeZone
		TimeZone.setDefault(TimeZone.getTimeZone("GMT-0300"));

    }
    
    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected TomcatWebServer getTomcatWebServer(org.apache.catalina.startup.Tomcat tomcat) {
                tomcat.enableNaming(); 
                return super.getTomcatWebServer(tomcat);
            }

            @Override 
            protected void postProcessContext(Context context) {
                ContextResource resource1 = getResource("jdbc/meldica", "meldica");
                context.getNamingResources().addResource(resource1);
                
//                ContextResource resource2 = getResource("jdbc/sca", "p_sca");
//                context.getNamingResources().addResource(resource2); 
            }

			private ContextResource getResource(String name, String database) {
				ContextResource resource = new ContextResource();
                resource.setName(name);
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "org.postgresql.Driver");
                resource.setProperty("url", "jdbc:postgresql://localhost:5432/"+database+"?autoReconnect=true");
                resource.setProperty("username", "postgres");
                resource.setProperty("password", "210184");
                resource.setProperty("maxIdle", "4");
                resource.setProperty("maxTotal", "8");
                resource.setProperty("maxWaitMillis", "60000");
                resource.setProperty("removeAbandonedOnBorrow", "true");
                resource.setProperty("removeAbandonedOnMaintenance", "true");
                resource.setProperty("removeAbandonedTimeout", "90");
                resource.setProperty("testOnBorrow", "true");
                resource.setProperty("validationQuery", "SELECT 1");
				return resource;
			}
        };
    }

}
	