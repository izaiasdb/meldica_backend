package br.com.meldica.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.service.acesso.PerfilMenuService;
import br.com.meldica.service.acesso.UsuarioMenuService;
import br.com.meldica.service.acesso.UsuarioService;
import br.com.meldica.utils.Md5Utils;
import br.com.meldica.utils.error.handler.CustomException;

@Service
public class MyUserDetails implements UserDetailsService {
	@Value("${info.app.id}")
	private Integer sistema;
	
	@Value("${info.app.idmodulo}")
	private Integer idModulo;	
	
	@Autowired private UsuarioService usuario2Service;

	@Autowired private UsuarioMenuService usuarioMenuService;
	
	@Autowired private PerfilMenuService perfilMenuService;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		final UsuarioEntity user = usuario2Service.obterPorLogin(login);

		if (user == null) {
			throw new UsernameNotFoundException("Usuário '" + login + "' não encontrado");
		}

		FiltroUsuarioMenuDto filtroMenu = new FiltroUsuarioMenuDto(user.getId(), idModulo, user.getPerfil().getId());
		
		//List<String> auths = usuarioMenuService.listarPermissoesSecurity(usuarioMenu);
		List<String> auths = perfilMenuService.listarPermissoesSecurity(filtroMenu);
		
		if ((auths == null || auths.size() == 0) && !user.getDesenvolvedor()) {
			throw new CustomException("Usuário não possui permissões cadastradas!", HttpStatus.FORBIDDEN);
		}
		
		List<SimpleGrantedAuthority> authorities = auths.stream().map(a -> new SimpleGrantedAuthority(a)).collect(Collectors.toList());

		List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
		roles.add(new SimpleGrantedAuthority("ADMINISTRADOR"));
		authorities.addAll(roles);

		UserDetails usuario = User.withUsername(login)
							//.password(user.getSenha())
							.password(Md5Utils.getMd5(user.getSenha())) //OK
							.authorities(authorities)
							.accountExpired(false)
							.accountLocked(false)
							.credentialsExpired(false)
							.disabled(false)
							.build();
		
		return usuario; 
	}
	
}