package br.com.meldica.controller.acesso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.acesso.SistemaEntity;
import br.com.meldica.service.acesso.SistemaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/sistema")
public class SistemaController {
	@Autowired
	SistemaService sistemaService;
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('SISTEMA_CONSULTAR')")
	public List<SistemaEntity> listar(@RequestBody SistemaEntity sistema) {
		try {
			return sistemaService.listar(sistema);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('SISTEMA_INSERIR', 'SISTEMA_ALTERAR')")
	public SistemaEntity salvar(@RequestBody SistemaEntity sistema) {
		try {
			return sistemaService.salvar(sistema);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	

}
