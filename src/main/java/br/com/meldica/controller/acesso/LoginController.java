package br.com.meldica.controller.acesso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.acesso.CredenciaisDto;
import br.com.meldica.model.dto.acesso.UsuarioDto;
import br.com.meldica.service.acesso.UsuarioService;
import br.com.meldica.utils.error.handler.CustomException;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private UsuarioService usuarioService;	
	
	@PostMapping("/login")
	public UsuarioDto login(@RequestBody CredenciaisDto credenciais) {
		try {
			return usuarioService.login(credenciais.getUsername(), credenciais.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/esquecisenha")
	public void esqueciSenha(@RequestBody CredenciaisDto credenciais) {
		try {
			usuarioService.esqueciSenha(credenciais.getUsername());
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/alterarsenhaesquecida")
	public void alterarSenhaEsquecida(@RequestBody CredenciaisDto credenciais) {
		try {
			usuarioService.alterarsenha(credenciais);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	
}