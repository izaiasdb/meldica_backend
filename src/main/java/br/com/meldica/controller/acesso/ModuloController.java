package br.com.meldica.controller.acesso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.acesso.ModuloEntity;
import br.com.meldica.model.entity.acesso.SistemaEntity;
import br.com.meldica.service.acesso.ModuloService;
import br.com.meldica.service.acesso.SistemaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/modulo")
public class ModuloController {
	@Autowired
	ModuloService moduloService;
	@Autowired
	SistemaService sistemaService;	
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			List<SistemaEntity> sistemas = sistemaService.listar();
			
			params.put("sistemas", sistemas);
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('MODULO_CONSULTAR')")
	public List<ModuloEntity> listar(@RequestBody ModuloEntity modulo) {
		try {
			return moduloService.listar(modulo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('MODULO_INSERIR', 'MODULO_ALTERAR')")
	public ModuloEntity salvar(@RequestBody ModuloEntity modulo) {
		try {
			return moduloService.salvar(modulo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	

}
