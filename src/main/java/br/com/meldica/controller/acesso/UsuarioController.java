package br.com.meldica.controller.acesso;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.service.acesso.UsuarioService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			return usuarioService.init();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	
	
	@PostMapping("/pesquisar")
	//@PreAuthorize("hasAuthority('USUARIO_CONSULTAR')")
	public List<UsuarioEntity> listar(@RequestBody UsuarioEntity usuario) {
		try {
			return usuarioService.listar(usuario);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	//@PreAuthorize("hasAnyAuthority('USUARIO_INSERIR', 'USUARIO_ALTERAR')")
	public UsuarioEntity salvar(@RequestBody UsuarioEntity usuario) {
		try {
			return usuarioService.salvar(usuario);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}	
	
	@PostMapping("/alterarsenha")
	public UsuarioEntity alterarSenha(@RequestBody UsuarioEntity usuario) {
		try {
			return usuarioService.alterarsenha(usuario);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}		

}
