package br.com.meldica.controller.acesso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.enums.MenuPermissaoEnum;
import br.com.meldica.service.acesso.MenuService;
import br.com.meldica.service.acesso.ModuloService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/menu")
public class MenuController {
	@Autowired
	MenuService menuService;	
	@Autowired
	ModuloService moduloService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("modulos", moduloService.listar());
			params.put("menus", menuService.listar());
			params.put("permissoes", MenuPermissaoEnum.listarValueObject());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	//@PreAuthorize("hasAuthority('MENU_CONSULTAR')")
	public List<MenuEntity> listar(@RequestBody MenuEntity menu) {
		try {
			return menuService.listar(menu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	//@PreAuthorize("hasAnyAuthority('MENU_INSERIR', 'MENU_ALTERAR')")
	public MenuEntity salvar(@RequestBody MenuEntity menu) {
		try {
			return menuService.salvar(menu);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/pesquisarpornivel/{nivel}")
	//@PreAuthorize("hasAuthority('MENU_CONSULTAR')")
	public List<MenuEntity> listarPorNivel(@PathVariable("nivel") Integer nivel) {
		try {
			return menuService.listarPorNivel(nivel);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
