package br.com.meldica.controller.acesso;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.acesso.PerfilEntity;
import br.com.meldica.service.acesso.PerfilService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/perfil")
public class PerfilController {
	@Autowired
	PerfilService perfilService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			return perfilService.init();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	
	
	@PostMapping("/pesquisar")
//	@PreAuthorize("hasAuthority('PERFIL_CONSULTAR')")
	public List<PerfilEntity> listar(@RequestBody PerfilEntity perfil) {
		try {
			return perfilService.listar(perfil);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
//	@PreAuthorize("hasAnyAuthority('PERFIL_INSERIR', 'PERFIL_ALTERAR')")
	public PerfilEntity salvar(@RequestBody PerfilEntity perfil) {
		try {
			return perfilService.salvar(perfil);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}	

}
