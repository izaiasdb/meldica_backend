package br.com.meldica.controller.relatorios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.relatorios.RelatorioEstoqueProdutoAcabadoDto;
import br.com.meldica.model.dto.relatorios.RelatorioListagemVendaDto;
import br.com.meldica.model.dto.relatorios.RelatorioResumoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.enums.ExtensaoEnum;
import br.com.meldica.service.publico.OrdemServicoService;
import br.com.meldica.service.relatorio.RelatorioOrdemServicoService;
import br.com.meldica.utils.ReportGenerator;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/relatorio/ordemServico")
public class RelatorioOrdemServicoController {
	
	@Autowired private OrdemServicoService service;
	@Autowired private RelatorioOrdemServicoService relatorioOrdemServicoService;
	private @Autowired ReportGenerator reportGenerator;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
//			params.put("tipoTerminalList", TipoLogTerminalEventoEnum.listarValueObject());
		}catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return params;
	}
	
	@PostMapping("/relatorioResumoMensal")
//	@PreAuthorize("hasAnyAuthority('RELATORIO_TERMINAL_LOG_CONSULTAR')")
	public List<RelatorioResumoDto> pesquisar(@RequestBody OrdemServicoEntity filtro) {
		try {
			System.out.println("relatorioResumoMensal");
			return relatorioOrdemServicoService.relatorioResumoMensal(filtro);
		}catch(Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/imprimiResumoMensal")
	//@PreAuthorize("hasAnyAuthority('FICHA_CUSTODIADO_IMPRIMIR')")
//	@PreAuthorize("hasAnyAuthority('CUSTODIADO_IMPRIMIR')")
	public ResponseEntity<byte[]> imprimiResumoMensal(HttpServletRequest request, @RequestBody OrdemServicoEntity filtro) {	
		try {
			System.out.println("imprimiResumoMensal");
			ServletContext context = request.getServletContext();
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/resumoMensal.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<RelatorioResumoDto> list = relatorioOrdemServicoService.relatorioResumoMensal(filtro);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParamsResumoMensal(context, list.get(0)), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	private Map<String, Object> getParamsResumoMensal(ServletContext context, RelatorioResumoDto pedido) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
		params.put("RELATIVE_PATH_SUB_DADOS", context.getRealPath("/WEB-INF/resources/relatorios/subResumoMensalDados.jasper"));
		params.put("RELATIVE_PATH_SUB_PRODUTOS", context.getRealPath("/WEB-INF/resources/relatorios/subResumoMensalProduto.jasper"));
		params.put("RELATIVE_PATH_SUB_FORMAS", context.getRealPath("/WEB-INF/resources/relatorios/subResumoMensalForma.jasper"));
		
		params.put("PRODUTOS", pedido.getProdutoList());
		params.put("FORMAS", pedido.getFormaList());
		params.put("RESUMO", pedido);
		return params;
	}
	
	@PostMapping("/imprimirListagemVenda")
	public ResponseEntity<byte[]> imprimirListagemVenda(HttpServletRequest request, @RequestBody OrdemServicoEntity filtro) {	
		try {
			ServletContext context = request.getServletContext();
			//String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/listagem/resumoListagemVenda.jasper");
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/listagem/resumoListagemVendaTotais.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<OrdemServicoEntity> list = relatorioOrdemServicoService.relatorioListagemVenda(filtro);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParamsListagemVenda(context, list.get(0)), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	private Map<String, Object> getParamsListagemVenda(ServletContext context, OrdemServicoEntity pedido) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
		
//		params.put("PRODUTOS", pedido.getProdutoList());
//		params.put("FORMAS", pedido.getFormaList());
		params.put("RESUMO", pedido);
		return params;
	}
	
	@PostMapping("/imprimirProduzirProduto")
	public ResponseEntity<byte[]> imprimirProduzirProduto(HttpServletRequest request, @RequestBody OrdemServicoEntity filtro) {	
		try {
			ServletContext context = request.getServletContext();
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/aProduzir.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<RelatorioResumoDto> list = relatorioOrdemServicoService.relatorioProduzir(filtro);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParamsProduzirProduto(context, list.get(0)), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	private Map<String, Object> getParamsProduzirProduto(ServletContext context, RelatorioResumoDto pedido) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
		params.put("RELATIVE_PATH_SUB_DADOS", context.getRealPath("/WEB-INF/resources/relatorios/aProduzirMensalDados.jasper"));
		params.put("RELATIVE_PATH_SUB_PRODUTOS", context.getRealPath("/WEB-INF/resources/relatorios/aProduzirMensalProduto.jasper"));
		
		params.put("PRODUTOS", pedido.getProdutoList());
		params.put("RESUMO", pedido);
		return params;
	}
	
	@GetMapping("/imprimirEstoqueProdutoAcabado/{idEmpresa}")
	public ResponseEntity<byte[]> imprimirEstoqueProdutoAcabado(HttpServletRequest request, @PathVariable Integer idEmpresa) {	
		try {
			ServletContext context = request.getServletContext();
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/estoqueProdAcabado/estqProdAcabado.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<RelatorioEstoqueProdutoAcabadoDto> list = relatorioOrdemServicoService.relatorioEstoqueProdutoAcabado(idEmpresa);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParamsimprimirEstoqueProdutoAcabado(context, list.get(0)), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	private Map<String, Object> getParamsimprimirEstoqueProdutoAcabado(ServletContext context, RelatorioEstoqueProdutoAcabadoDto rel) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
		params.put("RESUMO", rel);
		return params;
	}
}
