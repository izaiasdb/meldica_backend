package br.com.meldica.controller.relatorios;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/relatorios/geral")
public class RelatorioGeralController {
	
	@GetMapping("/init")
	public Map<String, Object> listarTodos() {
		Map<String, Object> params = new HashMap<String, Object>();
		try {
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
		return params;
	}

}