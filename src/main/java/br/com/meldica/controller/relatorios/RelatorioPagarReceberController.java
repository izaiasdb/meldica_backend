package br.com.meldica.controller.relatorios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.relatorios.PagarReceberDto;
import br.com.meldica.service.publico.PagarReceberService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/relatorio/pagarReceber")
public class RelatorioPagarReceberController {
	
	private @Autowired PagarReceberService pagarReceberService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
//			params.put("tipoTerminalList", TipoLogTerminalEventoEnum.listarValueObject());
		}catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return params;
	}
	
	@PostMapping("/pesquisar")
//	@PreAuthorize("hasAnyAuthority('RELATORIO_TERMINAL_LOG_CONSULTAR')")
	public List<PagarReceberDto> pesquisar(@RequestBody PagarReceberDto filtro) {
		List<PagarReceberDto> retorno = new ArrayList<PagarReceberDto>();
		
		try {
			retorno = pagarReceberService.listarRelatorio(filtro);
		}catch(Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return retorno;
	}
}
