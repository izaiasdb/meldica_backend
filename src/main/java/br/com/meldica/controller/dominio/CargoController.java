package br.com.meldica.controller.dominio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.dominio.CargoEntity;
import br.com.meldica.service.dominio.CargoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/cargo")
public class CargoController {
	private @Autowired CargoService service;
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('CARGOS_CONSULTAR')") 
	public List<CargoEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('CARGOS_CONSULTAR')") 
	public List<CargoEntity> pesquisar(@RequestBody CargoEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('CARGOS_INSERIR', 'CARGOS_ALTERAR')")
	public CargoEntity salvar(@RequestBody CargoEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
}
