package br.com.meldica.controller.dominio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.dominio.UnidadeMedidaEntity;
import br.com.meldica.service.dominio.UnidadeMedidaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/unidadeMedida")
public class UnidadeMedidaController {
	private @Autowired UnidadeMedidaService service;
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('UNIDADES_DE_MEDIDA_CONSULTAR')") 
	public List<UnidadeMedidaEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('UNIDADES_DE_MEDIDA_CONSULTAR')") 
	public List<UnidadeMedidaEntity> pesquisar(@RequestBody UnidadeMedidaEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('UNIDADES_DE_MEDIDA_INSERIR', 'UNIDADES_DE_MEDIDA_ALTERAR')")
	public UnidadeMedidaEntity salvar(@RequestBody UnidadeMedidaEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
}
