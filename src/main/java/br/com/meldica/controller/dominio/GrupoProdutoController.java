package br.com.meldica.controller.dominio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;
import br.com.meldica.service.dominio.GrupoProdutoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/grupoProduto")
public class GrupoProdutoController {
	private @Autowired GrupoProdutoService service;
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('GRUPO_PRODUTO_CONSULTAR')") 
	public List<GrupoProdutoEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('GRUPO_PRODUTO_CONSULTAR')") 
	public List<GrupoProdutoEntity> pesquisar(@RequestBody GrupoProdutoEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('GRUPO_PRODUTO_INSERIR', 'GRUPO_PRODUTO_ALTERAR')")
	public GrupoProdutoEntity salvar(@RequestBody GrupoProdutoEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
}
