package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.ProdutoEntity;
import br.com.meldica.service.dominio.GrupoProdutoService;
import br.com.meldica.service.dominio.UnidadeMedidaService;
import br.com.meldica.service.publico.EmpresaService;
import br.com.meldica.service.publico.ProdutoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/produto")
public class ProdutoController {
	
	@Autowired ProdutoService service;
	@Autowired UnidadeMedidaService unidadeMedidaService;
	@Autowired GrupoProdutoService grupoProdutoService;
	@Autowired EmpresaService empresaService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("produtoList", service.listar());
			params.put("unidadeMedidaList", unidadeMedidaService.listar());
			params.put("empresaList", empresaService.listar());
			params.put("grupoProdutoList", grupoProdutoService.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('PRODUTOS_CONSULTAR')")
	public List<ProdutoEntity> listar(@RequestBody ProdutoEntity produto) {
		try {
			return service.listar(produto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('PRODUTOS_INSERIR', 'PRODUTOS_ALTERAR')")
	public ProdutoEntity salvar(@RequestBody ProdutoEntity produto) {
		try {
			return service.salvar(produto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
