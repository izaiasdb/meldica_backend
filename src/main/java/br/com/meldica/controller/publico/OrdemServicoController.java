package br.com.meldica.controller.publico;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.relatorios.RelatorioResumoProdutoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;
import br.com.meldica.model.enums.ExtensaoEnum;
import br.com.meldica.service.dominio.MunicipioService;
import br.com.meldica.service.dominio.UfService;
import br.com.meldica.service.publico.ClienteEnderecoService;
import br.com.meldica.service.publico.ClienteRazaoService;
import br.com.meldica.service.publico.ClienteService;
import br.com.meldica.service.publico.ClienteTabelaPrecoService;
import br.com.meldica.service.publico.ConfiguracaoService;
import br.com.meldica.service.publico.EmpresaService;
import br.com.meldica.service.publico.FormaCondicaoPagamentoService;
import br.com.meldica.service.publico.FuncionarioService;
import br.com.meldica.service.publico.OrdemServicoService;
import br.com.meldica.service.publico.PlanoContaService;
import br.com.meldica.service.publico.ProdutoService;
import br.com.meldica.service.publico.TabelaPrecoProdutoService;
import br.com.meldica.service.publico.TabelaPrecoService;
import br.com.meldica.service.publico.TransportadoraEnderecoService;
import br.com.meldica.service.publico.TransportadoraService;
import br.com.meldica.utils.ReportGenerator;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/ordemServico")
public class OrdemServicoController {
	
	@Autowired OrdemServicoService service;
	@Autowired ClienteService clienteService;
	@Autowired ClienteRazaoService clienteRazaoService;
	@Autowired ClienteEnderecoService clienteEnderecoService;
	@Autowired FuncionarioService funcionarioService;
	@Autowired ProdutoService produtoService;
	@Autowired TransportadoraService transportadoraService;
	@Autowired UfService ufService;
	@Autowired MunicipioService municipioService;
	@Autowired FormaCondicaoPagamentoService formaCondicaoPagamentoService;
	@Autowired TabelaPrecoService tabelaPrecoService;
	@Autowired TabelaPrecoProdutoService tabelaPrecoProdutoService;
	@Autowired PlanoContaService planoContaService;
	@Autowired TransportadoraEnderecoService transportadoraEnderecoService;
	@Autowired ConfiguracaoService configuracaoService;
	@Autowired ClienteTabelaPrecoService clienteTabelaPrecoService;
	@Autowired EmpresaService empresaService;
	private @Autowired ReportGenerator reportGenerator;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("clienteList", clienteService.listar());
			params.put("clienteRazaoList", clienteRazaoService.listar());
			params.put("funcionarioList", funcionarioService.listar());
			params.put("produtoList", produtoService.listar());
			params.put("formaCondicaoList", formaCondicaoPagamentoService.listar());
			params.put("transportadoraList", transportadoraService.listar());
			params.put("ufList", ufService.listarTodos());
			params.put("municipioList", municipioService.listarTodos());
			params.put("tabelaPrecoList", tabelaPrecoService.listarAtivos());
			params.put("tabelaPrecoProdutoList", tabelaPrecoProdutoService.listar());
			params.put("planoContaList", planoContaService.listar());
			params.put("clienteEnderecoList", clienteEnderecoService.listar());
			params.put("transportadoraEnderecoList", transportadoraEnderecoService.listar());
			params.put("configuracaoList", configuracaoService.listar());
			params.put("clienteTabelaPrecoList", clienteTabelaPrecoService.listar());
			params.put("empresaList", empresaService.listar());
			
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('VENDAS_CONSULTAR')")
	public OrdemServicoEntity obter(@PathVariable Integer id) {
		try {
			return service.obter(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}
		
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('VENDAS_CONSULTAR')")
	public List<OrdemServicoEntity> listar(@RequestBody OrdemServicoEntity ordemServico) {
		try {
			return service.listar(ordemServico);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('VENDAS_INSERIR', 'VENDAS_ALTERAR')")
	public void salvar(@RequestBody OrdemServicoEntity ordemServico) {
		try {
			service.salvar(ordemServico);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/alterarStatus")
	@PreAuthorize("hasAnyAuthority('VENDAS_ALTERAR')")
	public void finalizar(@RequestBody OrdemServicoEntity ordemServico) {
		try {
			service.alterarStatus(ordemServico);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/imprimir")
	//@PreAuthorize("hasAnyAuthority('FICHA_CUSTODIADO_IMPRIMIR')")
//	@PreAuthorize("hasAnyAuthority('CUSTODIADO_IMPRIMIR')")
	public ResponseEntity<byte[]> imprimir(HttpServletRequest request, @RequestBody OrdemServicoEntity filtro) {	
		try {
			ServletContext context = request.getServletContext();
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/ordemPedido.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<OrdemServicoEntity> list = new ArrayList<OrdemServicoEntity>();
			OrdemServicoEntity pedido = service.obter(filtro.getId());
			
			List<OrdemServicoProdutoEntity> produtoList = pedido.getProdutosNaturaisEncapsulados(pedido.getProdutoItemsList());
			List<OrdemServicoProdutoEntity> produtoCosmeticoList = pedido.getProdutosCosmeticos(pedido.getProdutoItemsList());
			List<OrdemServicoProdutoEntity> bonificacaoList = pedido.getProdutosBonificacao(pedido.getProdutoItemsList());
			List<OrdemServicoFormaPagamentoEntity> formaList = pedido.getFormaItemsList();

			pedido.setProdutoItemsList(produtoList);
			pedido.setProdutoCosmeticoList(produtoCosmeticoList);
			pedido.setBonificacaoItemsList(bonificacaoList);
			pedido.setFormaItemsList(formaList);
//			pedido.setarTotais();
			
			if (pedido.getIdClienteRazao() != null) {
				pedido.setClienteRazao(clienteRazaoService.obter(pedido.getIdClienteRazao()));
			}
			
			if (pedido.getTransportadoraItemsList() != null) {
				pedido.getTransportadoraItemsList().forEach(c->{
					if (c.getIdTransportadoraDestino() != null) {
						c.setTransportadoraDestino(transportadoraService.obter(c.getIdTransportadoraDestino()));
					}
				});
			}
				
			list.add(pedido);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParams(context, pedido), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	private Map<String, Object> getParams(ServletContext context, OrdemServicoEntity pedido) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
		params.put("RELATIVE_PATH_SUB_DADOS_PEDIDOS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoDados.jasper"));
		params.put("RELATIVE_PATH_SUB_PRODUTOS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoProdutos.jasper"));		
		params.put("RELATIVE_PATH_SUB_COSMETICOS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoProdutos.jasper"));
		params.put("RELATIVE_PATH_SUB_BONIFICACAO", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoBonificacao.jasper"));
		params.put("RELATIVE_PATH_SUB_KITS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoKits.jasper"));
		params.put("RELATIVE_PATH_SUB_FORMAS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoFormas.jasper"));
		params.put("RELATIVE_PATH_SUB_TRANSPORTADORAS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoTransportadoras.jasper"));
		
		params.put("PRODUTOS", pedido.getProdutoItemsList());
		params.put("COSMETICOS", pedido.getProdutoCosmeticoList());
		params.put("BONIFICACAO", pedido.getBonificacaoItemsList());
		params.put("FORMAS", pedido.getFormaItemsList());
		params.put("KITS", pedido.getKitList());
		params.put("KITS_PRODUTOS", pedido.getKitProdutoList());
		params.put("TRANSPORTADORAS", pedido.getTransportadoraItemsList());
//		params.put("ID_USUARIO", filtro.getIdUsuario());
		params.put("PEDIDO", pedido);
		
		return params;
	}
	
	@PostMapping("/imprimirSeparado")
	public ResponseEntity<byte[]> imprimirSeparado(HttpServletRequest request, @RequestBody OrdemServicoEntity filtro) {	
		try {
			ServletContext context = request.getServletContext();
			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/ordemPedido.jasper");
			ExtensaoEnum extensao = ExtensaoEnum.PDF;
			
			List<OrdemServicoEntity> list = new ArrayList<OrdemServicoEntity>();
			OrdemServicoEntity pedido = service.obter(filtro.getId());
			
			/*
			List<OrdemServicoProdutoEntity> produtoList = pedido.getProdutosNaturaisEncapsulados();
			List<OrdemServicoProdutoEntity> produtoCosmeticoList = pedido.getProdutosCosmeticos();
			List<OrdemServicoProdutoEntity> bonificacaoList = pedido.getProdutosBonificacao();
			List<OrdemServicoFormaPagamentoEntity> formaList = new ArrayList<>();
			
			if (filtro.getTipoRelatorio().equals("N")) {
				produtoList = pedido.getProdutosNaturaisEncapsulados();
				produtoCosmeticoList = pedido.getProdutosCosmeticos();
				bonificacaoList = pedido.getProdutosBonificacao();
				formaList = pedido.getFormaItemsList();
			} else if (filtro.getTipoRelatorio().equals("N")) {
				//RESUMO MENSAL - MÉLDICA NATURAIS E ENCAPSULADOS
				produtoList = pedido.getProdutoItemsList().stream()
						// 07/07/2021 -> Mostrar as bonificações também
						//.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(1) || c.getIdEmpresaProduto().equals(3))
						.filter(c -> c.getIdEmpresaProduto().equals(1) || c.getIdEmpresaProduto().equals(3))
						.collect(Collectors.toList());
				
				formaList = pedido.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(1) || c.getIdEmpresa().equals(3))
						.collect(Collectors.toList());
			} else if (filtro.getTipoRelatorio().equals("C")) { // Cométicos
				//"RESUMO MENSAL - MÉLDICA COSMÉTICOS"
				produtoList = pedido.getProdutoItemsList().stream()
						// 07/07/2021 -> Mostrar as bonificações também
						//.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(2))
						.filter(c -> c.getIdEmpresaProduto().equals(2))
						.collect(Collectors.toList());
				
				formaList = pedido.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(2))
						.collect(Collectors.toList());
			}
			
//			produtoList = produtoList.stream()
//		            .sorted(Comparator.<OrdemServicoProdutoEntity, Integer> comparing(c-> c.getProduto().getId()))
//		            		.collect(Collectors.toList());
			
			produtoList = produtoList.stream()
		            .sorted(Comparator.<OrdemServicoProdutoEntity, String> comparing(c-> c.getProduto().getNome()))
		            		.collect(Collectors.toList());

			pedido.setProdutoItemsList(produtoList);
			pedido.setBonificacaoItemsList(bonificacaoList);
			pedido.setFormaItemsList(formaList);
//			pedido.setarTotais();
			
			if (pedido.getIdClienteRazao() != null) {
				pedido.setClienteRazao(clienteRazaoService.obter(pedido.getIdClienteRazao()));
			}
			
			if (pedido.getTransportadoraItemsList() != null) {
				pedido.getTransportadoraItemsList().forEach(c->{
					if (c.getIdTransportadoraDestino() != null) {
						c.setTransportadoraDestino(transportadoraService.obter(c.getIdTransportadoraDestino()));
					}
				});
			}
			*/
				
			list.add(pedido);
			
			byte[] content = reportGenerator.getReport(jasperPath, getParams(context, pedido), extensao, list);
			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("gerarfinanceiro/{id}")
	@PreAuthorize("hasAnyAuthority('VENDAS_ALTERAR')")
	public void gerarfinanceiro(@PathVariable Integer id) {
		try {
			service.gerarFinanceiro(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}
	
	@GetMapping("deletarfinanceiro/{id}")
	@PreAuthorize("hasAnyAuthority('VENDAS_ALTERAR')")
	public void deletarfinanceiro(@PathVariable Integer id) {
		try {
			service.deletarFinanceiro(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}
	
	@GetMapping("/ultimaCompraCliente/{idCliente}")
	@PreAuthorize("hasAnyAuthority('VENDAS_CONSULTAR')")
	public OrdemServicoEntity obterUltimaCompraCliente(@PathVariable Integer idCliente) {
		try {
			return service.obterUltimaCompraCliente(idCliente);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}	
}
