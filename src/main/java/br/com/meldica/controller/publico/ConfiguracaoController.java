package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.ConfiguracaoEntity;
import br.com.meldica.service.publico.ConfiguracaoService;
import br.com.meldica.service.publico.PlanoContaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/configuracao")
public class ConfiguracaoController {
	private @Autowired ConfiguracaoService service;
	private @Autowired PlanoContaService planoContaService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("planoContaList", planoContaService.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	//@PreAuthorize("hasAuthority('MENU_CONSULTAR')")
	public List<ConfiguracaoEntity> listar(@RequestBody ConfiguracaoEntity configuracao) {
		try {
			return service.listar(configuracao);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	//@PreAuthorize("hasAnyAuthority('MENU_INSERIR', 'MENU_ALTERAR')")
	public ConfiguracaoEntity salvar(@RequestBody ConfiguracaoEntity configuracao) {
		try {
			return service.salvar(configuracao);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
