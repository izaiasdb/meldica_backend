package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.publico.FichaDto;
import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.service.dominio.MunicipioService;
import br.com.meldica.service.dominio.UfService;
import br.com.meldica.service.publico.ClienteService;
import br.com.meldica.service.publico.TabelaPrecoProdutoService;
import br.com.meldica.service.publico.TabelaPrecoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired ClienteService service;
	@Autowired UfService ufService;
	@Autowired MunicipioService municipioService;
	@Autowired TabelaPrecoService tabelaPrecoService;
	@Autowired TabelaPrecoProdutoService tabelaPrecoProdutoService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("ufList", ufService.listarTodos());
			params.put("municipioList", municipioService.listarTodos());
			params.put("tabelaPrecoList", tabelaPrecoService.listar());
			params.put("tabelaPrecoProdutoList", tabelaPrecoProdutoService.listar());
			
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('CLIENTES_CONSULTAR')")
	public ClienteEntity obter(@PathVariable Integer id) {
		try {
			return service.obter(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}	
	
	@GetMapping("/obterFicha/{id}")
	@PreAuthorize("hasAnyAuthority('CLIENTES_CONSULTAR')")
	public ClienteEntity obterFicha(@PathVariable Integer id) {
		try {
			return service.obterFicha(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('CLIENTES_CONSULTAR')")
	public List<ClienteEntity> listar(@RequestBody ClienteEntity cliente) {
		try {
			return service.listar(cliente);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('CLIENTES_INSERIR', 'CLIENTES_ALTERAR')")
	public ClienteEntity salvar(@RequestBody ClienteEntity cliente) {
		try {
			return service.salvar(cliente);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
//	@PostMapping("/obterFicha")
//	public ClienteEntity obterFicha(HttpServletRequest request, @RequestBody FichaDto filtro){
//		try {
//			return service.obterFicha(filtro);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
//		}		
//	}
	
	@PostMapping("/cancelar")
	@PreAuthorize("hasAuthority('CLIENTES_EXCLUIR')")
	public void cancelar(@RequestBody ClienteEntity entity) {
		try {
			service.cancelar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
