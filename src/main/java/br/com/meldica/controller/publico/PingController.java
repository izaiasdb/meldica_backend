package br.com.meldica.controller.publico;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.utils.error.handler.CustomException;

@RestController
@RequestMapping("/ping")
public class PingController {
	
	@GetMapping("/ping")
	public String pinginit() {
		try {
			return "pong teste 2";
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
}
