package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.dto.publico.FichaDto;
import br.com.meldica.model.enums.StatusNotaEnum;
import br.com.meldica.service.publico.MainService;
import br.com.meldica.service.publico.OrdemServicoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/main")
public class MainController {
	
	@Autowired private MainService service;
	@Autowired private OrdemServicoService ordemServicoService;
	
	@GetMapping("/init")
	public Map<String, Object> init(@RequestParam("idUsuario") Integer idUsuario) {	
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			params.put("pedidoLogisticaList", ordemServicoService.listarPorStatus(StatusNotaEnum.LOGISTICA.getId()));
			params.put("pedidoReabertoList", ordemServicoService.listarPorStatus(StatusNotaEnum.REABERTO.getId()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return params;
	}
	
	@PostMapping("/buscaRapida")
	public List<FichaDto> buscaRapida(@RequestBody FichaDto filtro) {
		try {
			return service.listarBuscaRapida(filtro);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
//	@PostMapping("/inativarAlerta")
//	public void inativarAlerta(@RequestBody AlertaUsuarioDto alertaUsuario) {
//		try {
//			if (alertaUsuario.getIdTipoAlerta().equals(TipoAlertaEnum.DOCUMENTO_PARECER_JURIDICO.getId())) {
//				atendimentoJuridicoDocumentoService.inativarAlerta(alertaUsuario.getId().longValue());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
//		}
//	}
	

}
