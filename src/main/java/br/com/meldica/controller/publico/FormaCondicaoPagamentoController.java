package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.FormaCondicaoPagamentoEntity;
import br.com.meldica.model.entity.publico.FormaPagamentoEntity;
import br.com.meldica.service.publico.CondicaoPagamentoService;
import br.com.meldica.service.publico.FormaCondicaoPagamentoService;
import br.com.meldica.service.publico.FormaPagamentoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/formaCondicaoPagamento")
public class FormaCondicaoPagamentoController {
	private @Autowired FormaCondicaoPagamentoService service;
	private @Autowired CondicaoPagamentoService condicaoPagamentoService;
	private @Autowired FormaPagamentoService formaPagamentoService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("condicaoPagamentoList", condicaoPagamentoService.listar());
			params.put("formaPagamentoList", formaPagamentoService.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('FORMA_CONDICOES_DE_PAGAMENTO_CONSULTAR')") 
	public List<FormaCondicaoPagamentoEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('FORMA_CONDICOES_DE_PAGAMENTO_CONSULTAR')") 
	public List<FormaCondicaoPagamentoEntity> pesquisar(@RequestBody FormaCondicaoPagamentoEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('FORMA_CONDICOES_DE_PAGAMENTO_INSERIR', 'FORMA_CONDICOES_DE_PAGAMENTO_ALTERAR')")
	public FormaCondicaoPagamentoEntity salvar(@RequestBody FormaCondicaoPagamentoEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
	
	@PostMapping("/cancelar")
	@PreAuthorize("hasAuthority('FORMA_CONDICOES_DE_PAGAMENTO_EXCLUIR')")
	public void cancelar(@RequestBody FormaCondicaoPagamentoEntity entity) {
		try {
			service.cancelar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
