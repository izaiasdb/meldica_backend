package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.service.dominio.CargoService;
import br.com.meldica.service.dominio.MunicipioService;
import br.com.meldica.service.dominio.UfService;
import br.com.meldica.service.publico.FuncionarioService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {
	@Autowired FuncionarioService service;
	@Autowired CargoService cargoService;
	@Autowired UfService ufService;
	@Autowired MunicipioService municipioService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("ufList", ufService.listarTodos());
			params.put("municipioList", municipioService.listarTodos());			
			params.put("cargoList", cargoService.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('FUNCIONARIOS_CONSULTAR')")
	public List<FuncionarioEntity> listar(@RequestBody FuncionarioEntity funcionario) {
		try {
			return service.listar(funcionario);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('FUNCIONARIOS_INSERIR', 'FUNCIONARIOS_ALTERAR')")
	public FuncionarioEntity salvar(@RequestBody FuncionarioEntity funcionario) {
		try {
			return service.salvar(funcionario);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
