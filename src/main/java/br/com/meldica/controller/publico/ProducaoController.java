package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.ProducaoEntity;
import br.com.meldica.service.publico.EmpresaService;
import br.com.meldica.service.publico.ProducaoService;
import br.com.meldica.service.publico.ProdutoService;
import br.com.meldica.utils.ReportGenerator;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/producao")
public class ProducaoController {
	
	@Autowired ProducaoService service;
	@Autowired ProdutoService produtoService;
	@Autowired EmpresaService empresaService;
	private @Autowired ReportGenerator reportGenerator;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("produtoList", produtoService.listar());
			params.put("empresaList", empresaService.listar());
			
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('PRODUCAO_CONSULTAR')")
	public ProducaoEntity obter(@PathVariable Integer id) {
		try {
			return service.obter(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}
		
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('PRODUCAO_CONSULTAR')")
	public List<ProducaoEntity> listar(@RequestBody ProducaoEntity ordemServico) {
		try {
			return service.listar(ordemServico);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('PRODUCAO_INSERIR', 'PRODUCAO_ALTERAR')")
	public void salvar(@RequestBody ProducaoEntity producao) {
		try {
			service.salvar(producao);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
//	@PostMapping("/imprimir")
//	public ResponseEntity<byte[]> imprimir(HttpServletRequest request, @RequestBody ProducaoEntity filtro) {	
//		try {
//			ServletContext context = request.getServletContext();
//			String jasperPath = context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/ordemPedido.jasper");
//			ExtensaoEnum extensao = ExtensaoEnum.PDF;
//			
//			List<ProducaoEntity> list = new ArrayList<ProducaoEntity>();
//			ProducaoEntity pedido = service.obter(filtro.getId());
//			
//			List<ProducaoProdutoEntity> produtoList = new ArrayList<>();
//			
//			if (filtro.getTipoRelatorio().equals("T")) {
//				//RESUMO MENSAL - MÉLDICA NATURAIS, COSMÉTICOS E ENCAPSULADOS
//				produtoList = pedido.getProdutoItemsList().stream()
//						.filter(c -> !c.isBonificacao())
//						.collect(Collectors.toList());
//			} else if (filtro.getTipoRelatorio().equals("N")) {
//				//RESUMO MENSAL - MÉLDICA NATURAIS E ENCAPSULADOS
//				produtoList = pedido.getProdutoItemsList().stream()
//						// 07/07/2021 -> Mostrar as bonificações também
//						//.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(1) || c.getIdEmpresaProduto().equals(3))
//						.filter(c -> c.getIdEmpresaProduto().equals(1) || c.getIdEmpresaProduto().equals(3))
//						.collect(Collectors.toList());
//			} else if (filtro.getTipoRelatorio().equals("C")) { // Cométicos
//				//"RESUMO MENSAL - MÉLDICA COSMÉTICOS"
//				produtoList = pedido.getProdutoItemsList().stream()
//						// 07/07/2021 -> Mostrar as bonificações também
//						//.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(2))
//						.filter(c -> c.getIdEmpresaProduto().equals(2))
//						.collect(Collectors.toList());
//			}
//			
//			produtoList = produtoList.stream()
//		            .sorted(Comparator.<OrdemServicoProdutoEntity, String> comparing(c-> c.getProduto().getNome()))
//		            		.collect(Collectors.toList());
//
//			pedido.setProdutoItemsList(produtoList);
//			
//				
//			list.add(pedido);
//			
//			byte[] content = reportGenerator.getReport(jasperPath, getParams(context, pedido), extensao, list);
//			return new ResponseEntity<byte[]>(content, reportGenerator.getHttpHeaders(extensao), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
//		}
//	}
	
//	private Map<String, Object> getParams(ServletContext context, ProducaoEntity pedido) {
//		Map<String,Object> params = new HashMap<String, Object>();
//		params.put("LOGO_PATH", context.getRealPath("/WEB-INF/resources/images/logo.png"));
//		params.put("RELATIVE_PATH_SUB_DADOS_PEDIDOS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoDados.jasper"));
//		params.put("RELATIVE_PATH_SUB_PRODUTOS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoProdutos.jasper"));
//		params.put("RELATIVE_PATH_SUB_BONIFICACAO", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoBonificacao.jasper"));
//		params.put("RELATIVE_PATH_SUB_KITS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoKits.jasper"));
//		params.put("RELATIVE_PATH_SUB_FORMAS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoFormas.jasper"));
//		params.put("RELATIVE_PATH_SUB_TRANSPORTADORAS", context.getRealPath("/WEB-INF/resources/relatorios/os/pedido/subPedidoTransportadoras.jasper"));
//		
//		params.put("PRODUTOS", pedido.getProdutoItemsList());
//		params.put("BONIFICACAO", pedido.getBonificacaoItemsList());
//		params.put("FORMAS", pedido.getFormaItemsList());
//		params.put("KITS", pedido.getKitList());
//		params.put("KITS_PRODUTOS", pedido.getKitProdutoList());
//		params.put("TRANSPORTADORAS", pedido.getTransportadoraItemsList());
////		params.put("ID_USUARIO", filtro.getIdUsuario());
//		params.put("PEDIDO", pedido);
//		
//		return params;
//	}

}
