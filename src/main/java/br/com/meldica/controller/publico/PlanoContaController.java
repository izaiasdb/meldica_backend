package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.PlanoContaEntity;
import br.com.meldica.service.publico.PlanoContaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/planoConta")
public class PlanoContaController {
	private @Autowired PlanoContaService service;	
//	@Autowired
//	ModuloService moduloService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("planoContaList", service.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('PLANO_DE_CONTAS_CONSULTAR')")
	public List<PlanoContaEntity> listar(@RequestBody PlanoContaEntity planoConta) {
		try {
			return service.listar(planoConta);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('PLANO_DE_CONTAS_INSERIR', 'PLANO_DE_CONTAS_ALTERAR')")
	public PlanoContaEntity salvar(@RequestBody PlanoContaEntity planoConta) {
		try {
			return service.salvar(planoConta);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/pesquisarpornivel/{nivel}")
//	@PreAuthorize("hasAuthority('PLANO_DE_CONTAS_CONSULTAR')")
	public List<PlanoContaEntity> listarPorNivel(@PathVariable("nivel") Integer nivel) {
		try {
			return service.listarPorNivel(nivel);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
