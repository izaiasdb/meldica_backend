package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.TransportadoraEntity;
import br.com.meldica.service.dominio.MunicipioService;
import br.com.meldica.service.dominio.UfService;
import br.com.meldica.service.publico.ClienteService;
import br.com.meldica.service.publico.TransportadoraService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/transportadora")
public class TransportadoraController {
	
	@Autowired TransportadoraService service;
	@Autowired UfService ufService;
	@Autowired MunicipioService municipioService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("ufList", ufService.listarTodos());
			params.put("municipioList", municipioService.listarTodos());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('TRANSPORTADORA_CONSULTAR')")
	public List<TransportadoraEntity> listar(@RequestBody TransportadoraEntity entity) {
		try {
			return service.listar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('TRANSPORTADORA_INSERIR', 'TRANSPORTADORA_ALTERAR')")
	public TransportadoraEntity salvar(@RequestBody TransportadoraEntity entity) {
		try {
			return service.salvar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
