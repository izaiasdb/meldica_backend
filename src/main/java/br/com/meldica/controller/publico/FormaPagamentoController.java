package br.com.meldica.controller.publico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.FormaPagamentoEntity;
import br.com.meldica.service.publico.FormaPagamentoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/formaPagamento")
public class FormaPagamentoController {
	private @Autowired FormaPagamentoService service;
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('FORMAS_DE_PAGAMENTO_CONSULTAR')") 
	public List<FormaPagamentoEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('FORMAS_DE_PAGAMENTO_CONSULTAR')") 
	public List<FormaPagamentoEntity> pesquisar(@RequestBody FormaPagamentoEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('FORMAS_DE_PAGAMENTO_INSERIR', 'FORMAS_DE_PAGAMENTO_ALTERAR')")
	public FormaPagamentoEntity salvar(@RequestBody FormaPagamentoEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
	
	@PostMapping("/cancelar")
	@PreAuthorize("hasAuthority('FORMAS_DE_PAGAMENTO_EXCLUIR')")
	public void cancelar(@RequestBody FormaPagamentoEntity entity) {
		try {
			service.cancelar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
