package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.TabelaPrecoEntity;
import br.com.meldica.service.publico.ProdutoService;
import br.com.meldica.service.publico.TabelaPrecoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/tabelaPreco")
public class TabelaPrecoController {
	
	@Autowired TabelaPrecoService service;
	@Autowired ProdutoService produtoService;
	
	@PostMapping("/init")
	public Map<String, Object> init() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("produtoList", produtoService.listar());
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
		
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAuthority('TABELA_PRECO_CONSULTAR')")
	public List<TabelaPrecoEntity> listar(@RequestBody TabelaPrecoEntity produto) {
		try {
			return service.listar(produto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('TABELA_PRECO_INSERIR', 'TABELA_PRECO_ALTERAR')")
	public TabelaPrecoEntity salvar(@RequestBody TabelaPrecoEntity entity) {
		try {
			return service.salvar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
}
