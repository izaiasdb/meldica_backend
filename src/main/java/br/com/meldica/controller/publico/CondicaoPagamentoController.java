package br.com.meldica.controller.publico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.CondicaoPagamentoEntity;
import br.com.meldica.service.publico.CondicaoPagamentoService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/condicaoPagamento")
public class CondicaoPagamentoController {
	private @Autowired CondicaoPagamentoService service;
	
	@GetMapping("/listartodos")
	@PreAuthorize("hasAnyAuthority('CONDICOES_DE_PAGAMENTO_CONSULTAR')") 
	public List<CondicaoPagamentoEntity> listarTodos() {
		try {
			return service.listar();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('CONDICOES_DE_PAGAMENTO_CONSULTAR')") 
	public List<CondicaoPagamentoEntity> pesquisar(@RequestBody CondicaoPagamentoEntity entity) {
		try {
			return service.listar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}		
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('CONDICOES_DE_PAGAMENTO_INSERIR', 'CONDICOES_DE_PAGAMENTO_ALTERAR')")
	public CondicaoPagamentoEntity salvar(@RequestBody CondicaoPagamentoEntity entity) {
		try {
			return service.salvar(entity);			
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}				
	}
	
	@PostMapping("/cancelar")
	@PreAuthorize("hasAuthority('CONDICOES_DE_PAGAMENTO_EXCLUIR')")
	public void cancelar(@RequestBody CondicaoPagamentoEntity entity) {
		try {
			service.cancelar(entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
