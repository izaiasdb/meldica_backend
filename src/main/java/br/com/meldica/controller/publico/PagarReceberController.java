package br.com.meldica.controller.publico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.entity.publico.PagarReceberItemEntity;
import br.com.meldica.service.publico.ClienteService;
import br.com.meldica.service.publico.FormaCondicaoPagamentoService;
import br.com.meldica.service.publico.FornecedorService;
import br.com.meldica.service.publico.PagarReceberItemService;
import br.com.meldica.service.publico.PagarReceberService;
import br.com.meldica.service.publico.PlanoContaService;
import br.com.meldica.utils.error.handler.CustomException;

@CrossOrigin
@RestController
@RequestMapping("/pagarReceber")
public class PagarReceberController {
	
	@Autowired PagarReceberService service;
	@Autowired PagarReceberItemService pagarReceberItemService;
	@Autowired ClienteService clienteService;
	@Autowired FornecedorService fornecedorService;
	@Autowired FormaCondicaoPagamentoService formaCondicaoPagamentoService;
	@Autowired PlanoContaService planoContaService;
	
	@PostMapping("/initReceber")
	public Map<String, Object> initReceber() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("clienteList", clienteService.listar());
			params.put("formaCondicaoList", formaCondicaoPagamentoService.listar());
			params.put("planoContaList", planoContaService.listar());
			
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/initPagar")
	public Map<String, Object> initPagar() {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("fornecedorList", fornecedorService.listar());
			params.put("formaCondicaoList", formaCondicaoPagamentoService.listar());
			params.put("planoContaList", planoContaService.listar());
			
			return params;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_CONSULTAR')")
	public PagarReceberEntity obter(@PathVariable Integer id) {
		try {
			return service.obter(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);			
		}
	}	
		
	@PostMapping("/pesquisar")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_CONSULTAR', 'CONTAS_PAGAR_CONSULTAR')")
	public List<PagarReceberEntity> listar(@RequestBody PagarReceberEntity pagarReceber) {
		try {
			return service.listar(pagarReceber);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_INSERIR', 'CONTAS_RECEBER_ALTERAR', 'CONTAS_PAGAR_INSERIR', 'CONTAS_PAGAR_ALTERAR')")
	public void salvar(@RequestBody PagarReceberEntity pagarReceber) {
		try {
			service.salvar(pagarReceber);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/pagar")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_ALTERAR', 'CONTAS_PAGAR_ALTERAR')")
	public void finalizar(@RequestBody PagarReceberEntity pagarReceber) {
		try {
			service.pagar(pagarReceber);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/salvarPagarReceberItem")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_INSERIR', 'CONTAS_RECEBER_ALTERAR', 'CONTAS_PAGAR_INSERIR', 'CONTAS_PAGAR_ALTERAR')")
	public void salvarPagarReceberItem(@RequestBody PagarReceberItemEntity pagarReceberItem) {
		try {
			pagarReceberItemService.salvar(pagarReceberItem);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/excluir")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_EXCLUIR', 'CONTAS_PAGAR_EXCLUIR')")
	public void excluir(@RequestBody PagarReceberEntity pagarReceber) {
		try {
			service.excluir(pagarReceber);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/excluirItem")
	@PreAuthorize("hasAnyAuthority('CONTAS_RECEBER_EXCLUIR', 'CONTAS_PAGAR_EXCLUIR')")
	public void excluirItem(@RequestBody PagarReceberItemEntity pagarReceberItem) {
		try {
			pagarReceberItemService.excluir(pagarReceberItem);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
