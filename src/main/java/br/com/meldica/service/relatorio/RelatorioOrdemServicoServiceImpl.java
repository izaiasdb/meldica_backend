package br.com.meldica.service.relatorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.OrdemServicoDao;
import br.com.meldica.model.dao.publico.OrdemServicoMapperDao;
import br.com.meldica.model.dto.relatorios.RelatorioEstoqueProdutoAcabadoDto;
import br.com.meldica.model.dto.relatorios.RelatorioListagemVendaDto;
import br.com.meldica.model.dto.relatorios.RelatorioResumoDto;
import br.com.meldica.model.dto.relatorios.RelatorioResumoFormaDto;
import br.com.meldica.model.dto.relatorios.RelatorioResumoProdutoDto;
import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoTransportadoraEntity;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.enums.EmpresaEnum;
import br.com.meldica.service.publico.OrdemServicoFormaPagamentoService;
import br.com.meldica.service.publico.OrdemServicoKitProdutoService;
import br.com.meldica.service.publico.OrdemServicoKitService;
import br.com.meldica.service.publico.OrdemServicoProdutoService;
import br.com.meldica.service.publico.OrdemServicoStatusService;
import br.com.meldica.service.publico.OrdemServicoTransportadoraService;
import br.com.meldica.service.publico.PagarReceberItemService;
import br.com.meldica.service.publico.PagarReceberService;
import br.com.meldica.utils.DateUtils;

@Service
public class RelatorioOrdemServicoServiceImpl implements RelatorioOrdemServicoService {

	@Autowired OrdemServicoDao dao;
	@Autowired OrdemServicoMapperDao mapperDao;
	@Autowired OrdemServicoProdutoService ordemServicoProdutoService;
	@Autowired OrdemServicoFormaPagamentoService ordemServicoFormaPagamentoService;
	@Autowired OrdemServicoTransportadoraService ordemServicoTransportadoraService;
	@Autowired OrdemServicoStatusService ordemServicoStatusService;
	@Autowired OrdemServicoKitService ordemServicoKitService;
	@Autowired OrdemServicoKitProdutoService ordemServicoKitProdutoService;
	@Autowired PagarReceberService pagarReceberService;
	@Autowired PagarReceberItemService pagarReceberItemService;

	@Override
	public List<RelatorioResumoDto> relatorioResumoMensal(OrdemServicoEntity filtro) {
		List<RelatorioResumoDto> list = new ArrayList<>();
		Date dataIni = new Date();
		
		try {
			dataIni = DateUtils.stringToDate(filtro.getDataInicio(), false);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		filtro.getPeriodoVenda().add(dataIni);
		filtro.getPeriodoVenda().add(DateUtils.getUltimoDiaMes(dataIni));
		
		List<OrdemServicoEntity> osList = mapperDao.relatorioResumoMensal(filtro);
		
		RelatorioResumoDto pai = new RelatorioResumoDto();
		pai.setId(1);
		pai.setCampanha(DateUtils.getMesAno(filtro.getPeriodoVenda().get(0)));
		
		for (OrdemServicoEntity ordemServico : osList) {
			List<OrdemServicoProdutoEntity> produtoList = new ArrayList<>();
			List<OrdemServicoFormaPagamentoEntity> formaList = new ArrayList<>();
					
			if (filtro.getTipoRelatorio().equals("NAT")) {
				pai.setTipoRelatorio("RESUMO MENSAL - MÉLDICA NATURAIS E ENCAPSULADOS");
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId()) || 
								c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
						.collect(Collectors.toList());
				
				formaList = ordemServico.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(EmpresaEnum.NATURAIS.getId()) || 
								c.getIdEmpresa().equals(EmpresaEnum.ENCAPSULADOS.getId()))
						.collect(Collectors.toList());
			} else {
				pai.setTipoRelatorio("RESUMO MENSAL - MÉLDICA COSMÉTICOS");
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
						.collect(Collectors.toList());
				
				formaList = ordemServico.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(EmpresaEnum.COSMETICOS.getId()))
						.collect(Collectors.toList());
			}
			
			if (produtoList != null && produtoList.size() > 0) {
				pai.getProdutoItemList().addAll(produtoList);
			}
			
			if (formaList != null && formaList.size() > 0) {
				pai.getFormaItemList().addAll(formaList);	
				
				for (OrdemServicoFormaPagamentoEntity forma : formaList) {
					List<PagarReceberEntity> pgList = ordemServico.getPagarReceberList().stream()
							.filter(c -> c.getIdOrdemServicoFormaCondicaoPagamento().equals(forma.getId()))
							.collect(Collectors.toList());
					
					if (forma.getTipoForma().equals("P") && pgList != null && pgList.size() > 0) {
						pai.getPagarReceberList().addAll(pgList);	
					} else if (forma.getTipoForma().equals("F") && pgList != null && pgList.size() > 0) {
						pai.getPagarReceberFreteList().addAll(pgList);
					}
				}
			}
			
			if (ordemServico.getTransportadoraItemsList() != null && ordemServico.getTransportadoraItemsList().size() > 0) {
				for (OrdemServicoTransportadoraEntity item : ordemServico.getTransportadoraItemsList()) {
					pai.setTotalFrete(pai.getTotalFrete().add(item.getValorFrete()));	
				}
			}
		}
		
		if (pai.getProdutoItemList() != null && pai.getProdutoItemList().size() > 0) {
			int i = 0;
			
			for (OrdemServicoProdutoEntity ordemServicoProduto : pai.getProdutoItemList().stream()
					.filter(c -> !c.isBonificacao()).collect(Collectors.toList())) {
				RelatorioResumoProdutoDto item = null;//new RelatorioResumoOrdemServicoItemDto();
				boolean inseriu = false;
				
				if (ordemServicoProduto.isFracionado()) {
					//Desconto na unidade
					pai.setDescontoProduto(pai.getDescontoProduto().add(ordemServicoProduto.getQuantidadeUnidade().multiply(ordemServicoProduto.getDesconto())));

					try {
//						item = pai.getProdutoList().stream()
//								.filter(c -> c.getIdProduto().equals(ordemServicoProduto.getProduto().getId())
//										&& c.getValor().equals(ordemServicoProduto.getValorUnidade().subtract(ordemServicoProduto.getDesconto())) )
//								.findFirst().orElse(null);
						item = pai.getProdutoList().stream()
								.filter(c -> c.getIdProduto().equals(ordemServicoProduto.getProduto().getId())
										// Valor da caixa -> (vlUnidade - desc) * qtdNaCaixa
										&& c.getValor().equals(ordemServicoProduto.getQuantidadeNaCaixa()
												.multiply(ordemServicoProduto.getValorUnidade().subtract(ordemServicoProduto.getDesconto()))))
								.findFirst().orElse(null);						
					} catch (Exception e) {
						System.out.println(i);
					}
					
				} else {
					pai.setDescontoProduto(pai.getDescontoProduto().add(ordemServicoProduto.getQuantidadeCaixa().multiply(ordemServicoProduto.getDesconto())));
					
					try {
						item = pai.getProdutoList().stream()
								.filter(c -> c.getIdProduto().equals(ordemServicoProduto.getProduto().getId())
										&& c.getValor().equals(ordemServicoProduto.getValorCaixa().subtract(ordemServicoProduto.getDesconto())) )
								.findFirst().orElse(null);	
					} catch (Exception e) {
						System.out.println(i);
					}
				}
					
				if (item == null) {
					i++;
					inseriu = true;
					item = new RelatorioResumoProdutoDto();
					item.setId(i);
					item.setIdGrupoProduto(ordemServicoProduto.getProduto().getGrupoProduto().getId());
					item.setGrupoProduto(ordemServicoProduto.getProduto().getGrupoProduto().getNome());
					item.setIdProduto(ordemServicoProduto.getProduto().getId());
					item.setProduto(ordemServicoProduto.getProduto().getNome());
					item.setValor(BigDecimal.ZERO);
					item.setQuantidade(BigDecimal.ZERO);
					
					GrupoProdutoEntity grupoProduto = null;
					Integer idGrupoProduto = item.getIdGrupoProduto();
					
					grupoProduto = pai.getGrupoProdutoList().stream()
							.filter(c -> c.getId().equals(idGrupoProduto)).findFirst().orElse(null);
					
					if (grupoProduto == null) {
						pai.getGrupoProdutoList().add(ordemServicoProduto.getProduto().getGrupoProduto());
					}
				}
				
				// Não vai ser dúzias, vai ser caixas, pode ficar 0,25 se for um produto de 48
				item.setQuantidade(item.getQuantidade().add(ordemServicoProduto.getQuantidadeCaixa()));
				
				if (ordemServicoProduto.isFracionado()) {
					//item.setValor(ordemServicoProduto.getValorUnidade().subtract(ordemServicoProduto.getDesconto()));
					// O desconto é dado na unidade, É necessário o valor da caixa
					// Valor da caixa -> (vlUnidade - desc) * qtdNaCaixa
					BigDecimal valorProduto = ordemServicoProduto.getQuantidadeNaCaixa().multiply(ordemServicoProduto.getValorUnidade().subtract(ordemServicoProduto.getDesconto()));
					item.setValor(valorProduto);
				} else {
					item.setValor(ordemServicoProduto.getValorCaixa().subtract(ordemServicoProduto.getDesconto()));
				}
				
				if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())) { //MELDICA NATURAIS
					pai.setTotalDuzias(pai.getTotalDuzias().add(ordemServicoProduto.getQuantidadeCaixa()));
				} else if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId())) { //MELDICA COSMETICOS
					pai.setTotalDuzias(pai.getTotalDuzias().add(ordemServicoProduto.getQuantidadeCaixa()));
				} else if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId())) { //MELDICA ENCAPSULADOS
					pai.setTotalDuziasEncapsulados(pai.getTotalDuziasEncapsulados().add(ordemServicoProduto.getQuantidadeCaixa()));
				}
				
				// Total de produtos é independente de fração
				pai.setTotalProdutos(pai.getTotalProdutos().add(ordemServicoProduto.getTotal()));
				
				if (inseriu) {
					pai.getProdutoList().add(item);
				}
			}
			
			for (RelatorioResumoProdutoDto item : pai.getProdutoList()){
				item.setTotal(item.getQuantidade().multiply(item.getValor()));
			}
			
//			https://stackoverflow.com/questions/369512/how-to-compare-objects-by-multiple-fields
//			pai.setProdutoList(pai.getProdutoList().stream()
//		            .sorted(Comparator.comparing(RelatorioResumoProdutoDto::getProduto))
//		            .collect(Collectors.toList()));
			
			pai.setProdutoList(pai.getProdutoList().stream()
		            .sorted(Comparator.comparing(RelatorioResumoProdutoDto::getGrupoProduto)
            		.thenComparing(RelatorioResumoProdutoDto::getProduto)
		            .thenComparing(RelatorioResumoProdutoDto::getValor))
		            .collect(Collectors.toList()));
		}
			
		if (pai.getFormaItemList() != null && pai.getFormaItemList().size() > 0) {
			int i = 0;
			
			for (OrdemServicoFormaPagamentoEntity item : pai.getFormaItemList()) {
				if (item.getTipoForma().equals("P")) {
					pai.setDescontoForma(pai.getDescontoForma().add(item.getDesconto()));
				} else {
					pai.setDescontoFormaFrete(pai.getDescontoFormaFrete().add(item.getDesconto()));
				}
			}
			
			for (OrdemServicoFormaPagamentoEntity item : pai.getFormaItemList()) {
				if (item.getTipoForma().equals("P")) {
					pai.setDescontoCartao(pai.getDescontoCartao().add(item.getDescontoFormaCondicao()));
				} else {
					pai.setDescontoCartaoFrete(pai.getDescontoCartaoFrete().add(item.getDescontoFormaCondicao()));
				}
			}
			
			for (OrdemServicoFormaPagamentoEntity ordemServicoForma : pai.getFormaItemList()) {
				RelatorioResumoFormaDto item = null;
				boolean inseriu = false;

				item = pai.getFormaList().stream()
						.filter(c -> c.getIdForma().equals(ordemServicoForma.getFormaCondicaoPagamento().getFormaPagamento().getId()))
						.findFirst().orElse(null);	
				
				if (item == null) {
					i++;
					inseriu = true;
					item = new RelatorioResumoFormaDto();
					item.setId(i);
					item.setIdForma(ordemServicoForma.getFormaCondicaoPagamento().getFormaPagamento().getId());
					item.setForma(ordemServicoForma.getFormaCondicaoPagamento().getFormaPagamento().getNome());
					item.setValor(item.getValor().add(ordemServicoForma.getValor()));
				} else {
					item.setValor(item.getValor().add(ordemServicoForma.getValor()));
				}

				if (inseriu) {
					pai.getFormaList().add(item);
				}
			}
				
			pai.setFormaList(pai.getFormaList().stream()
		            .sorted(Comparator.comparing(RelatorioResumoFormaDto::getForma))
		            .collect(Collectors.toList()));
		}
		
		if (pai.getPagarReceberList() != null && pai.getPagarReceberList().size() > 0) {
//			 Pago no período
			List<PagarReceberEntity> pagoList = pai.getPagarReceberList().stream().filter(c-> 
				c.getDataVencimento().after(filtro.getPeriodoVenda().get(0)) && 
				c.getDataVencimento().before(filtro.getPeriodoVenda().get(1))).collect(Collectors.toList());
			
			for (PagarReceberEntity item : pai.getPagarReceberList()) {
				pai.setTotalGerado(pai.getTotalGerado().add(item.getValor()));
			}
			
			for (PagarReceberEntity item : pagoList) {
				pai.setTotalPago(pai.getTotalPago().add(item.getValorPago()));
			}
			
			pai.setTotalReceber(pai.getTotalGerado().subtract(pai.getTotalPago()));
		}
		
		if (pai.getPagarReceberFreteList() != null && pai.getPagarReceberFreteList().size() > 0) {
//			 Pago no período
			List<PagarReceberEntity> pagoList = pai.getPagarReceberFreteList().stream().filter(c-> 
				c.getDataVencimento().after(filtro.getPeriodoVenda().get(0)) && 
				c.getDataVencimento().before(filtro.getPeriodoVenda().get(1))).collect(Collectors.toList());
			
			for (PagarReceberEntity item : pai.getPagarReceberFreteList()) {
				pai.setTotalGeradoFrete(pai.getTotalGeradoFrete().add(item.getValor()));
			}
			
			for (PagarReceberEntity item : pagoList) {
				pai.setTotalPagoFrete(pai.getTotalPagoFrete().add(item.getValorPago()));
			}
			
			pai.setTotalReceberFrete(pai.getTotalGeradoFrete().subtract(pai.getTotalPagoFrete()));
		}		
		
		list.add(pai);
		return list;
	}
	
	@Override
	public List<OrdemServicoEntity> relatorioListagemVenda(OrdemServicoEntity filtro) {
		List<OrdemServicoEntity> osList = mapperDao.relatorioListagemVenda(filtro);
		
		for (OrdemServicoEntity ordemServico : osList) {
			List<OrdemServicoProdutoEntity> produtoList = new ArrayList<>();
			List<OrdemServicoFormaPagamentoEntity> formaList = new ArrayList<>();
			List<PagarReceberEntity> pagarReceberList = new ArrayList<>();
			List<PagarReceberEntity> pagarReceberFreteList = new ArrayList<>();
			
			//RESUMO MENSAL - MÉLDICA NATURAIS E ENCAPSULADOS
			if (filtro.getTipoRelatorio().equals("NAT")) {
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId()) || c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
						.collect(Collectors.toList());
				
				formaList = ordemServico.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(EmpresaEnum.NATURAIS.getId()) || c.getIdEmpresa().equals(EmpresaEnum.ENCAPSULADOS.getId()))
						.collect(Collectors.toList());		
				
				
				ordemServico.setProdutoItemsList(produtoList);
				ordemServico.setProdutoCosmeticoList(new ArrayList<>());
			} else {
				//RESUMO MENSAL - MÉLDICA COSMÉTICOS
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> !c.isBonificacao() && c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
						.collect(Collectors.toList());
				
				formaList = ordemServico.getFormaItemsList().stream()
						.filter(c -> c.getIdEmpresa().equals(EmpresaEnum.COSMETICOS.getId()))
						.collect(Collectors.toList());
								
				ordemServico.setProdutoCosmeticoList(produtoList);
				ordemServico.setProdutoItemsList(new ArrayList<>());
			}
			
			ordemServico.setFormaItemsList(formaList);
			
			// 09/09/2021
//			if (produtoList != null && produtoList.size() > 0) {
//				for (OrdemServicoProdutoEntity item : produtoList) {
//					ordemServico.setTotalProdutos(ordemServico.getTotalProdutos().add(item.getTotal()));
//					ordemServico.setTotalCaixa(ordemServico.getTotalCaixa().add(item.getQuantidadeCaixa()));
//				}
//			}
			
			// 09/09/2021
			if (formaList != null && formaList.size() > 0) {
				for (OrdemServicoFormaPagamentoEntity forma : formaList) {
					List<PagarReceberEntity> pgList = ordemServico.getPagarReceberList().stream()
							.filter(c -> c.getIdOrdemServicoFormaCondicaoPagamento().equals(forma.getId()))
							.collect(Collectors.toList());
					
					if (forma.getTipoForma().equals("P") && pgList != null && pgList.size() > 0) {
						pagarReceberList.addAll(pgList);
//						ordemServico.setTotalForma(ordemServico.getTotalForma().add(forma.getTotal())); // 09/09/2021
//						ordemServico.setTotalFormaDescontoCartao(ordemServico.getTotalFormaDescontoCartao().add(forma.getDescontoFormaCondicao())); // 09/09/2021
					} else if (forma.getTipoForma().equals("F") && pgList != null && pgList.size() > 0) {
						pagarReceberFreteList.addAll(pgList);
//						ordemServico.setTotalFormaFrete(ordemServico.getTotalFormaFrete().add(forma.getTotal())); // 09/09/2021
//						ordemServico.setTotalFormaFreteDescontoCartao(ordemServico.getTotalFormaFreteDescontoCartao().add(forma.getDescontoFormaCondicao())); // 09/09/2021
					}
				}
			}
			
			// 09/09/2021
//			if (ordemServico.getTransportadoraItemsList() != null && ordemServico.getTransportadoraItemsList().size() > 0) {
//				for (OrdemServicoTransportadoraEntity item : ordemServico.getTransportadoraItemsList()) {
//					ordemServico.setTotalFrete(ordemServico.getTotalFrete().add(item.getValorFrete()));
//				}
//			}
			
			ordemServico.setTotalPago(BigDecimal.ZERO);
			ordemServico.setFaltaReceber(BigDecimal.ZERO);
			ordemServico.setTotalPagoFrete(BigDecimal.ZERO);
			ordemServico.setFaltaReceberFrete(BigDecimal.ZERO);
			
			if (pagarReceberList != null && pagarReceberList.size() > 0) {
//				 Pago no período
				List<PagarReceberEntity> pagoList = pagarReceberList.stream().filter(c-> 
					c.getDataVencimento().after(filtro.getPeriodoVenda().get(0)) && 
					c.getDataVencimento().before(filtro.getPeriodoVenda().get((EmpresaEnum.NATURAIS.getId())))).collect(Collectors.toList());
				
				for (PagarReceberEntity item : pagarReceberList) {
					ordemServico.setTotalGerado(ordemServico.getTotalGerado().add(item.getValor()));
				}
				
				for (PagarReceberEntity item : pagoList) {
					ordemServico.setTotalPago(ordemServico.getTotalPago().add(item.getValorPago()));
				}
				
				ordemServico.setFaltaReceber(ordemServico.getTotalGerado().subtract(ordemServico.getTotalPago()));
			}
			
			if (pagarReceberFreteList != null && pagarReceberFreteList.size() > 0) {
//				 Pago no período
				List<PagarReceberEntity> pagoList = pagarReceberFreteList.stream().filter(c-> 
					c.getDataVencimento().after(filtro.getPeriodoVenda().get(0)) && 
					c.getDataVencimento().before(filtro.getPeriodoVenda().get(EmpresaEnum.NATURAIS.getId()))).collect(Collectors.toList());
				
				for (PagarReceberEntity item : pagarReceberFreteList) {
					ordemServico.setTotalGeradoFrete(ordemServico.getTotalGeradoFrete().add(item.getValor()));
				}
				
				for (PagarReceberEntity item : pagoList) {
					ordemServico.setTotalPagoFrete(ordemServico.getTotalPagoFrete().add(item.getValorPago()));
				}
				
				ordemServico.setFaltaReceberFrete(ordemServico.getTotalGeradoFrete().subtract(ordemServico.getTotalPagoFrete()));
			}
		}
		
		return osList;
	}

	@Override
	public List<OrdemServicoEntity> listarDashboard(OrdemServicoEntity filtro) {
		Assert.isTrue(filtro != null , "Preencha os dados.");
		filtro.setCliente(filtro.getCliente() == null || filtro.getCliente().getId() == null ? null : filtro.getCliente());
		filtro.setFuncionario(filtro.getFuncionario() == null || filtro.getFuncionario().getId() == null ? null : filtro.getFuncionario());
		filtro.setPlanoConta(filtro.getPlanoConta() == null || filtro.getPlanoConta().getId() == null ? null : filtro.getPlanoConta());
		filtro.setTabelaPreco(filtro.getTabelaPreco() == null || filtro.getTabelaPreco().getId() == null ? null : filtro.getTabelaPreco());
		filtro.setPeriodoVenda(filtro.getPeriodoVenda() == null || (filtro.getPeriodoVenda() != null && filtro.getPeriodoVenda().size() > 1 && filtro.getPeriodoVenda().get(0) == null) ? null : filtro.getPeriodoVenda());
		filtro.setPeriodoEntrega(filtro.getPeriodoEntrega() == null || (filtro.getPeriodoEntrega() != null && filtro.getPeriodoEntrega().size() > 1 && filtro.getPeriodoEntrega().get(0) == null) ? null : filtro.getPeriodoEntrega());
		filtro.setPeriodoLiberacao(filtro.getPeriodoLiberacao() == null || (filtro.getPeriodoLiberacao() != null && filtro.getPeriodoLiberacao().size() > 1 && filtro.getPeriodoLiberacao().get(0) == null) ? null : filtro.getPeriodoLiberacao());
		filtro.setPeriodoPrevisaoEntrega(filtro.getPeriodoPrevisaoEntrega() == null || (filtro.getPeriodoPrevisaoEntrega() != null && filtro.getPeriodoPrevisaoEntrega().size() > 1 && filtro.getPeriodoPrevisaoEntrega().get(0) == null) ? null : filtro.getPeriodoPrevisaoEntrega());
//		filtro.setPeriodoVenda(filtro.getPeriodoVenda() == null || filtro.getPeriodoVenda().get(0) == null ? null : filtro.getPeriodoVenda());
//		filtro.setPeriodoEntrega(filtro.getPeriodoEntrega() == null || filtro.getPeriodoEntrega().get(0) == null ? null : filtro.getPeriodoEntrega());
//		filtro.setPeriodoLiberacao(filtro.getPeriodoLiberacao() == null || filtro.getPeriodoLiberacao().get(0) == null ? null : filtro.getPeriodoLiberacao());
//		filtro.setPeriodoPrevisaoEntrega(filtro.getPeriodoPrevisaoEntrega() == null || filtro.getPeriodoPrevisaoEntrega().get(0) == null ? null : filtro.getPeriodoPrevisaoEntrega());
		
		
		return mapperDao.relatorioDashboard(filtro);
	}
	
	@Override
	public List<RelatorioResumoDto> relatorioProduzir(OrdemServicoEntity filtro) {
		List<RelatorioResumoDto> list = new ArrayList<>();
		
		List<OrdemServicoEntity> osList = mapperDao.relatorioAProduzir(filtro);
		RelatorioResumoDto pai = new RelatorioResumoDto();
		pai.setId(1);
		
		for (OrdemServicoEntity ordemServico : osList) {
			List<OrdemServicoProdutoEntity> produtoList = new ArrayList<>();
					
			if (filtro.getTipoRelatorio().equals("NAT")) {
				pai.setTipoRelatorio("RESUMO PRODUZIR - MÉLDICA NATURAIS E ENCAPSULADOS");
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> //!c.isBonificacao() && //Independete de bonificação
								c.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId()) || c.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId()))
						.collect(Collectors.toList());
			} else {
				pai.setTipoRelatorio("RESUMO PRODUZIR - MÉLDICA COSMÉTICOS");
				produtoList = ordemServico.getProdutoItemsList().stream()
						.filter(c -> //!c.isBonificacao() && //Independete de bonificação
								c.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId()))
						.collect(Collectors.toList());
			}
			
			if (produtoList != null && produtoList.size() > 0) {
				pai.getProdutoItemList().addAll(produtoList);
			}
		}
		
		if (pai.getProdutoItemList() != null && pai.getProdutoItemList().size() > 0) {
			int i = 0;
			
			for (OrdemServicoProdutoEntity ordemServicoProduto : pai.getProdutoItemList().stream()
					.filter(c -> !c.isBonificacao()).collect(Collectors.toList())) {
				RelatorioResumoProdutoDto item = null;//new RelatorioResumoOrdemServicoItemDto();
				boolean inseriu = false;
				
//				totVolumeProd = totVolumeProd.add(item.getQuantidadeUnidade().divide(item.getQuantidadeNaCaixa()));
				if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.NATURAIS.getId())) { //MELDICA NATURAIS
					pai.setTotalDuzias(pai.getTotalDuzias().add(ordemServicoProduto.getQuantidadeCaixa()));
				} else if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.COSMETICOS.getId())) { //MELDICA COSMETICOS
					pai.setTotalDuzias(pai.getTotalDuzias().add(ordemServicoProduto.getQuantidadeCaixa()));
				} else if (ordemServicoProduto.getIdEmpresaProduto().equals(EmpresaEnum.ENCAPSULADOS.getId())) { //MELDICA ENCAPSULADOS
					pai.setTotalDuziasEncapsulados(pai.getTotalDuzias().add(ordemServicoProduto.getQuantidadeCaixa()));
				}

				pai.setTotalProdutos(pai.getTotalProdutos().add(ordemServicoProduto.getTotal()));
				
				if (ordemServicoProduto.isFracionado()) {
					try {
						item = pai.getProdutoList().stream()
								.filter(c -> c.getIdProduto().equals(ordemServicoProduto.getProduto().getId()))
								.findFirst().orElse(null);	
					} catch (Exception e) {
						System.out.println(i);
					}
				} else {
					try {
						item = pai.getProdutoList().stream()
								.filter(c -> c.getIdProduto().equals(ordemServicoProduto.getProduto().getId()))
								.findFirst().orElse(null);	
					} catch (Exception e) {
						System.out.println(i);
					}
				}
					
				if (item == null) {
					i++;
					inseriu = true;
					item = new RelatorioResumoProdutoDto();
					item.setId(i);
					item.setIdGrupoProduto(ordemServicoProduto.getProduto().getGrupoProduto().getId());
					item.setGrupoProduto(ordemServicoProduto.getProduto().getGrupoProduto().getNome());
					item.setIdProduto(ordemServicoProduto.getProduto().getId());
					item.setProduto(ordemServicoProduto.getProduto().getNome());
					item.setQuantidade(BigDecimal.ZERO);
					item.setEstoque(ordemServicoProduto.getProduto().getQtdEstoqueCaixa());
					item.setProduzir(BigDecimal.ZERO);
					
					GrupoProdutoEntity grupoProduto = null;
					Integer idGrupoProduto = item.getIdGrupoProduto();
					
					grupoProduto = pai.getGrupoProdutoList().stream()
							.filter(c -> c.getId().equals(idGrupoProduto)).findFirst().orElse(null);
					
					if (grupoProduto == null) {
						pai.getGrupoProdutoList().add(ordemServicoProduto.getProduto().getGrupoProduto());
					}
				}
				
				// Independente de fracionado da certo
				item.setQuantidade(item.getQuantidade().add(ordemServicoProduto.getQuantidadeCaixa()));
				item.setProduzir(BigDecimal.ZERO);
				
				// https://www.geeksforgeeks.org/bigdecimal-compareto-function-in-java/
				// Quantidade maior que estoque  
				if (item.getQuantidade().compareTo(item.getEstoque()) == 1) {
					item.setProduzir(item.getEstoque().subtract(item.getQuantidade()));	
				}
				
				if (inseriu) {
					pai.getProdutoList().add(item);
				}
			}
			
			pai.setProdutoList(pai.getProdutoList().stream()
		            .sorted(Comparator.comparing(RelatorioResumoProdutoDto::getGrupoProduto)
            		.thenComparing(RelatorioResumoProdutoDto::getProduto))
		            .collect(Collectors.toList()));
		}
			
		list.add(pai);
		return list;
	}
	
	public void preencherRelacionamentos(OrdemServicoEntity item) {
		item.setProdutoItemsList(ordemServicoProdutoService.listarPorOrdemServico(item.getId()));
		item.setFormaItemsList(ordemServicoFormaPagamentoService.listarPorOrdemServico(item.getId()));
		item.setTransportadoraItemsList(ordemServicoTransportadoraService.listarPorOrdemServico(item.getId()));
		item.setKitList(ordemServicoKitService.listarPorOrdemServico(item.getId()));
		item.setOrdemServicoStatusList(ordemServicoStatusService.listarPorOrdemServico(item.getId()));
	
//		if (item.getKitProdutoList() == null) {
//			item.setKitProdutoList(new ArrayList<>());
//		}
		
		if (item.getKitList() != null && item.getKitList().size() > 0) {
			for (OrdemServicoKitEntity kit : item.getKitList()) {
				List<OrdemServicoKitProdutoEntity> kitProdutoList = ordemServicoKitProdutoService.listarPorOrdemServicoKit(kit.getId());
				
				if (kitProdutoList != null && kitProdutoList.size() > 0) {
					item.getKitProdutoList().addAll(kitProdutoList);	
				}				
			}
		}
		
		if (item.getFormaItemsList() != null && item.getFormaItemsList().size() > 0) {
			for (OrdemServicoFormaPagamentoEntity forma : item.getFormaItemsList()) {
				List<PagarReceberEntity> pagarReceberList = pagarReceberService.listarPorOrdemServicoForma(forma.getId());
				
				if (pagarReceberList != null && pagarReceberList.size() > 0) {
//					if (item.getPagarReceberList() == null) {
//						item.setPagarReceberList(new ArrayList<>());
//					}
					
					// Por enquanto não está deixando muito lento
//					for (PagarReceberEntity pagarReceber : pagarReceberList) {
//						List<PagarReceberItemEntity> pagarReceberItemList = pagarReceberItemService.listarPorPagarReceber(pagarReceber.getId());
//						
//						if (pagarReceber.getPagarReceberItemsList() == null) {
//							pagarReceber.setPagarReceberItemsList(new ArrayList<>());
//						}
//						
//						pagarReceber.getPagarReceberItemsList().addAll(pagarReceberItemList);
//					}
					
					item.getPagarReceberList().addAll(pagarReceberList);	
				}
			}
		}
	}
	
	@Override
	public List<RelatorioEstoqueProdutoAcabadoDto> relatorioEstoqueProdutoAcabado(Integer idEmpresa) {
		return mapperDao.relatorioEstoqueProdutoAcabado(idEmpresa);
	}

}
