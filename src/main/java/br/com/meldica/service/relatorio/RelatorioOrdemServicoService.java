package br.com.meldica.service.relatorio;

import java.util.List;

import br.com.meldica.model.dto.relatorios.RelatorioEstoqueProdutoAcabadoDto;
import br.com.meldica.model.dto.relatorios.RelatorioListagemVendaDto;
import br.com.meldica.model.dto.relatorios.RelatorioResumoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;

public interface RelatorioOrdemServicoService {
	List<RelatorioResumoDto> relatorioResumoMensal(OrdemServicoEntity ordemServico);
	
	List<OrdemServicoEntity> relatorioListagemVenda(OrdemServicoEntity ordemServico);

	List<OrdemServicoEntity> listarDashboard(OrdemServicoEntity filtro);

	List<RelatorioResumoDto> relatorioProduzir(OrdemServicoEntity filtro);
	
	List<RelatorioEstoqueProdutoAcabadoDto> relatorioEstoqueProdutoAcabado(Integer idEmpresa);
}
