package br.com.meldica.service.acesso;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.acesso.MenuDao;
import br.com.meldica.model.entity.acesso.MenuEntity;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	MenuDao menuDao;

	@Override
	public List<MenuEntity> listar() {
		return menuDao.findAll();
	}
	
	@Override
	public List<MenuEntity> listarPorModulo(Integer idModulo) {
		Assert.isTrue(idModulo != null , "O módulo é obrigatório.");
		
		List<MenuEntity> list = menuDao.listarPorModulo(idModulo);
		
		list.forEach(c-> {
			if (c.getSomaPermissao() != null && c.getSomaPermissao() > 0)
				c.setIdPermissoes(c.listarPermissoesSoma());
		});
		
		return list;
	}	
	
	
	@Override
	public List<MenuEntity> listar(MenuEntity menu) {
		Assert.isTrue(menu != null , "Preencha os dados.");
		Integer moduloId = menu.getModulo() == null  || menu.getModulo().getId() == null ? null : menu.getModulo().getId();
		List<MenuEntity> list = menuDao.listarPorNomeModulo(moduloId, menu.getNome());
		
		for (MenuEntity c : list) {
			if (c.getSomaPermissao() != null && c.getSomaPermissao() > 0)
				c.setIdPermissoes(c.listarPermissoesSoma());
		}
		
		return list;
	}
	
	@Override
	public List<MenuEntity> listarPorNivel(Integer nivel) {
		Assert.isTrue(nivel != null , "Nível precisa ser informado.");
		
		List<MenuEntity> list = menuDao.listarPorNivel(nivel);
		
		list.forEach(c-> {
			if (c.getSomaPermissao() != null && c.getSomaPermissao() > 0)
				c.setIdPermissoes(c.listarPermissoesSoma());
		});
		
		return list;		
	}		

	@Override
	public MenuEntity salvar(MenuEntity menu) {
		if (menu.getIdPermissoes() != null && menu.getIdPermissoes().size() > 0) {
			menu.setSomaPermissao(menu.getIdPermissoes().stream().mapToInt(c-> c.intValue()).sum());	
		} else {
			menu.setSomaPermissao(0);
		}
		
		if (menu.getMenu() != null && menu.getMenu().getId() == null) {
			menu.setMenu(null);
		}
		
		if (menu.getId() != null){
			menu.setDataAlteracao(new Date());
		}
				
		return menuDao.saveAndFlush(menu);
	}


}
