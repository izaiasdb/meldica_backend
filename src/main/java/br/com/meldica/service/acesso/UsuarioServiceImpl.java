package br.com.meldica.service.acesso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dao.acesso.UsuarioDao;
import br.com.meldica.model.dto.acesso.CredenciaisDto;
import br.com.meldica.model.dto.acesso.MenuTreeDto;
import br.com.meldica.model.dto.acesso.UsuarioDto;
import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;
import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.model.entity.acesso.UsuarioMenuEntity;
import br.com.meldica.security.JwtTokenProvider;
import br.com.meldica.service.publico.FuncionarioService;
import br.com.meldica.utils.EnviaEmail;
import br.com.meldica.utils.StringUtils;
import br.com.meldica.utils.error.handler.CustomException;
import io.jsonwebtoken.lang.Assert;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	private @Autowired UsuarioDao usuarioDao;
	private @Autowired AuthenticationManager authenticationManager;
	private @Autowired PerfilService perfilService;
	private @Autowired MenuService menuService;
	private @Autowired UsuarioMenuService usuarioMenuService;
//	private @Autowired UsuarioMapperDao usuarioMapperDao;
	private @Autowired PerfilMenuService perfilMenuService;
	private @Autowired FuncionarioService funcionarioService;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	@Value("${info.app.id}")
	private Integer sistema;	

	@Override
	public Map<String, Object> init() {
		Map<String, Object> params = new HashMap<String, Object>();
//		List<MenuEntity> menus = menuService.listarPorModulo(1);
		
//		params.put("menus", MenuTreeDto.obterMenu(menus, 1));
		params.put("perfis", perfilService.listar());
		params.put("funcionarioList", funcionarioService.listar());
		
		return params;		
	}
	
	@Override
	public UsuarioEntity Obter(Integer id) {
		return usuarioDao.findById(id).orElse(null);
	}	
	
	@Override
	public UsuarioEntity obterPorLogin(String login) {
		Assert.isTrue(login != null && !login.isEmpty(),"Login de usuário é obrigatório!");
		return usuarioDao.obterPorLogin(login);
	}
	
	@Override
	public UsuarioEntity obterPorLoginEmail(String login, String email) {
		Assert.isTrue(login != null && !login.isEmpty(),"Login de usuário é obrigatório!");
		Assert.isTrue(email != null && !email.isEmpty(),"Email do usuário é obrigatório!");
		return usuarioDao.obterPorLoginEmail(login, email);
	}

	@Override
	public List<UsuarioEntity> listar() {
		return usuarioDao.findAll();
	}
	
	@Override
	public UsuarioEntity salvar(UsuarioEntity usuario) {
		// Guadar as permissões que vieram do frontend
//		List<String> permissoes = usuario.getPermissoes();
//		List<Long> unidadeIds = usuario.getUnidadeIds();
		boolean cadastrou = false;
		
		if (usuario.getId() == null){
			usuario.setDataInclusao(new Date());
			usuario.setDesenvolvedor(false);
			usuario.setTrocaSenha(true);
			//usuario.setSenha(StringUtils.randomSenha());
			//usuario.setCodigoVerificacao(StringUtils.randomCodigoVerificacao());
			usuario.setSenha("123456");
			cadastrou = true;
		}
		
		usuario.setDataAlteracao(new Date());
		usuario = usuarioDao.saveAndFlush(usuario);
		
		// Retorna para o objeto
//		usuario.setPermissoes(permissoes);		
//		usuario.setUnidadeIds(unidadeIds);
		
//		usuarioMenuService.salvar(usuario);
		
//		if (cadastrou && !StringUtils.isNullOrEmpty(usuario.getEmail())){
//			try {
//				EnviaEmail enviaEmail = new EnviaEmail();
//				enviaEmail.enviarCodigoVerificacaoCadastro(usuario);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}

		return usuario;
	}
	
	@Override
	public UsuarioEntity alterarsenha(UsuarioEntity usuario) {
		UsuarioEntity usuarioBanco = usuarioDao.obterPorLogin(usuario.getLogin());
		
		if(usuarioBanco == null) {
			throw new EntityNotFoundException("Usuário não encontrado.");
		}		
		
		if (!usuario.getSenhaAtual().equals(usuarioBanco.getSenha())) {
			throw new IllegalArgumentException("Senha atual incorreta.");
		}
		
		usuarioBanco.setSenha(usuario.getSenha());
		usuarioBanco = usuarioDao.saveAndFlush(usuarioBanco);
		
		return usuarioBanco;
	}	
	
	@Override
	public List<UsuarioEntity> listar(UsuarioEntity usuario) {
		List<UsuarioEntity> list = new ArrayList<UsuarioEntity>();
		list = usuarioDao.listar(usuario);
		list.forEach(c->{
//			List<UsuarioMenuEntity> usuarioMenuList = usuarioMenuService.listarPorUsuario(c.getId()); 
//			c.setPermissoes(MenuTreeDto.listarUsuarioPermissoes(usuarioMenuList));
		});
		
		return list;
	}
	
	@Override
	public UsuarioDto login(String login, String password) {
		try {
		  Authentication auth =  authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));

		  UsuarioEntity usuario = obterPorLogin(login);
		  
//		  List<UsuarioMenuEntity> usuarioMenuList = usuarioMenuService.listarPorUsuario(usuario.getId());
		  //Por Perfil
		  List<PerfilMenuEntity> perfilMenuList = perfilMenuService.listarPorPerfil(usuario.getPerfil().getId());		  

		  return new UsuarioDto(usuario, jwtTokenProvider.createToken(login, auth.getAuthorities()), auth, perfilMenuList);
	    } catch (AuthenticationException e) {
	      throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
	    }
	}
	
	@Override
	public void esqueciSenha(String login) {
		try {
			UsuarioEntity usuario = obterPorLogin(login);
			
			if(usuario == null) {
				throw new EntityNotFoundException("Usuário não encontrado.");
			}
			
			if(StringUtils.isNullOrEmpty(usuario.getEmail())) {
				throw new Exception("Email não cadastrado!");
			}
			
			try {
				usuario.setCodigoVerificacao(StringUtils.randomCodigoVerificacao());
				usuario = usuarioDao.saveAndFlush(usuario);
				
				EnviaEmail enviaEmail = new EnviaEmail();
				enviaEmail.enviarCodigoVerificacaoEsqueceuSenha(usuario);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    } catch (Exception e) {
	      throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
	    }
	}
	
	@Override
	public void alterarsenha(CredenciaisDto credenciais) {
		try {
			if(StringUtils.isNullOrEmpty(credenciais.getUsername())) {
				throw new Exception("Login não informado!");
			}
			
			if(StringUtils.isNullOrEmpty(credenciais.getCodigoVerificacao())) {
				throw new Exception("Código verificação não informado!");
			}		
			
			UsuarioEntity usuarioBanco = usuarioDao.obterPorLoginCodigoVerificacao(credenciais.getUsername(), credenciais.getCodigoVerificacao());
			
			if(usuarioBanco == null) {
				throw new EntityNotFoundException("Usuário não encontrado.");
			}		
			
//			if (!credenciais.getPassword().equals(usuarioBanco.getSenha())) {
//				throw new IllegalArgumentException("Senha atual incorreta.");
//			}
			if (!credenciais.getCodigoVerificacao().equals(usuarioBanco.getCodigoVerificacao())) {
				throw new IllegalArgumentException("Código de verificação incorreto.");
			}			
			
			usuarioBanco.setSenha(credenciais.getNovaSenha());
			usuarioBanco = usuarioDao.saveAndFlush(usuarioBanco);
		} catch (Exception e) {
			throw new CustomException(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
		}		
	}

	@Override
	public List<UsuarioEntity> listarPorGrupo(Integer idGrupo) {
//		return usuarioMapperDao.listarPorGrupo(idGrupo);
		return null;
	}
		

}
