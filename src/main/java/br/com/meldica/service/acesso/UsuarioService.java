package br.com.meldica.service.acesso;

import java.util.List;
import java.util.Map;

import br.com.meldica.model.dto.acesso.CredenciaisDto;
import br.com.meldica.model.dto.acesso.UsuarioDto;
import br.com.meldica.model.entity.acesso.UsuarioEntity;

public interface UsuarioService {
	
	Map<String, Object> init();
	
	/**
	 * Listar Sistemas
	 * @return List<UsuarioEntity>
	 */
	List<UsuarioEntity> listar();

	UsuarioEntity salvar(UsuarioEntity usuario);

	List<UsuarioEntity> listar(UsuarioEntity usuario);

	/**
	 * Busca um usuário pelo login
	 * @param login
	 * @return Usuario
	 */
	UsuarioEntity obterPorLogin(String login);

	/**
	 * Loga o usuário
	 * @param username
	 * @param password
	 * @return UsuarioDto
	 */
	UsuarioDto login(String username, String password);

	UsuarioEntity Obter(Integer id);

	UsuarioEntity alterarsenha(UsuarioEntity usuario);

	UsuarioEntity obterPorLoginEmail(String login, String email);

	void esqueciSenha(String login);

	void alterarsenha(CredenciaisDto credenciais);

	List<UsuarioEntity> listarPorGrupo(Integer idGrupo);

}
