package br.com.meldica.service.acesso;

import java.util.List;

import br.com.meldica.model.entity.acesso.SistemaEntity;

public interface SistemaService {
	/**
	 * Listar Sistemas
	 * @return List<SistemaEntity>
	 */
	List<SistemaEntity> listar();

	SistemaEntity salvar(SistemaEntity sistema);

	List<SistemaEntity> listar(SistemaEntity sistemaDto);
}
