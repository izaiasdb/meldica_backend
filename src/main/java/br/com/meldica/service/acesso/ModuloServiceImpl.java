package br.com.meldica.service.acesso;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.acesso.ModuloDao;
import br.com.meldica.model.entity.acesso.ModuloEntity;

@Service
public class ModuloServiceImpl implements ModuloService {

	@Autowired
	ModuloDao moduloDao;

	@Override
	public List<ModuloEntity> listar() {
		return moduloDao.findAll();
	}
	
	@Override
	public List<ModuloEntity> listar(ModuloEntity modulo) {
		Assert.isTrue(modulo != null , "Preencha os dados.");
		Integer sistemaId = modulo.getSistema() == null || modulo.getSistema().getId() == null ? null : modulo.getSistema().getId();
		return moduloDao.listarPorNomeSistema(sistemaId, modulo.getNome());
	}	

	@Override
	public ModuloEntity salvar(ModuloEntity modulo) {
		if (modulo.getId() != null){
			modulo.setDataAlteracao(new Date());
		}		
		
		return moduloDao.saveAndFlush(modulo);
	}


}
