package br.com.meldica.service.acesso;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dao.acesso.SistemaDao;
import br.com.meldica.model.entity.acesso.SistemaEntity;

@Service
public class SistemaServiceImpl implements SistemaService {

	@Autowired
	SistemaDao sistemaDao;

	@Override
	public List<SistemaEntity> listar() {
		return sistemaDao.findAll();
	}
	
	@Override
	public List<SistemaEntity> listar(SistemaEntity sistema) {
		if (sistema.getNome() == null || sistema.getNome().equals("")) {
			return sistemaDao.findAll();
		} else {
			return sistemaDao.listar(sistema.getNome());
		}
	}	

	@Override
	public SistemaEntity salvar(SistemaEntity sistema) {
		if (sistema.getId() != null){
			sistema.setDataAlteracao(new Date());
		}
		
		return sistemaDao.saveAndFlush(sistema);
	}


}
