package br.com.meldica.service.acesso;

import java.util.List;

import br.com.meldica.model.entity.acesso.MenuEntity;

public interface MenuService {
	/**
	 * Listar Menus
	 * @return List<MenuDto>
	 */
	List<MenuEntity> listar();

	MenuEntity salvar(MenuEntity modulo);

	List<MenuEntity> listar(MenuEntity modulo);

	List<MenuEntity> listarPorNivel(Integer nivel);

	List<MenuEntity> listarPorModulo(Integer idModulo);
}
