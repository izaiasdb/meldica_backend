package br.com.meldica.service.acesso;

import java.util.List;

import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.model.entity.acesso.UsuarioMenuEntity;

public interface UsuarioMenuService {
	/**
	 * Listar Usuario Menu
	 * @return List<UsuarioMenuEntity>
	 */
	List<UsuarioMenuEntity> listar();

	UsuarioMenuEntity salvar(UsuarioMenuEntity usuarioMenu);
	
	void salvar(List<UsuarioMenuEntity> usuarioMenuList);
	
	void salvar(UsuarioEntity usuario);

	List<UsuarioMenuEntity> listarPorUsuario(Integer idUsuario);

	List<String> listarPermissoesSecurity(FiltroUsuarioMenuDto usuarioMenu);
}
