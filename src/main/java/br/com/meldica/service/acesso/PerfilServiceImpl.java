package br.com.meldica.service.acesso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dao.acesso.PerfilDao;
import br.com.meldica.model.dto.acesso.MenuTreeDto;
import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.entity.acesso.PerfilEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;
import br.com.meldica.utils.GeralConst;

@Service
public class PerfilServiceImpl implements PerfilService {

	@Autowired
	PerfilDao perfilDao;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	PerfilMenuService perfilMenuService;	
	
	@Override
	public Map<String, Object> init() {
		Map<String, Object> params = new HashMap<String, Object>();
		List<MenuEntity> menus = menuService.listarPorModulo(GeralConst.MELDICA);
		
		params.put("menus", MenuTreeDto.obterMenu(menus, null, 1));		
		
		return params;		
	}
		

	@Override
	public List<PerfilEntity> listar() {
		return perfilDao.findAll();
	}
	
	@Override
	public List<PerfilEntity> listar(PerfilEntity perfil) {
		List<PerfilEntity> list = new ArrayList<PerfilEntity>();
		
		if (perfil.getNome() == null || perfil.getNome().equals("")) {
			list = perfilDao.findAll();
		} else {
			list = perfilDao.listar(perfil.getNome());
		}
		
		list.forEach(c->{
			List<PerfilMenuEntity> perfilMenuList = perfilMenuService.listarPorPerfil(c.getId(), GeralConst.MELDICA);
			c.setPermissoes(MenuTreeDto.listarPerfilPermissoes(perfilMenuList));
		});
		
		return list;
	}	

	@Override
	public PerfilEntity salvar(PerfilEntity perfil) {
		// Guadar as permissões que vieram do frontend
		List<String> permissoes = perfil.getPermissoes();
		
		perfil.setDataAlteracao(new Date());
		perfil = perfilDao.saveAndFlush(perfil);
		
//		perfil.setPermissoes(permissoes);
//		perfilMenuService.salvar(perfil);
		perfilMenuService.salvar(perfil, permissoes, GeralConst.MELDICA);

		return perfil;			
	}


}
