package br.com.meldica.service.acesso;

import java.util.List;

import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.entity.acesso.PerfilEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;

public interface PerfilMenuService {
	/**
	 * Listar Perfil Menu	
	 * @return List<PerfilMenuEntity>
	 */
	List<PerfilMenuEntity> listar();

	PerfilMenuEntity salvar(PerfilMenuEntity perfilMenu);
	
	void salvar(List<PerfilMenuEntity> perfilMenuList);
	
//	void salvar(PerfilEntity perfil);

	List<PerfilMenuEntity> listarPorPerfil(Integer idPerfil);

	List<String> listarPermissoesPorIdPerfil(Integer idPerfil);

	List<String> listarPermissoesSecurity(FiltroUsuarioMenuDto perfilMenu);

	List<PerfilMenuEntity> listarPorPerfil(Integer idPerfil, Integer idModulo);

	void salvar(PerfilEntity perfil, List<String> permissoes, int idModulo);
}
