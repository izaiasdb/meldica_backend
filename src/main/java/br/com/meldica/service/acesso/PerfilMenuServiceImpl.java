package br.com.meldica.service.acesso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.acesso.MenuDao;
import br.com.meldica.model.dao.acesso.PerfilMenuDao;
import br.com.meldica.model.dao.acesso.PerfilMenuMapperDao;
import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.dto.acesso.MenuTreeDto;
import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.entity.acesso.PerfilEntity;
import br.com.meldica.model.entity.acesso.PerfilMenuEntity;

@Service
public class PerfilMenuServiceImpl implements PerfilMenuService {

	private @Autowired PerfilMenuDao perfilMenuDao;
	private @Autowired MenuDao menuDao;
	private @Autowired PerfilMenuMapperDao perfilMenuMapperDao;
	
	@Value("${info.app.idmodulo}")
	private Integer idModulo;	

	@Override
	public List<PerfilMenuEntity> listar() {
		return perfilMenuDao.findAll();
	}
	
	
	@Override
	public PerfilMenuEntity salvar(PerfilMenuEntity perfilMenu) {
		return perfilMenuDao.saveAndFlush(perfilMenu);
	}

	@Override
	public void salvar(List<PerfilMenuEntity> perfilMenuList) {
		perfilMenuList.forEach(c-> {
			perfilMenuDao.saveAndFlush(c);			
		});
	}

	@Override
	public List<PerfilMenuEntity> listarPorPerfil(Integer idPerfil) {
		Assert.isTrue(idPerfil != null, "O perfil é obrigatório.");
		return perfilMenuDao.listarPorPerfil(idPerfil);
	}
	
	@Override
	public List<PerfilMenuEntity> listarPorPerfil(Integer idPerfil, Integer idModulo) {
		Assert.isTrue(idPerfil != null, "O perfil é obrigatório.");
		return perfilMenuDao.listarPorPerfilModulo(idPerfil, idModulo);
	}	
	
	@Override
	public List<String> listarPermissoesSecurity(FiltroUsuarioMenuDto perfilMenu) {
		Assert.isTrue(perfilMenu != null && perfilMenu.getIdPerfil() != null, "O perfil é obrigatório.");
		return perfilMenuMapperDao.litarPermissoesPerfil(perfilMenu);
	}
	
	@Override
	public List<String> listarPermissoesPorIdPerfil(Integer idPerfil) {
		Assert.isTrue(idPerfil != null, "O perfil é obrigatório.");
		List<PerfilMenuEntity> menus = perfilMenuDao.listarPorPerfil(idPerfil);
		
		if (menus.size() > 0)
			return MenuTreeDto.listarPerfilPermissoes(menus);
		else
			return new ArrayList<String>();		
	}
	

	@Override
	public void salvar(PerfilEntity perfil, List<String> permissoes, int idModulo) {
		List<PerfilMenuEntity> perfilMenuOldList = listarPorPerfil(perfil.getId(), idModulo);
		List<PerfilMenuEntity> perfilMenuAddList = new ArrayList<PerfilMenuEntity>();
		List<MenuEntity> menus = menuDao.listarPorModulo(idModulo);
		
		//if (perfil.getPermissoes() != null && perfil.getPermissoes().size() > 0) {
		if (permissoes != null && permissoes.size() > 0) {
			//for (String permissao : perfil.getPermissoes()) {
			
			Collections.sort(permissoes);
			
			for (String permissao : permissoes) {
				String[] permissaoArray = permissao.split("-");
				
				if (permissaoArray[0] != null) {
					PerfilMenuEntity perfilMenu = null;
					// Verifica se o menu, já está inserido no array menus, para não ter que ir pegar no banco!					
					MenuEntity menu = menus.stream().filter(c-> c.getId().equals(Integer.parseInt(permissaoArray[0]))).findFirst().orElse(null);
					
					if (menu != null) {
						if (perfilMenuAddList.size() > 0) {
							perfilMenu = perfilMenuAddList.stream()
									.filter(c-> c.getMenu().getId().equals(Integer.parseInt(permissaoArray[0]))).findFirst().orElse(null);
						}
						
						if (perfilMenu == null) {
							perfilMenu = new PerfilMenuEntity();
							perfilMenu.setIdPerfil(perfil.getId());
							perfilMenu.setIdUsuarioInclusao(perfil.getIdUsuarioInclusao());
							perfilMenu.setIdUsuarioAlteracao(perfil.getIdUsuarioAlteracao());
							perfilMenu.setMenu(menu);
							perfilMenu.setSomaPermissao(0);
							perfilMenu.setAcessa(true);
							perfilMenuAddList.add(perfilMenu);
							
							// Gravar o menu pai também
							if (menu.getMenu() != null) {
								PerfilMenuEntity menuPaiExist = perfilMenuAddList.stream().filter(c-> c.getMenu().getId().equals(menu.getMenu().getId())).findFirst().orElse(null);
								
								if (menuPaiExist == null) {
									PerfilMenuEntity perfilMenuPai = new PerfilMenuEntity();
									perfilMenuPai.setIdPerfil(perfil.getId());
									perfilMenuPai.setIdUsuarioInclusao(perfil.getIdUsuarioInclusao());
									perfilMenuPai.setIdUsuarioAlteracao(perfil.getIdUsuarioAlteracao());
									perfilMenuPai.setMenu(menu.getMenu());
									perfilMenuPai.setSomaPermissao(0);
									perfilMenuPai.setAcessa(true);
									perfilMenuAddList.add(perfilMenuPai);
								}
							}						
						}
						
						if (permissaoArray.length > 1) {
							perfilMenu.setSomaPermissao(perfilMenu.getSomaPermissao() + Integer.parseInt(permissaoArray[1]));
						}
					}
				}
			}			
		}
		
		// Verifica se algum item de menu foi retirado ao salvar. 
		perfilMenuOldList.forEach(c-> {
			PerfilMenuEntity perfilMenuDel = perfilMenuAddList.stream()
					.filter(old-> old.getMenu().getId().equals(c.getMenu().getId())).findFirst().orElse(null);
			
			// Ativar e desativar depois???
			if (perfilMenuDel == null) {
				perfilMenuDao.delete(c);
			}
		});
		
		perfilMenuAddList.forEach(c-> {
			PerfilMenuEntity perfilMenuOld = perfilMenuOldList.stream()
					.filter(old-> old.getMenu().getId().equals(c.getMenu().getId())).findFirst().orElse(null);		
			
			// Se foi inclído um novo item de menu ou a soma das permissões foram alteradas, salvar!
			if (perfilMenuOld == null) {
				if (c.getId() != null){
					c.setDataAlteracao(new Date());
				}
				
				perfilMenuDao.saveAndFlush(c);
			}
			else if (perfilMenuOld != null && perfilMenuOld.getSomaPermissao() != c.getSomaPermissao()) {
				perfilMenuOld.setSomaPermissao(c.getSomaPermissao());
				perfilMenuOld.setIdUsuarioAlteracao(perfil.getIdUsuarioAlteracao());
				perfilMenuOld.setDataAlteracao(new Date());
				
				perfilMenuDao.saveAndFlush(perfilMenuOld);			
			}
		});		
	}


}
