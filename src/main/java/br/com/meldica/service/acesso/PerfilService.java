package br.com.meldica.service.acesso;

import java.util.List;
import java.util.Map;

import br.com.meldica.model.entity.acesso.PerfilEntity;

public interface PerfilService {
	
	Map<String, Object> init();
	
	/**
	 * Listar Perfils
	 * @return List<PerfilEntity>
	 */
	List<PerfilEntity> listar();

	PerfilEntity salvar(PerfilEntity sistema);

	List<PerfilEntity> listar(PerfilEntity sistemaDto);

	
}
