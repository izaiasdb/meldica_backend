package br.com.meldica.service.acesso;

import java.util.List;

import br.com.meldica.model.entity.acesso.ModuloEntity;

public interface ModuloService {
	/**
	 * Listar Modulos
	 * @return List<ModuloDto>
	 */
	List<ModuloEntity> listar();

	ModuloEntity salvar(ModuloEntity modulo);

	List<ModuloEntity> listar(ModuloEntity modulo);
}
