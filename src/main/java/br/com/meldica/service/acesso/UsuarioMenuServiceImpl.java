package br.com.meldica.service.acesso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.acesso.MenuDao;
import br.com.meldica.model.dao.acesso.UsuarioMenuDao;
import br.com.meldica.model.dao.acesso.UsuarioMenuMapperDao;
import br.com.meldica.model.dto.acesso.FiltroUsuarioMenuDto;
import br.com.meldica.model.entity.acesso.MenuEntity;
import br.com.meldica.model.entity.acesso.UsuarioEntity;
import br.com.meldica.model.entity.acesso.UsuarioMenuEntity;

@Service
public class UsuarioMenuServiceImpl implements UsuarioMenuService {

	@Autowired
	UsuarioMenuDao usuarioMenuDao;
	@Autowired
	MenuDao menuDao;
	@Autowired
	UsuarioMenuMapperDao usuarioMenuMapperDao;
	
	@Value("${info.app.idmodulo}")
	private Integer idModulo;	

	@Override
	public List<UsuarioMenuEntity> listar() {
		return usuarioMenuDao.findAll();
	}
	
	
	@Override
	public UsuarioMenuEntity salvar(UsuarioMenuEntity usuarioMenu) {
		return usuarioMenuDao.saveAndFlush(usuarioMenu);
	}

	@Override
	public void salvar(List<UsuarioMenuEntity> usuarioMenuList) {
		usuarioMenuList.forEach(c-> {
			usuarioMenuDao.saveAndFlush(c);			
		});
	}

	@Override
	public List<UsuarioMenuEntity> listarPorUsuario(Integer idUsuario) {
		Assert.isTrue(idUsuario != null, "O usuário é obrigatório.");
		return usuarioMenuDao.listarPorUsuario(idUsuario);
	}
	
	@Override
	public List<String> listarPermissoesSecurity(FiltroUsuarioMenuDto usuarioMenu) {
		Assert.isTrue(usuarioMenu != null && usuarioMenu.getIdUsuario() != null, "O usuário é obrigatório.");
		return usuarioMenuMapperDao.litarPermissoesUsuario(usuarioMenu);
	}

	@Override
	public void salvar(UsuarioEntity usuario) {
		List<UsuarioMenuEntity> usuarioMenuOldList = listarPorUsuario(usuario.getId());
		List<UsuarioMenuEntity> usuarioMenuAddList = new ArrayList<UsuarioMenuEntity>();
		List<MenuEntity> menus = menuDao.listarPorModulo(idModulo);
		
		if (usuario.getPermissoes() != null && usuario.getPermissoes().size() > 0) {
			for (String permissao : usuario.getPermissoes()) {
				String[] permissaoArray = permissao.split("-");
				
				if (permissaoArray[0] != null) {
					UsuarioMenuEntity usuarioMenu = null;
					// Verifica se o menu, já está inserido no array menus, para não ter que ir pegar no banco!
					MenuEntity menu = menus.stream().filter(c-> c.getId().equals(Integer.parseInt(permissaoArray[0]))).findFirst().orElse(null);
					
					// Verifica se o menu, já está inserido no usuarioMenuNewList					
					if (usuarioMenuAddList.size() > 0)
						usuarioMenu = usuarioMenuAddList.stream()
							.filter(c-> c.getMenu().getId().equals(Integer.parseInt(permissaoArray[0]))).findFirst().orElse(null);
					
					if (usuarioMenu == null) {
						usuarioMenu = new UsuarioMenuEntity();
						usuarioMenu.setIdUsuario(usuario.getId());
						usuarioMenu.setIdUsuarioInclusao(usuario.getIdUsuarioInclusao());
						usuarioMenu.setIdUsuarioAlteracao(usuario.getIdUsuarioAlteracao());
						usuarioMenu.setMenu(menu);
						usuarioMenu.setSomaPermissao(0);
						usuarioMenu.setAcessa(true);
						usuarioMenuAddList.add(usuarioMenu);
						
						// Gravar o menu pai também
						if (menu.getMenu() != null) {
							UsuarioMenuEntity menuPaiExist = usuarioMenuAddList.stream().filter(c-> c.getMenu().getId().equals(menu.getMenu().getId())).findFirst().orElse(null);
							
							if (menuPaiExist == null) {
								UsuarioMenuEntity usuarioMenuPai = new UsuarioMenuEntity();
								usuarioMenuPai.setIdUsuario(usuario.getId());
								usuarioMenuPai.setIdUsuarioInclusao(usuario.getIdUsuarioInclusao());
								usuarioMenuPai.setIdUsuarioAlteracao(usuario.getIdUsuarioAlteracao());
								usuarioMenuPai.setMenu(menu.getMenu());
								usuarioMenuPai.setSomaPermissao(0);
								usuarioMenuPai.setAcessa(true);
								usuarioMenuAddList.add(usuarioMenuPai);
							}
						}
					}
					
					if (permissaoArray.length > 1) {
						usuarioMenu.setSomaPermissao(usuarioMenu.getSomaPermissao() + Integer.parseInt(permissaoArray[1]));
					}
				}
			}			
		}
		
		// Verifica se algum item de menu foi retirado ao salvar. 
		usuarioMenuOldList.forEach(c-> {
			UsuarioMenuEntity usuarioMenuDel = usuarioMenuAddList.stream()
					.filter(old-> old.getMenu().getId().equals(c.getMenu().getId())).findFirst().orElse(null);
			
			// Ativar e desativar depois???
			if (usuarioMenuDel == null) {
				usuarioMenuDao.delete(c);
			}
		});
		
		usuarioMenuAddList.forEach(c-> {
			UsuarioMenuEntity usuarioMenuOld = usuarioMenuOldList.stream()
					.filter(old-> old.getMenu().getId().equals(c.getMenu().getId())).findFirst().orElse(null);		
			
			// Se foi inclído um novo item de menu ou a soma das permissões foram alteradas, salvar!
			if (usuarioMenuOld == null) {
				if (c.getId() != null){
					c.setDataAlteracao(new Date());
				}
				
				usuarioMenuDao.saveAndFlush(c);
			}
			else if (usuarioMenuOld != null && usuarioMenuOld.getSomaPermissao() != c.getSomaPermissao()) {
				usuarioMenuOld.setSomaPermissao(c.getSomaPermissao());
				usuarioMenuOld.setIdUsuarioAlteracao(usuario.getIdUsuarioAlteracao());
				usuarioMenuOld.setDataAlteracao(new Date());
				usuarioMenuDao.saveAndFlush(usuarioMenuOld);			
			}
		});
	}


}
