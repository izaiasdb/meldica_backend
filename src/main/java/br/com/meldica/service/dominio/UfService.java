package br.com.meldica.service.dominio;

import java.util.List;

import br.com.meldica.model.entity.dominio.UfEntity;

public interface UfService {
	List<UfEntity> listarTodos();	
}
