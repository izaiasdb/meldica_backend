package br.com.meldica.service.dominio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.dominio.UnidadeMedidaDao;
import br.com.meldica.model.entity.dominio.UnidadeMedidaEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class UnidadeMedidaServiceImpl implements UnidadeMedidaService {

	@Autowired
	UnidadeMedidaDao dao;

	@Override
	public List<UnidadeMedidaEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<UnidadeMedidaEntity> listar(UnidadeMedidaEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar();
		else
			return dao.listar(entity.getNome());
	}		
	
	@Override
	public UnidadeMedidaEntity salvar(UnidadeMedidaEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}

}
