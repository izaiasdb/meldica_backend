package br.com.meldica.service.dominio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.dominio.CargoDao;
import br.com.meldica.model.entity.dominio.CargoEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class CargoServiceImpl implements CargoService {

	private @Autowired CargoDao dao;

	@Override
	public List<CargoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<CargoEntity> listar(CargoEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar();
		else
			return dao.listar(entity.getNome());
	}		
	
	@Override
	public CargoEntity salvar(CargoEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}

}
