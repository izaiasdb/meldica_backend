package br.com.meldica.service.dominio;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.dominio.UnidadeMedidaEntity;

public interface UnidadeMedidaService {
	List<UnidadeMedidaEntity> listar();
	UnidadeMedidaEntity salvar(@RequestBody UnidadeMedidaEntity entity);
	List<UnidadeMedidaEntity> listar(UnidadeMedidaEntity entity);
}
