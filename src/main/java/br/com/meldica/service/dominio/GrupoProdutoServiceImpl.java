package br.com.meldica.service.dominio;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.dominio.GrupoProdutoDao;
import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class GrupoProdutoServiceImpl implements GrupoProdutoService {

	@Autowired
	GrupoProdutoDao dao;

	@Override
	public List<GrupoProdutoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<GrupoProdutoEntity> listar(GrupoProdutoEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar();
		else
			return dao.listar(entity.getNome());
	}		
	
	@Override
	public GrupoProdutoEntity salvar(GrupoProdutoEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}

}
