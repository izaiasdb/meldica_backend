package br.com.meldica.service.dominio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dao.dominio.UfDao;
import br.com.meldica.model.entity.dominio.UfEntity;

@Service
public class UfServiceImpl implements UfService {

	private @Autowired UfDao dao;

	@Override
	public List<UfEntity> listarTodos() {
		return dao.findAll();
	}

}
