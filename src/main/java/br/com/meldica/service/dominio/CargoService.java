package br.com.meldica.service.dominio;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.dominio.CargoEntity;

public interface CargoService {
	List<CargoEntity> listar();
	CargoEntity salvar(@RequestBody CargoEntity entity);
	List<CargoEntity> listar(CargoEntity entity);
}
