package br.com.meldica.service.dominio;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.dominio.GrupoProdutoEntity;

public interface GrupoProdutoService {
	List<GrupoProdutoEntity> listar();
	GrupoProdutoEntity salvar(@RequestBody GrupoProdutoEntity entity);
	List<GrupoProdutoEntity> listar(GrupoProdutoEntity entity);
}
