package br.com.meldica.service.dominio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dao.dominio.MunicipioDao;
import br.com.meldica.model.entity.dominio.MunicipioEntity;

@Service
public class MunicipioServiceImpl implements MunicipioService {

	private @Autowired MunicipioDao municipioDao;

	@Override
	public List<MunicipioEntity> listarTodos() {
		return municipioDao.findAll();
	}

	@Override
	public List<MunicipioEntity> nome(String nome) {
		nome = nome != null && nome.length() > 0 ? "%"+nome.toUpperCase()+"%" : "";
		return municipioDao.findByNomeLike(nome);
	}
}
