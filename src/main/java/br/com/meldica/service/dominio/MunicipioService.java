package br.com.meldica.service.dominio;

import java.util.List;

import br.com.meldica.model.entity.dominio.MunicipioEntity;

public interface MunicipioService {
	List<MunicipioEntity> listarTodos();
	List<MunicipioEntity> nome(String nome);	
}
