package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.ProdutoDao;
import br.com.meldica.model.entity.publico.ProdutoEntity;
import br.com.meldica.model.entity.publico.ProdutoItemEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired ProdutoDao dao;
	@Autowired ProdutoItemService produtoItemService;

	@Override
	public List<ProdutoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<ProdutoEntity> listar(ProdutoEntity produto) {
		Assert.isTrue(produto != null , "Preencha os dados.");

		List<ProdutoEntity> list = new ArrayList<ProdutoEntity>();
		
		list = dao.listar(produto.getTipo(), produto.getNome(), produto.isAtivo());
		
		for (ProdutoEntity item : list) {
			item.setProdutoItemsList(produtoItemService.listarPorProduto(item.getId()));
		}
		
		return list;
	}	

	@Override
	public ProdutoEntity salvar(ProdutoEntity produto) {
		if (produto.getId() == null){
			produto.setDataInclusao(new Date());
		}		
		
		produto.setDataAtualizacao(new Date());
		
		List<ProdutoItemEntity> produtoItemsList = produto.getProdutoItemsList();
		
		produto = dao.saveAndFlush(produto);
		
		produto.setProdutoItemsList(produtoItemsList);
		produtoItemService.salvar(produto);
		
		return produto;
	}


}
