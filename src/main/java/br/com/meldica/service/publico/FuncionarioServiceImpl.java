package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.FuncionarioDao;
import br.com.meldica.model.entity.publico.FuncionarioEnderecoEntity;
import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.model.entity.publico.FuncionarioTelefoneEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired FuncionarioDao dao;
	@Autowired FuncionarioEnderecoService funcionarioEnderecoService;
	@Autowired FuncionarioTelefoneService funcionarioTelefoneService;

	@Override
	public List<FuncionarioEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<FuncionarioEntity> listar(FuncionarioEntity funcionario) {
		Assert.isTrue(funcionario != null , "Preencha os dados.");
		
		List<FuncionarioEntity> list = new ArrayList<FuncionarioEntity>();
		
		if (StringUtils.isNullOrEmpty(funcionario.getNome())) {
			list = dao.listar();
		} else
		{
			list = dao.listar(funcionario.getNome());
		}
		
		for (FuncionarioEntity item : list) {
			item.setFuncionarioEnderecoList(funcionarioEnderecoService.listarPorFuncionario(item.getId()));
			item.setFuncionarioTelefoneList(funcionarioTelefoneService.listarPorFuncionario(item.getId()));
		}
		
		return list;
	}	

	@Override
	public FuncionarioEntity salvar(FuncionarioEntity funcionario) {
		if (funcionario.getId() == null){
			funcionario.setDataInclusao(new Date());
		}		
		
		funcionario.setDataAtualizacao(new Date());
		
		List<FuncionarioEnderecoEntity> funcionarioEnderecoList = funcionario.getFuncionarioEnderecoList();
		List<FuncionarioTelefoneEntity> funcionarioTelefoneList = funcionario.getFuncionarioTelefoneList();
		
		funcionario = dao.saveAndFlush(funcionario);
		
		funcionario.setFuncionarioEnderecoList(funcionarioEnderecoList);
		funcionario.setFuncionarioTelefoneList(funcionarioTelefoneList);;
		funcionarioEnderecoService.salvar(funcionario);
		funcionarioTelefoneService.salvar(funcionario);
		
		return funcionario;
	}


}
