package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.ProdutoItemDao;
import br.com.meldica.model.entity.publico.ProdutoEntity;
import br.com.meldica.model.entity.publico.ProdutoItemEntity;

@Service
public class ProdutoItemServiceImpl implements ProdutoItemService {

	private @Autowired ProdutoItemDao dao;

	@Override
	public List<ProdutoItemEntity> listarPorProduto(Integer idProdutoPai) {
		Assert.isTrue(idProdutoPai != null , "Preencha os dados.");
		return dao.listarPorProduto(idProdutoPai);
	}	

	@Override
	public void salvar(ProdutoEntity produto) {
		List<ProdutoItemEntity> newList = produto.getProdutoItemsList();
		
		if(newList != null && !newList.isEmpty()) {
			List<ProdutoItemEntity> oldList = listarPorProduto(produto.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ProdutoItemEntity produtoItem : newList) {
				produtoItem.setIdProdutoPai(produto.getId());
				
				if(Objects.isNull(produtoItem.getId())) {
					produtoItem.setDataInclusao(now);
				}
				
				if(!Objects.isNull(produto.getIdUsuarioInclusao())) {
					produtoItem.setIdUsuarioInclusao(produto.getIdUsuarioInclusao());
					produtoItem.setIdUsuarioAlteracao(produto.getIdUsuarioInclusao());
				}
				
				produtoItem.setDataAtualizacao(now);
				dao.save(produtoItem);
			}
		}
	}


}
