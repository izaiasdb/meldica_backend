package br.com.meldica.service.publico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meldica.model.dto.publico.DashboardDto;
import br.com.meldica.model.entity.publico.EmpresaEntity;
import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;
import br.com.meldica.service.relatorio.RelatorioOrdemServicoService;
import br.com.meldica.utils.DateUtils;
import br.com.meldica.utils.GeralConst;
import br.com.meldica.utils.StringUtils;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired EmpresaService empresaService;
	@Autowired FuncionarioService funcionarioService;
	@Autowired OrdemServicoService ordemServicoService;
	@Autowired OrdemServicoProdutoService ordemServicoProdutoService;
	@Autowired OrdemServicoKitService ordemServicoKitService;
	@Autowired OrdemServicoKitProdutoService ordemServicoKitProdutoService;
	@Autowired RelatorioOrdemServicoService relatorioOrdemServicoService;
	
	@Override
	public List<DashboardDto> listar(DashboardDto filtro) {
		List<DashboardDto> list = new ArrayList<>();
		OrdemServicoEntity osFiltro = new OrdemServicoEntity();
		Date dataIni = new Date();
		
		try {
			dataIni = DateUtils.stringToDate(filtro.getData(), false);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		List<Date> periodoVenda = new ArrayList<>();
		
		periodoVenda.add(dataIni);
		periodoVenda.add(DateUtils.getUltimoDiaMes(dataIni));
		osFiltro.setPeriodoVenda(periodoVenda);
		osFiltro.setCancelado(false);
		String descricao = " - " + DateUtils.dateToString(periodoVenda.get(0)) + " à " + DateUtils.dateToString(periodoVenda.get(1)); 
		
		List<OrdemServicoEntity> osList = relatorioOrdemServicoService.listarDashboard(osFiltro);
		List<EmpresaEntity> empresaList = empresaService.listar();
		
		// Por Empresa
		if (filtro.getIdTipoDashboard().equals(1) && !StringUtils.isNullOrEmpty(filtro.getData())) {
			for (EmpresaEntity empresa : empresaList) {
				BigDecimal valor = BigDecimal.ZERO;
				BigDecimal caixa = BigDecimal.ZERO;
				Integer venda = 0;
				
				for (OrdemServicoEntity item : osList) {
					List<OrdemServicoProdutoEntity> tempList = item.getProdutoItemsList().stream()
							.filter(c-> !c.isBonificacao() && 
									c.getIdEmpresaProduto().equals(empresa.getId())).collect(Collectors.toList());
					
					if (tempList != null && tempList.size() > 0) {
						venda++;
						
						for (OrdemServicoProdutoEntity temp : tempList) {
							valor = valor.add(temp.getTotal());
							caixa = caixa.add(temp.getQuantidadeCaixa());
						}
					}
				}
				
				DashboardDto dto = new DashboardDto(empresa.getId().longValue(), empresa.getNome(), valor, caixa, venda);
				dto.setDescricao(descricao);
				dto.setIdEmpresa(empresa.getId());
				dto.setEmpresa(empresa.getNome());
				list.add(dto);
			}
		// Por Funcionário			
		} else if (filtro.getIdTipoDashboard().equals(2) && !StringUtils.isNullOrEmpty(filtro.getData())) {
			List<FuncionarioEntity> funcioarioList = funcionarioService.listar();
			List<FuncionarioEntity> vendedorList = funcioarioList.stream()
					.filter(c-> c.getCargo().getId().equals(GeralConst.CARGO_VENDEDOR)).collect(Collectors.toList());
			
			for (FuncionarioEntity vendedor : vendedorList) {			
				for (OrdemServicoEntity item : osList) {
					if (item.getFuncionario().getId().equals(vendedor.getId())) {
						for (EmpresaEntity empresa : empresaList) {
							
							List<OrdemServicoProdutoEntity> tempList = item.getProdutoItemsList().stream()
									.filter(c-> !c.isBonificacao() && 
											c.getIdEmpresaProduto().equals(empresa.getId())).collect(Collectors.toList());
							
							if (tempList != null && tempList.size() > 0) {
								DashboardDto dto = list.stream().filter(c-> 
									c.getIdEmpresa().equals(empresa.getId()) && 
									c.getIdFuncionario().equals(vendedor.getId())
								).findFirst().orElse(null);
								
								if (dto == null) {
									dto = new DashboardDto();
									dto.setDescricao(descricao);
									dto.setIdFuncionario(vendedor.getId());
									dto.setFuncionario(vendedor.getNome());
									dto.setIdEmpresa(empresa.getId());
									dto.setEmpresa(empresa.getNome());									
									list.add(dto);
								}
								
								dto.setVenda(dto.getVenda() + 1);
								
								for (OrdemServicoProdutoEntity temp : tempList) {
									dto.setValor(dto.getValor().add(temp.getTotal()));
									dto.setCaixa(dto.getCaixa().add(temp.getQuantidadeCaixa()));
								}
							}	
						}
					}
				}
			}				
		}
		
		return list;
	}	

}
