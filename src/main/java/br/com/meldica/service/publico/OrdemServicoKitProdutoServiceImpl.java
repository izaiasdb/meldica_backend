package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.OrdemServicoKitProdutoDao;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;

@Service
public class OrdemServicoKitProdutoServiceImpl implements OrdemServicoKitProdutoService {

	private @Autowired OrdemServicoKitProdutoDao dao;

	@Override
	public List<OrdemServicoKitProdutoEntity> listarPorOrdemServicoKit(Integer idOrdemServicoKit) { 
		return dao.listarPorOrdemServicoKit(idOrdemServicoKit);
	}

	@Override
	@Transactional
	public void salvar(OrdemServicoKitEntity ordemServicoKit) {
		List<OrdemServicoKitProdutoEntity> novoList = ordemServicoKit.getProdutoItemsList();
		
		if(novoList != null) { //Pode estar vázio por ter limpado
			List<OrdemServicoKitProdutoEntity> oldList = listarPorOrdemServicoKit(ordemServicoKit.getId());
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(OrdemServicoKitProdutoEntity novo : novoList) {
				novo.setIdOrdemServicoKit(ordemServicoKit.getId());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(ordemServicoKit.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(ordemServicoKit.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(ordemServicoKit.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				dao.save(novo);
			}
		}
	}
	
}
