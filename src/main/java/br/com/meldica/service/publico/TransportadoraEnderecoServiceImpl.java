package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.TransportadoraEnderecoDao;
import br.com.meldica.model.entity.publico.TransportadoraEnderecoEntity;
import br.com.meldica.model.entity.publico.TransportadoraEntity;

@Service
public class TransportadoraEnderecoServiceImpl implements TransportadoraEnderecoService {

	@Autowired
	TransportadoraEnderecoDao dao;

	@Override
	public List<TransportadoraEnderecoEntity> listarPorTransportadora(Integer idTransportadora) { 
		return dao.listarPorTransportadora(idTransportadora);
	}
	
	@Override
	public List<TransportadoraEnderecoEntity> listar() { 
		return dao.findAll();
	}

	@Override
	@Transactional
	public void salvar(TransportadoraEntity cliente) {
		List<TransportadoraEnderecoEntity> newList = cliente.getTransportadoraEnderecoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<TransportadoraEnderecoEntity> oldList = listarPorTransportadora(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(TransportadoraEnderecoEntity endereco : newList) {
				endereco.setIdTransportadora(cliente.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}
	
}
