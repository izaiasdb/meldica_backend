package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FuncionarioEntity;

public interface FuncionarioService {
	List<FuncionarioEntity> listar();

	FuncionarioEntity salvar(FuncionarioEntity funcionario);

	List<FuncionarioEntity> listar(FuncionarioEntity funcionario);
}
