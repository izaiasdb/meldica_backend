package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.OrdemServicoKitDao;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;

@Service
public class OrdemServicoKitServiceImpl implements OrdemServicoKitService {

	private @Autowired OrdemServicoKitDao dao;
	private @Autowired OrdemServicoKitProdutoService ordemServicoKitProdutoService;

	@Override
	public List<OrdemServicoKitEntity> listarPorOrdemServico(Integer idOrdemServico) { 
		return dao.listarPorOrdemServico(idOrdemServico);
	}

	@Override
	@Transactional
	public void salvar(OrdemServicoEntity ordemServico) {
		List<OrdemServicoKitEntity> novoList = ordemServico.getKitList();
		
		//if(novoList != null && !novoList.isEmpty()) {
		if(novoList != null) { //Pode estar vázio por ter limpado
			List<OrdemServicoKitEntity> oldList = listarPorOrdemServico(ordemServico.getId());
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(OrdemServicoKitEntity novo : novoList) {
				novo.setIdOrdemServico(ordemServico.getId());
				final int codigo = novo.getCodigo();
				
				List<OrdemServicoKitProdutoEntity> itemList = new ArrayList<OrdemServicoKitProdutoEntity>();
				itemList = ordemServico.getKitProdutoList().stream().filter(c-> c.getCodigoPai().equals(codigo)).collect(Collectors.toList());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(ordemServico.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(ordemServico.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(ordemServico.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				novo = dao.save(novo);
				
				novo.setProdutoItemsList(itemList);
				ordemServicoKitProdutoService.salvar(novo);
			}
		}
	}
	
}
