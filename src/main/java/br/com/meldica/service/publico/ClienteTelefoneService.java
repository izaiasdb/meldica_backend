package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteTelefoneEntity;

public interface ClienteTelefoneService {
	/**
	 * Listaro telefone pelo sequencial do cliente
	 * @param idCliente
	 * @return listarPorCliente
	 */
	List<ClienteTelefoneEntity> listarPorCliente(Integer idCliente);
	
	void salvar(ClienteEntity cliente);
}
