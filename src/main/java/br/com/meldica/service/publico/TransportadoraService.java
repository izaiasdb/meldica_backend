package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.TransportadoraEntity;

public interface TransportadoraService {
	TransportadoraEntity obter(Integer id);
	
	List<TransportadoraEntity> listar();

	TransportadoraEntity salvar(TransportadoraEntity cliente);

	List<TransportadoraEntity> listar(TransportadoraEntity cliente);
}
