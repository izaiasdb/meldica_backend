package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.dto.publico.DashboardDto;

public interface DashboardService {
	List<DashboardDto> listar(DashboardDto filtro);
}
