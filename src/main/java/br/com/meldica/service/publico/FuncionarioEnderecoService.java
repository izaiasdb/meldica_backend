package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FuncionarioEnderecoEntity;
import br.com.meldica.model.entity.publico.FuncionarioEntity;

public interface FuncionarioEnderecoService {
	/**
	 * Listaro endereço pelo sequencial do funcionario
	 * @param idFuncionario
	 * @return listarPorFuncionario
	 */
	List<FuncionarioEnderecoEntity> listarPorFuncionario(Integer idFuncionario);
	
	void salvar(FuncionarioEntity funcionario);
}
