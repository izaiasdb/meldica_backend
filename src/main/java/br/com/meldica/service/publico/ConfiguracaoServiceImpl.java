package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.ConfiguracaoDao;
import br.com.meldica.model.entity.publico.ConfiguracaoEntity;

@Service
public class ConfiguracaoServiceImpl implements ConfiguracaoService {

	private @Autowired ConfiguracaoDao dao;

	@Override
	public List<ConfiguracaoEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<ConfiguracaoEntity> listar(ConfiguracaoEntity configuracao) {
		Assert.isTrue(configuracao != null , "Preencha os dados.");
		
		List<ConfiguracaoEntity> list = new ArrayList<>(); 
				
//		if (StringUtils.isNullOrEmpty(configuracao.getNome())) {
			list = dao.findAll();
//		} else
//		{
//			list = dao.listar(configuracao.getNome());
//		}
		
		return list;
	}
	
	@Override
	public ConfiguracaoEntity salvar(ConfiguracaoEntity configuracao) {
		if (configuracao.getId() == null){
			configuracao.setDataInclusao(new Date());
		}		
		
		configuracao.setDataAtualizacao(new Date());
		
		return dao.saveAndFlush(configuracao);
	}
	
}
