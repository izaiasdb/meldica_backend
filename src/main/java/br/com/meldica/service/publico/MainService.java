package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.dto.publico.FichaDto;

public interface MainService {
	List<FichaDto> listarBuscaRapida(FichaDto filtro);
}
