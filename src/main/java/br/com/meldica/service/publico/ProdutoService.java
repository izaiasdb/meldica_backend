package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ProdutoEntity;

public interface ProdutoService {
	List<ProdutoEntity> listar();

	ProdutoEntity salvar(ProdutoEntity produto);

	List<ProdutoEntity> listar(ProdutoEntity produto);
}
