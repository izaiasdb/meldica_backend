package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.FornecedorDao;
import br.com.meldica.model.entity.publico.FornecedorEnderecoEntity;
import br.com.meldica.model.entity.publico.FornecedorEntity;
import br.com.meldica.model.entity.publico.FornecedorTelefoneEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class FornecedorServiceImpl implements FornecedorService {

	@Autowired FornecedorDao dao;
	@Autowired FornecedorEnderecoService clienteEnderecoService;
	@Autowired FornecedorTelefoneService clienteTelefoneService;

	@Override
	public List<FornecedorEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<FornecedorEntity> listar(FornecedorEntity cliente) {
		Assert.isTrue(cliente != null , "Preencha os dados.");
		
		List<FornecedorEntity> list = new ArrayList<FornecedorEntity>();
		
		if (StringUtils.isNullOrEmpty(cliente.getNome())) {
			list = dao.listar();
		} else
		{
			list = dao.listar(cliente.getNome());
		}
		
		for (FornecedorEntity item : list) {
			item.setFornecedorEnderecoList(clienteEnderecoService.listarPorFornecedor(item.getId()));
			item.setFornecedorTelefoneList(clienteTelefoneService.listarPorFornecedor(item.getId()));
		}
		
		return list;
	}	

	@Override
	public FornecedorEntity salvar(FornecedorEntity cliente) {
		if (cliente.getId() == null){
			cliente.setDataInclusao(new Date());
		}		
		
		cliente.setDataAtualizacao(new Date());
		
		List<FornecedorEnderecoEntity> clienteEnderecoList = cliente.getFornecedorEnderecoList();
		List<FornecedorTelefoneEntity> clienteTelefoneList = cliente.getFornecedorTelefoneList();
		
		cliente = dao.saveAndFlush(cliente);
		
		cliente.setFornecedorEnderecoList(clienteEnderecoList);
		cliente.setFornecedorTelefoneList(clienteTelefoneList);;
		clienteEnderecoService.salvar(cliente);
		clienteTelefoneService.salvar(cliente);
		
		return cliente;
	}


}
