package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.ClienteEnderecoDao;
import br.com.meldica.model.entity.publico.ClienteEnderecoEntity;
import br.com.meldica.model.entity.publico.ClienteEntity;

@Service
public class ClienteEnderecoServiceImpl implements ClienteEnderecoService {

	@Autowired
	ClienteEnderecoDao dao;

	@Override
	public List<ClienteEnderecoEntity> listarPorCliente(Integer idCliente) { 
		return dao.listarPorCliente(idCliente);
	}
	
	@Override
	public List<ClienteEnderecoEntity> listar() { 
		return dao.findAll();
	}

	@Override
	@Transactional
	public void salvar(ClienteEntity cliente) {
		List<ClienteEnderecoEntity> newList = cliente.getClienteEnderecoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<ClienteEnderecoEntity> oldList = listarPorCliente(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ClienteEnderecoEntity endereco : newList) {
				endereco.setIdCliente(cliente.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}
	
}
