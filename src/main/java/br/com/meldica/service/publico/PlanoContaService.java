package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.PlanoContaEntity;

public interface PlanoContaService {
	/**
	 * Listar Menus
	 * @return List<MenuDto>
	 */
	List<PlanoContaEntity> listar();

	PlanoContaEntity salvar(PlanoContaEntity modulo);

	List<PlanoContaEntity> listar(PlanoContaEntity modulo);

	List<PlanoContaEntity> listarPorNivel(Integer nivel);

	List<PlanoContaEntity> listarPorNivel(Integer nivel, Integer idPlanoContaPai);

}
