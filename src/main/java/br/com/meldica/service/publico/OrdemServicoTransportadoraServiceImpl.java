package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.OrdemServicoTransportadoraDao;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoTransportadoraEntity;

@Service
public class OrdemServicoTransportadoraServiceImpl implements OrdemServicoTransportadoraService {

	private @Autowired OrdemServicoTransportadoraDao dao;

	@Override
	public List<OrdemServicoTransportadoraEntity> listarPorOrdemServico(Integer idOrdemServico) { 
		return dao.listarPorOrdemServico(idOrdemServico);
	}

	@Override
	@Transactional
	public void salvar(OrdemServicoEntity ordemServico) {
		List<OrdemServicoTransportadoraEntity> novoList = ordemServico.getTransportadoraItemsList();
		
		if(novoList != null) { //Pode estar vázio por ter limpado
//		if(novoList != null && !novoList.isEmpty()) {
			List<OrdemServicoTransportadoraEntity> oldList = listarPorOrdemServico(ordemServico.getId());
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(OrdemServicoTransportadoraEntity novo : novoList) {
				novo.setIdOrdemServico(ordemServico.getId());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(ordemServico.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(ordemServico.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(ordemServico.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				dao.save(novo);
			}
		}
	}
	
}
