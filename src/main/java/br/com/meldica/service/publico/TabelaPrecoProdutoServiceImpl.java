package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.TabelaPrecoProdutoDao;
import br.com.meldica.model.entity.publico.TabelaPrecoEntity;
import br.com.meldica.model.entity.publico.TabelaPrecoProdutoEntity;

@Service
public class TabelaPrecoProdutoServiceImpl implements TabelaPrecoProdutoService {

	private @Autowired TabelaPrecoProdutoDao dao;

	@Override
	public List<TabelaPrecoProdutoEntity> listarPorTabelaPreco(Integer idTabelaPreco) {
		Assert.isTrue(idTabelaPreco != null , "Preencha os dados.");
		return dao.listarPorTabelaPreco(idTabelaPreco);
	}	

	@Override
	public void salvar(TabelaPrecoEntity tabelaPreco) {
		List<TabelaPrecoProdutoEntity> newList = tabelaPreco.getProdutoItemsList();
		
		if(newList != null && !newList.isEmpty()) {
			List<TabelaPrecoProdutoEntity> oldList = listarPorTabelaPreco(tabelaPreco.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(TabelaPrecoProdutoEntity produtoItem : newList) {
				produtoItem.setIdTabelaPreco(tabelaPreco.getId());
				
				if(Objects.isNull(produtoItem.getId())) {
					produtoItem.setDataInclusao(now);
				}
				
				if(!Objects.isNull(tabelaPreco.getIdUsuarioInclusao())) {
					produtoItem.setIdUsuarioInclusao(tabelaPreco.getIdUsuarioInclusao());
					produtoItem.setIdUsuarioAlteracao(tabelaPreco.getIdUsuarioInclusao());
				}
				
				produtoItem.setDataAtualizacao(now);
				dao.save(produtoItem);
			}
		}
	}

	@Override
	public List<TabelaPrecoProdutoEntity> listar() {
		return dao.findAll();
	}


}
