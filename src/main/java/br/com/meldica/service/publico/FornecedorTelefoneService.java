package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FornecedorEntity;
import br.com.meldica.model.entity.publico.FornecedorTelefoneEntity;

public interface FornecedorTelefoneService {
	/**
	 * Listaro telefone pelo sequencial do fornecedor
	 * @param idFornecedor
	 * @return listarPorFornecedor
	 */
	List<FornecedorTelefoneEntity> listarPorFornecedor(Integer idFornecedor);
	
	void salvar(FornecedorEntity fornecedor);
}
