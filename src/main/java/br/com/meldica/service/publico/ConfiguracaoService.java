package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ConfiguracaoEntity;

public interface ConfiguracaoService {
	List<ConfiguracaoEntity> listar();

	ConfiguracaoEntity salvar(ConfiguracaoEntity configuracao);

	List<ConfiguracaoEntity> listar(ConfiguracaoEntity configuracao);

}
