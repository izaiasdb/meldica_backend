package br.com.meldica.service.publico;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.publico.FormaPagamentoEntity;
public interface FormaPagamentoService {
	List<FormaPagamentoEntity> listar();
	List<FormaPagamentoEntity> listarAtivos();
	FormaPagamentoEntity salvar(@RequestBody FormaPagamentoEntity entity);
	List<FormaPagamentoEntity> listar(FormaPagamentoEntity entity);
	void cancelar(FormaPagamentoEntity entity);
}
