package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.ClienteTabelaPrecoDao;
import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteTabelaPrecoEntity;

@Service
public class ClienteTabelaPrecoServiceImpl implements ClienteTabelaPrecoService {

	private @Autowired ClienteTabelaPrecoDao dao;

	@Override
	public List<ClienteTabelaPrecoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<ClienteTabelaPrecoEntity> listarPorCliente(Integer idCliente) { 
		return dao.listarPorCliente(idCliente);
	}
	
	@Override
	@Transactional
	public void salvar(ClienteEntity cliente) {
		List<ClienteTabelaPrecoEntity> newList = cliente.getClienteTabelaPrecoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<ClienteTabelaPrecoEntity> oldList = listarPorCliente(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ClienteTabelaPrecoEntity endereco : newList) {
				endereco.setIdCliente(cliente.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}


	
}
