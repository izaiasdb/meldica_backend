package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ProdutoEntity;
import br.com.meldica.model.entity.publico.ProdutoItemEntity;

public interface ProdutoItemService {
	void salvar(ProdutoEntity produto);

	List<ProdutoItemEntity> listarPorProduto(Integer idProdutoPai);
}
