package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.TabelaPrecoEntity;

public interface TabelaPrecoService {
	List<TabelaPrecoEntity> listar();

	TabelaPrecoEntity salvar(TabelaPrecoEntity produto);

	List<TabelaPrecoEntity> listar(TabelaPrecoEntity produto);
	
	List<TabelaPrecoEntity> listarAtivos();
}
