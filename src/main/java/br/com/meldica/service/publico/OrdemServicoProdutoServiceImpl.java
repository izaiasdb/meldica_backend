package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.OrdemServicoProdutoDao;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;

@Service
public class OrdemServicoProdutoServiceImpl implements OrdemServicoProdutoService {

	private @Autowired OrdemServicoProdutoDao dao;

	@Override
	public List<OrdemServicoProdutoEntity> listarPorOrdemServico(Integer idOrdemServico) { 
		return dao.listarPorOrdemServico(idOrdemServico);
	}

	@Override
	@Transactional
	public void salvar(OrdemServicoEntity ordemServico) {
		List<OrdemServicoProdutoEntity> novoList = ordemServico.getProdutoItemsList();
		List<OrdemServicoProdutoEntity> oldList = listarPorOrdemServico(ordemServico.getId());
		
		if(novoList != null && !novoList.isEmpty()) {
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(OrdemServicoProdutoEntity novo : novoList) {
				novo.setIdOrdemServico(ordemServico.getId());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(ordemServico.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(ordemServico.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(ordemServico.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				dao.save(novo);
			}
			// Novo está vázio e o antigo tem registro
		} else if((novoList == null || novoList.isEmpty()) && (oldList != null && !oldList.isEmpty())) {
			dao.deleteAll(oldList);
		}
	}
	
}
