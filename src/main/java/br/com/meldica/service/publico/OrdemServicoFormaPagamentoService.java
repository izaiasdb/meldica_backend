package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;

public interface OrdemServicoFormaPagamentoService {
	/**
	 * Listar por ordem de serviço
	 * @param idOrdemServico
	 * @return
	 */
	List<OrdemServicoFormaPagamentoEntity> listarPorOrdemServico(Integer idOrdemServico);
	
	void salvar(OrdemServicoEntity ordemServico);
}
