package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ProducaoEntity;

public interface ProducaoService {
	ProducaoEntity obter(Integer id);	
	
	List<ProducaoEntity> listar();

	void salvar(ProducaoEntity producao);

	List<ProducaoEntity> listar(ProducaoEntity producao);

//	void alterarStatus(ProducaoEntity producao);
	
//	List<ProducaoEntity> listarPorStatus(String status);
}
