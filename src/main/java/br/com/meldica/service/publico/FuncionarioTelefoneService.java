package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.model.entity.publico.FuncionarioTelefoneEntity;

public interface FuncionarioTelefoneService {
	/**
	 * Listaro telefone pelo sequencial do funcionario
	 * @param idFuncionario
	 * @return listarPorFuncionario
	 */
	List<FuncionarioTelefoneEntity> listarPorFuncionario(Integer idFuncionario);
	
	void salvar(FuncionarioEntity funcionario);
}
