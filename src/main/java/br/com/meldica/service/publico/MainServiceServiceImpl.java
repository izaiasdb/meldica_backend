package br.com.meldica.service.publico;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.MainMapperDao;
import br.com.meldica.model.dto.publico.FichaDto;

@Service
public class MainServiceServiceImpl implements MainService {

	@Autowired private MainMapperDao mapperDao;
	
	@Override
	public List<FichaDto> listarBuscaRapida(FichaDto filtro) {
		Assert.isTrue(!Objects.isNull(filtro), "Os parâmetros da busca rápida são obrigatório.");
		String nome = filtro.getNome();
		Assert.isTrue(nome != null && !nome.isEmpty(), "Nome da pessoa é obrigatório.");
		Integer id = null;
		
		try {
			id = new Integer(nome);
			filtro.setId(id);
			filtro.setNome(null);
		}catch (Exception e) {
		}
		
		return mapperDao.listarBuscaRapida(filtro);
	}


}
