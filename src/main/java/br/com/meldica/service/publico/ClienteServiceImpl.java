package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.ClienteDao;
import br.com.meldica.model.dao.publico.ClienteMapperDao;
import br.com.meldica.model.dto.publico.FichaDto;
import br.com.meldica.model.entity.publico.ClienteEnderecoEntity;
import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteRazaoEntity;
import br.com.meldica.model.entity.publico.ClienteTabelaPrecoEntity;
import br.com.meldica.model.entity.publico.ClienteTelefoneEntity;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired ClienteDao dao;
	@Autowired ClienteEnderecoService clienteEnderecoService;
	@Autowired ClienteTelefoneService clienteTelefoneService;
	@Autowired ClienteTabelaPrecoService clienteTabelaPrecoService;
	@Autowired ClienteRazaoService clienteRazaoService;
	@Autowired OrdemServicoService ordemServicoService;
	@Autowired PagarReceberService pagarReceberService;
	@Autowired ClienteMapperDao mapperDao;

	@Override
	public List<ClienteEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<ClienteEntity> listar(ClienteEntity filtro) {
		Assert.isTrue(filtro != null , "Preencha os dados.");
		
		/*
		List<ClienteEntity> list = new ArrayList<ClienteEntity>();
		
		if (StringUtils.isNullOrEmpty(cliente.getNome())) {
			list = dao.listar();
		} else
		{
			list = dao.listar(cliente.getNome());
		}
		
		for (ClienteEntity item : list) {
			item.setClienteEnderecoList(clienteEnderecoService.listarPorCliente(item.getId()));
			item.setClienteTelefoneList(clienteTelefoneService.listarPorCliente(item.getId()));
			item.setClienteTabelaPrecoList(clienteTabelaPrecoService.listarPorCliente(item.getId()));
			item.setClienteRazaoList(clienteRazaoService.listarPorCliente(item.getId()));
		}*/
		
		return mapperDao.listar(filtro);
	}	

	@Override
	public ClienteEntity salvar(ClienteEntity cliente) {
		if (cliente.getId() == null){
			cliente.setDataInclusao(new Date());
		}		
		
		cliente.setDataAtualizacao(new Date());
		
		List<ClienteEnderecoEntity> clienteEnderecoList = cliente.getClienteEnderecoList();
		List<ClienteTelefoneEntity> clienteTelefoneList = cliente.getClienteTelefoneList();
		List<ClienteTabelaPrecoEntity> clienteTabelaPrecoList = cliente.getClienteTabelaPrecoList();
		List<ClienteRazaoEntity> clienteRazaoList = cliente.getClienteRazaoList();
		
		cliente = dao.saveAndFlush(cliente);
		
		cliente.setClienteEnderecoList(clienteEnderecoList);
		cliente.setClienteTelefoneList(clienteTelefoneList);
		cliente.setClienteRazaoList(clienteRazaoList);
		cliente.setClienteTabelaPrecoList(clienteTabelaPrecoList);
		clienteEnderecoService.salvar(cliente);
		clienteTelefoneService.salvar(cliente);
		clienteRazaoService.salvar(cliente);
		clienteTabelaPrecoService.salvar(cliente);
		
		return cliente;
	}
	
//	@Override
//	public ClienteEntity obterFicha(FichaDto filtro) {
//		filtro.setUfList(filtro.getUfList() == null || filtro.getUfList().isEmpty() ? null : filtro.getUfList());
//		filtro.setNaturalidade(filtro.getNaturalidade() == null || filtro.getNaturalidade().isEmpty() ? null : filtro.getNaturalidade());
//		
////		List<ClienteEntity> prestadoresServicoList = mapperDao.listar(filtro);
////		
////		if (prestadoresServicoList != null && prestadoresServicoList.size() > 0) {			
////			return prestadoresServicoList.get(0);
////		}			
//		
//		return null;
//	}

	@Override
	public ClienteEntity obter(Integer id) {
		ClienteEntity obj = dao.obter(id);
		
		if (obj != null) {
			obj.setClienteEnderecoList(clienteEnderecoService.listarPorCliente(obj.getId()));
			obj.setClienteTelefoneList(clienteTelefoneService.listarPorCliente(obj.getId()));
			obj.setClienteTabelaPrecoList(clienteTabelaPrecoService.listarPorCliente(obj.getId()));
			obj.setClienteRazaoList(clienteRazaoService.listarPorCliente(obj.getId()));
		}
		
		return obj;
	}
	
	@Override
	public ClienteEntity obterFicha(Integer id) {
		ClienteEntity obj = dao.obter(id);
		
		if (obj != null) {
			obj.setClienteEnderecoList(clienteEnderecoService.listarPorCliente(obj.getId()));
			obj.setClienteTelefoneList(clienteTelefoneService.listarPorCliente(obj.getId()));
			obj.setClienteTabelaPrecoList(clienteTabelaPrecoService.listarPorCliente(obj.getId()));
			obj.setClienteRazaoList(clienteRazaoService.listarPorCliente(obj.getId()));
			obj.setOrdemServicoList(ordemServicoService.listarPorCliente(obj.getId())); 
			
			// Erro de recursividade infinita
			// Could not write JSON: Infinite recursion (StackOverflowError);
			for (OrdemServicoEntity os : obj.getOrdemServicoList()) {
				os.setCliente(null);
				os.setPagarReceberList(pagarReceberService.listarPorOrdemServico(os.getId()));
			}
		}
		
		return obj;
	}
	
	@Override
	public void cancelar(ClienteEntity cliente) {
		Integer idUsuarioAlteracao = cliente.getIdUsuarioAlteracao();
		ClienteEntity obj = dao.obter(cliente.getId());
		
		if (obj != null) {
			obj.setIdUsuarioAlteracao(idUsuarioAlteracao);
			obj.setCancelado(true);
			dao.saveAndFlush(obj);
		}
	}

}
