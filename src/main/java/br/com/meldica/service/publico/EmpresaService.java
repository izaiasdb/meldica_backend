package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.EmpresaEntity;

public interface EmpresaService {
	List<EmpresaEntity> listar();

	EmpresaEntity salvar(EmpresaEntity empresa);

	List<EmpresaEntity> listar(EmpresaEntity empresa);
}
