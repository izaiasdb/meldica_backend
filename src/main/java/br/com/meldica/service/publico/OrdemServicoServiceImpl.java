package br.com.meldica.service.publico;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.OrdemServicoDao;
import br.com.meldica.model.dao.publico.OrdemServicoMapperDao;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoFormaPagamentoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoProdutoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoTransportadoraEntity;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.enums.StatusNotaEnum;

@Service
public class OrdemServicoServiceImpl implements OrdemServicoService {

	@Autowired OrdemServicoDao dao;
	@Autowired OrdemServicoMapperDao mapperDao;
	@Autowired OrdemServicoProdutoService ordemServicoProdutoService;
	@Autowired OrdemServicoFormaPagamentoService ordemServicoFormaPagamentoService;
	@Autowired OrdemServicoTransportadoraService ordemServicoTransportadoraService;
	@Autowired OrdemServicoStatusService ordemServicoStatusService;
	@Autowired OrdemServicoKitService ordemServicoKitService;
	@Autowired OrdemServicoKitProdutoService ordemServicoKitProdutoService;
	@Autowired PagarReceberService pagarReceberService;
	@Autowired PagarReceberItemService pagarReceberItemService;

	@Override
	public OrdemServicoEntity obter(Integer id) {
		OrdemServicoEntity item = dao.obter(id);
		
		preencherRelacionamentos(item);
		
		return item;
	}	
	
	@Override
	public OrdemServicoEntity obterUltimaCompraCliente(Integer idCliente) {
		List<OrdemServicoEntity> list = dao.listarPorCliente(idCliente);
		
		if (list != null && list.size() > 0) {
			OrdemServicoEntity item = list.get(0);
			preencherRelacionamentos(item);
			
			return item;	
		}
		
		return null;
	}	

	@Override
	public List<OrdemServicoEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<OrdemServicoEntity> listar(OrdemServicoEntity filtro) {
		Assert.isTrue(filtro != null , "Preencha os dados.");
		filtro.setCliente(filtro.getCliente() == null || filtro.getCliente().getId() == null ? null : filtro.getCliente());
		filtro.setFuncionario(filtro.getFuncionario() == null || filtro.getFuncionario().getId() == null ? null : filtro.getFuncionario());
		filtro.setPlanoConta(filtro.getPlanoConta() == null || filtro.getPlanoConta().getId() == null ? null : filtro.getPlanoConta());
		filtro.setTabelaPreco(filtro.getTabelaPreco() == null || filtro.getTabelaPreco().getId() == null ? null : filtro.getTabelaPreco());
		filtro.setPeriodoVenda(filtro.getPeriodoVenda() == null || (filtro.getPeriodoVenda() != null && filtro.getPeriodoVenda().size() > 1 && filtro.getPeriodoVenda().get(0) == null) ? null : filtro.getPeriodoVenda());
		filtro.setPeriodoEntrega(filtro.getPeriodoEntrega() == null || (filtro.getPeriodoEntrega() != null && filtro.getPeriodoEntrega().size() > 1 && filtro.getPeriodoEntrega().get(0) == null) ? null : filtro.getPeriodoEntrega());
		filtro.setPeriodoLiberacao(filtro.getPeriodoLiberacao() == null || (filtro.getPeriodoLiberacao() != null && filtro.getPeriodoLiberacao().size() > 1 && filtro.getPeriodoLiberacao().get(0) == null) ? null : filtro.getPeriodoLiberacao());
		filtro.setPeriodoPrevisaoEntrega(filtro.getPeriodoPrevisaoEntrega() == null || (filtro.getPeriodoPrevisaoEntrega() != null && filtro.getPeriodoPrevisaoEntrega().size() > 1 && filtro.getPeriodoPrevisaoEntrega().get(0) == null) ? null : filtro.getPeriodoPrevisaoEntrega());
		
		return mapperDao.listar(filtro);
	}
	
	@Override
	@Transactional
	public void salvar(OrdemServicoEntity ordemServico) {
		boolean incluindo = false;
		String statusAnterior = "";
		
		if (ordemServico.getId() == null){
			ordemServico.setDataInclusao(new Date());
			ordemServico.setStatusNota(StatusNotaEnum.ABERTO.getId());
			incluindo = true;
		} else {
			OrdemServicoEntity ordemServicoOld = dao.obter(ordemServico.getId());
			statusAnterior = ordemServicoOld.getStatusNota();
		}
		
		if (ordemServico.getStatusNota().equals(StatusNotaEnum.REABERTO.getId())) {
			ordemServico.setStatusNota(StatusNotaEnum.LOGISTICA.getId());
		}
		
		ordemServico.setDataAtualizacao(new Date());
		
		List<OrdemServicoProdutoEntity> ordemServicoProdutoList = ordemServico.getProdutoItemsList();
		List<OrdemServicoFormaPagamentoEntity> ordemServicoFormaPagamentoList = ordemServico.getFormaItemsList();
		List<OrdemServicoTransportadoraEntity> ordemServicoTransportadoraList = ordemServico.getTransportadoraItemsList();
		List<OrdemServicoKitEntity> ordemServicoKitList = ordemServico.getKitList();
		List<OrdemServicoKitProdutoEntity> ordemServicoKitProdutoList = ordemServico.getKitProdutoList();
		
		if (!incluindo) {
			ordemServicoStatusService.salvar(ordemServico, incluindo, statusAnterior);
		}
		
		ordemServico = dao.saveAndFlush(ordemServico);
		
		if (incluindo) {
			ordemServicoStatusService.salvar(ordemServico, incluindo, statusAnterior);	
		}
		
		ordemServico.setProdutoItemsList(ordemServicoProdutoList);
		ordemServico.setFormaItemsList(ordemServicoFormaPagamentoList);
		ordemServico.setTransportadoraItemsList(ordemServicoTransportadoraList);
		ordemServico.setKitList(ordemServicoKitList);
		ordemServico.setKitProdutoList(ordemServicoKitProdutoList);
		
		ordemServicoProdutoService.salvar(ordemServico);
		ordemServicoFormaPagamentoService.salvar(ordemServico);
		ordemServicoTransportadoraService.salvar(ordemServico);
		ordemServicoKitService.salvar(ordemServico);
	}
	
	@Override
	@Transactional
	public void alterarStatus(OrdemServicoEntity ordemServico) {
//		Assert.isTrue(idOrdemServico != null, "Por favor informe o número.");
		OrdemServicoEntity ordemServicoOld = dao.obter(ordemServico.getId());
		String statusAnterior = ordemServicoOld.getStatusNota();
		String novoStatus = ordemServico.getStatusNota();
		Integer idUsuarioAlteracao = ordemServico.getIdUsuarioAlteracao();
		
		ordemServico = obter(ordemServico.getId());
		ordemServico.setIdUsuarioAlteracao(idUsuarioAlteracao);
		ordemServico.setStatusNota(novoStatus);
		
		// Gerar formas sem ter informado formas de pagamento
		if (ordemServico.getStatusNota().equals(StatusNotaEnum.FECHADO.getId()) && 
				(ordemServico.getFormaItemsList() == null || ordemServico.getFormaItemsList().size() == 0)) {
			throw new IllegalArgumentException("Sem formas para gerar.");
		}
		
		if (ordemServico.getStatusNota().equals(StatusNotaEnum.FECHADO.getId()) && 
				(ordemServico.getTransportadoraItemsList() == null || ordemServico.getTransportadoraItemsList().size() == 0)) {
			throw new IllegalArgumentException("Nota não pode ser fechada sem informar nenhuma transportadora.");
		}
		
		if (ordemServico.getStatusNota().equals(StatusNotaEnum.FECHADO.getId()) && 
				ordemServico.getFormaItemsList() != null && ordemServico.getFormaItemsList().size() > 0) {
			
			BigDecimal totalProduto = BigDecimal.ZERO;
			BigDecimal totalKit = BigDecimal.ZERO;
			BigDecimal totalForma = BigDecimal.ZERO;
			BigDecimal totalFrete = BigDecimal.ZERO;
			BigDecimal total = BigDecimal.ZERO;
			
			for (final OrdemServicoProdutoEntity item : ordemServico.getProdutoItemsList()) {
				if (!item.isBonificacao()) {
					totalProduto = totalProduto.add(item.getTotal());
				}
			}
			
			for (final OrdemServicoKitProdutoEntity item : ordemServico.getKitProdutoList()) {
				if (!item.isBonificacao()) {
					totalKit = totalKit.add(item.getTotal());
				}
			}
			
			for (final OrdemServicoFormaPagamentoEntity item : ordemServico.getFormaItemsList()) {
				totalForma = totalForma.add(item.getValor());
			}
			
			for (final OrdemServicoTransportadoraEntity item : ordemServico.getTransportadoraItemsList()) {
				totalFrete = totalFrete.add(item.getValorFrete());
			}		
			
			total = total.add(totalProduto);
			total = total.add(totalKit);
			total = total.add(totalFrete);
			
			DecimalFormat df = new DecimalFormat("##.##");
			df.setRoundingMode(RoundingMode.DOWN);			
			
			if (total.compareTo(totalForma) > 0) {
				throw new IllegalArgumentException("Total de forma de pagamento não pode ser diferente que a soma do total de produtos e frete.");	
			}
		}
		
		ordemServico.setDataAtualizacao(new Date());
		
		if (ordemServico.getStatusNota().equals(StatusNotaEnum.LIBERADO.getId())) {
			ordemServico.setDataLiberacao(new Date());
		} else if (ordemServico.getStatusNota().equals(StatusNotaEnum.ENTREGUE.getId())) {
			ordemServico.setDataEntrega(new Date());
		} else if (ordemServico.getStatusNota().equals(StatusNotaEnum.CANCELADO.getId())) {
			ordemServico.setCancelado(true);
		}
		
		ordemServicoStatusService.salvar(ordemServico, false, statusAnterior);
		ordemServico = dao.saveAndFlush(ordemServico);
		
		// Salva novamente para não exec a trigger
//		if (ordemServico.getStatusNota().equals("L") && !ordemServico.isEstoqueGerado()) {
//			ordemServico.setEstoqueGerado(true);
//		} else if (ordemServico.getStatusNota().equals("O") && !ordemServico.isFormaGerada()) {
//			ordemServico.setFormaGerada(true);
//		}
	}

	@Override
	public List<OrdemServicoEntity> listarPorStatus(String statusNota) {
		return dao.listarPorStatus(statusNota);
	}
	
	public void preencherRelacionamentos(OrdemServicoEntity item) {
		item.setProdutoItemsList(ordemServicoProdutoService.listarPorOrdemServico(item.getId()));
		item.setFormaItemsList(ordemServicoFormaPagamentoService.listarPorOrdemServico(item.getId()));
		item.setTransportadoraItemsList(ordemServicoTransportadoraService.listarPorOrdemServico(item.getId()));
		item.setKitList(ordemServicoKitService.listarPorOrdemServico(item.getId()));
		item.setOrdemServicoStatusList(ordemServicoStatusService.listarPorOrdemServico(item.getId()));
	
		if (item.getKitList() != null && item.getKitList().size() > 0) {
			for (OrdemServicoKitEntity kit : item.getKitList()) {
				List<OrdemServicoKitProdutoEntity> kitProdutoList = ordemServicoKitProdutoService.listarPorOrdemServicoKit(kit.getId());
				
				if (kitProdutoList != null && kitProdutoList.size() > 0) {
					item.getKitProdutoList().addAll(kitProdutoList);	
				}				
			}
		}
		
		if (item.getFormaItemsList() != null && item.getFormaItemsList().size() > 0) {
			for (OrdemServicoFormaPagamentoEntity forma : item.getFormaItemsList()) {
				List<PagarReceberEntity> pagarReceberList = pagarReceberService.listarPorOrdemServicoForma(forma.getId());
				
				if (pagarReceberList != null && pagarReceberList.size() > 0) {
//					if (item.getPagarReceberList() == null) {
//						item.setPagarReceberList(new ArrayList<>());
//					}
					
					// Por enquanto não está deixando muito lento
//					for (PagarReceberEntity pagarReceber : pagarReceberList) {
//						List<PagarReceberItemEntity> pagarReceberItemList = pagarReceberItemService.listarPorPagarReceber(pagarReceber.getId());
//						
//						if (pagarReceber.getPagarReceberItemsList() == null) {
//							pagarReceber.setPagarReceberItemsList(new ArrayList<>());
//						}
//						
//						pagarReceber.getPagarReceberItemsList().addAll(pagarReceberItemList);
//					}
					
					pagarReceberList = pagarReceberList.stream()
							.sorted(Comparator.comparing(PagarReceberEntity::getDataVencimento))
							.collect(Collectors.toList());
					
					item.getPagarReceberList().addAll(pagarReceberList);	
				}
			}
		}
	}
	
	@Override
	public void gerarFinanceiro(Integer id) {
		List<PagarReceberEntity> pagarReceberList = pagarReceberService.listarPorOrdemServico(id);
		
		if (pagarReceberList == null || pagarReceberList.size() == 0) {
			mapperDao.gerarFinanceiro(id);	
		} else {
			throw new IllegalArgumentException("Financeiro já foi gerado!");
		}
	}
	
	@Override
	public void deletarFinanceiro(Integer id) {
		List<PagarReceberEntity> pagarReceberList = pagarReceberService.listarPorOrdemServico(id);
		
		if (pagarReceberList != null && pagarReceberList.size() > 0) {
			mapperDao.deletarFinanceiro(id);	
		} else {
			throw new IllegalArgumentException("Financeiro ainda não foi gerado!");
		}
	}

	@Override
	public List<OrdemServicoEntity> listarPorCliente(Integer idCliente) {
		return dao.listarPorCliente(idCliente);
	}

}
