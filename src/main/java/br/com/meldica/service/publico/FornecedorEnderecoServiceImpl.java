package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.FornecedorEnderecoDao;
import br.com.meldica.model.entity.publico.FornecedorEnderecoEntity;
import br.com.meldica.model.entity.publico.FornecedorEntity;

@Service
public class FornecedorEnderecoServiceImpl implements FornecedorEnderecoService {

	@Autowired
	FornecedorEnderecoDao dao;

	@Override
	public List<FornecedorEnderecoEntity> listarPorFornecedor(Integer idFornecedor) { 
		return dao.listarPorFornecedor(idFornecedor);
	}

	@Override
	@Transactional
	public void salvar(FornecedorEntity fornecedor) {
		List<FornecedorEnderecoEntity> newList = fornecedor.getFornecedorEnderecoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<FornecedorEnderecoEntity> oldList = listarPorFornecedor(fornecedor.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(FornecedorEnderecoEntity endereco : newList) {
				endereco.setIdFornecedor(fornecedor.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(fornecedor.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(fornecedor.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(fornecedor.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}
	
}
