package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.TransportadoraDao;
import br.com.meldica.model.entity.publico.TransportadoraEnderecoEntity;
import br.com.meldica.model.entity.publico.TransportadoraEntity;
import br.com.meldica.model.entity.publico.TransportadoraTelefoneEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class TransportadoraServiceImpl implements TransportadoraService {

	@Autowired TransportadoraDao dao;
	@Autowired TransportadoraEnderecoService clienteEnderecoService;
	@Autowired TransportadoraTelefoneService clienteTelefoneService;

	@Override
	public List<TransportadoraEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<TransportadoraEntity> listar(TransportadoraEntity cliente) {
		Assert.isTrue(cliente != null , "Preencha os dados.");
		
		List<TransportadoraEntity> list = new ArrayList<TransportadoraEntity>();
		
		if (StringUtils.isNullOrEmpty(cliente.getNome())) {
			list = dao.listar();
		} else
		{
			list = dao.listar(cliente.getNome());
		}
		
		for (TransportadoraEntity item : list) {
			item.setTransportadoraEnderecoList(clienteEnderecoService.listarPorTransportadora(item.getId()));
			item.setTransportadoraTelefoneList(clienteTelefoneService.listarPorTransportadora(item.getId()));
		}
		
		return list;
	}	

	@Override
	public TransportadoraEntity salvar(TransportadoraEntity cliente) {
		if (cliente.getId() == null){
			cliente.setDataInclusao(new Date());
		}		
		
		cliente.setDataAtualizacao(new Date());
		
		List<TransportadoraEnderecoEntity> clienteEnderecoList = cliente.getTransportadoraEnderecoList();
		List<TransportadoraTelefoneEntity> clienteTelefoneList = cliente.getTransportadoraTelefoneList();
		
		cliente = dao.saveAndFlush(cliente);
		
		cliente.setTransportadoraEnderecoList(clienteEnderecoList);
		cliente.setTransportadoraTelefoneList(clienteTelefoneList);;
		clienteEnderecoService.salvar(cliente);
		clienteTelefoneService.salvar(cliente);
		
		return cliente;
	}

	@Override
	public TransportadoraEntity obter(Integer id) {
		return dao.obter(id);
	}


}
