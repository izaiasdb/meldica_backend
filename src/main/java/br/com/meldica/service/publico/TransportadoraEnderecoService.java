package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.TransportadoraEnderecoEntity;
import br.com.meldica.model.entity.publico.TransportadoraEntity;

public interface TransportadoraEnderecoService {
	/**
	 * Listaro endereço pelo sequencial do cliente
	 * @param idTransportadora
	 * @return listarPorTransportadora
	 */
	List<TransportadoraEnderecoEntity> listarPorTransportadora(Integer idTransportadora);
	
	void salvar(TransportadoraEntity cliente);

	List<TransportadoraEnderecoEntity> listar();
}
