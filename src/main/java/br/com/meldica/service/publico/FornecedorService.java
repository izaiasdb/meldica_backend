package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FornecedorEntity;

public interface FornecedorService {
	List<FornecedorEntity> listar();

	FornecedorEntity salvar(FornecedorEntity fornecedor);

	List<FornecedorEntity> listar(FornecedorEntity fornecedor);
}
