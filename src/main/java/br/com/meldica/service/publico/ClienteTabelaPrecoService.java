package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteTabelaPrecoEntity;

public interface ClienteTabelaPrecoService {
	List<ClienteTabelaPrecoEntity> listar();
	
	List<ClienteTabelaPrecoEntity> listarPorCliente(Integer idCliente);
	
	void salvar(ClienteEntity cliente);
}
