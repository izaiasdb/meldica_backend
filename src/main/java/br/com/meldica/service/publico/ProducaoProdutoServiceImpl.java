package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.ProducaoProdutoDao;
import br.com.meldica.model.entity.publico.ProducaoEntity;
import br.com.meldica.model.entity.publico.ProducaoProdutoEntity;

@Service
public class ProducaoProdutoServiceImpl implements ProducaoProdutoService {

	private @Autowired ProducaoProdutoDao dao;

	@Override
	public List<ProducaoProdutoEntity> listarPorProducao(Integer idProducao) { 
		return dao.listarPorProducao(idProducao);
	}

	@Override
	@Transactional
	public void salvar(ProducaoEntity producao) {
		List<ProducaoProdutoEntity> novoList = producao.getProdutoItemsList();
		List<ProducaoProdutoEntity> oldList = listarPorProducao(producao.getId());
		
		if(novoList != null && !novoList.isEmpty()) {
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ProducaoProdutoEntity novo : novoList) {
				novo.setIdProducao(producao.getId());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(producao.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(producao.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(producao.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				dao.save(novo);
			}
			// Novo está vázio e o antigo tem registro
		} else if((novoList == null || novoList.isEmpty()) && (oldList != null && !oldList.isEmpty())) {
			dao.deleteAll(oldList);
		}
	}
	
}
