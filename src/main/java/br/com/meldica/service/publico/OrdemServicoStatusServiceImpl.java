package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.OrdemServicoStatusDao;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoStatusEntity;
import br.com.meldica.model.enums.StatusNotaEnum;

@Service
public class OrdemServicoStatusServiceImpl implements OrdemServicoStatusService {

	private @Autowired OrdemServicoStatusDao dao;
	private @Autowired OrdemServicoService ordemServicoService;

	@Override
	public List<OrdemServicoStatusEntity> listarPorOrdemServico(Integer idOrdemServico) { 
		return dao.listarPorOrdemServico(idOrdemServico);
	}
	
	@Override
	@Transactional
	public void salvar(OrdemServicoEntity ordemServico, boolean incluindo, String statusAnterior) {
		Date now = new Date();
		
		if (incluindo) {
			OrdemServicoStatusEntity ordemServicoStatus = new OrdemServicoStatusEntity();
			ordemServicoStatus.setIdOrdemServico(ordemServico.getId());
			ordemServicoStatus.setIdUsuarioInclusao(ordemServico.getIdUsuarioInclusao());
			ordemServicoStatus.setStatusAnterior(StatusNotaEnum.ABERTO.getId());
			ordemServicoStatus.setStatusAtual(StatusNotaEnum.ABERTO.getId());
			ordemServicoStatus.setDataInclusao(now);
			dao.save(ordemServicoStatus);
		} else {
			if(!ordemServico.getStatusNota().equals(statusAnterior)) {
				OrdemServicoStatusEntity ordemServicoStatus = new OrdemServicoStatusEntity();
				ordemServicoStatus.setIdOrdemServico(ordemServico.getId());
				ordemServicoStatus.setIdUsuarioInclusao(ordemServico.getIdUsuarioInclusao());
				ordemServicoStatus.setStatusAnterior(statusAnterior);
				ordemServicoStatus.setStatusAtual(ordemServico.getStatusNota());
				ordemServicoStatus.setDataInclusao(now);
				dao.save(ordemServicoStatus);
			}
		}
	}
	
}
