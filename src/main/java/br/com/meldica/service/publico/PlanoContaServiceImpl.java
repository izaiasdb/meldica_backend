package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.PlanoContaDao;
import br.com.meldica.model.entity.publico.PlanoContaEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class PlanoContaServiceImpl implements PlanoContaService {

	private @Autowired PlanoContaDao dao;

	@Override
	public List<PlanoContaEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<PlanoContaEntity> listar(PlanoContaEntity planoConta) {
		Assert.isTrue(planoConta != null , "Preencha os dados.");
		
//		List<PlanoContaEntity> list = new ArrayList<>(); 
//				
//		if (StringUtils.isNullOrEmpty(planoConta.getNome())) {
//			//list = dao.findAll();
//			list = dao.lisfindAll();
//		} else
//		{
//			list = dao.listar(planoConta.getNome());
//		}
		
//		return list;
		
		return dao.listar(planoConta);
	}
	
	@Override
	public List<PlanoContaEntity> listarPorNivel(Integer nivel) {
		Assert.isTrue(nivel != null , "Nível precisa ser informado.");
		
		List<PlanoContaEntity> list = dao.listarPorNivel(nivel);

		return list;		
	}	
	
	@Override
	public List<PlanoContaEntity> listarPorNivel(Integer nivel, Integer idPlanoContaPai) {
		Assert.isTrue(nivel != null , "Nível precisa ser informado.");
		
		List<PlanoContaEntity> list = dao.listarPorNivel(nivel, idPlanoContaPai);

		return list;		
	}

	@Override
	public PlanoContaEntity salvar(PlanoContaEntity planoConta) {
		if (planoConta.getPlanoContaPai() != null && planoConta.getPlanoContaPai().getId() == null) {
			planoConta.setPlanoContaPai(null);
		}
		
		List<PlanoContaEntity> list = new ArrayList<>();
		PlanoContaEntity pai = null;
		
		if (planoConta.getPlanoContaPai() != null) {
			pai = dao.obter(planoConta.getPlanoContaPai().getId());
			
			list = listarPorNivel(planoConta.getNivel(), planoConta.getPlanoContaPai().getId());
		} else {
			list = listarPorNivel(planoConta.getNivel());
		}
		
		if (list != null && list.size() > 0) {
			PlanoContaEntity planoMax =  Collections.max(list, Comparator.comparing(s -> Long.valueOf(s.getNumeroConta())));
			
			//Se estiver editando, mas o plano não é o máximo
			//if (planoConta.getId() != null && !planoConta.getId().equals(planoMax.getId())){
			if (planoMax != null && (planoConta.getId() == null || planoConta.getId() != null && !planoConta.getId().equals(planoMax.getId()) )){
				Long numeroConta = Long.valueOf(planoMax.getNumeroConta());
				numeroConta++; 
				
				String novaConta = String.valueOf(numeroConta);
				novaConta = StringUtils.lpad(novaConta, planoConta.getNivel() * 4, '0');
				
//				if (planoConta.getPlanoContaPai() != null && pai != null) {
//					planoConta.setNumeroConta(pai.getNumeroConta() + novaConta);
//				} else {
					planoConta.setNumeroConta(novaConta);	
//				}				
			}
		} else {
			if (planoConta.getPlanoContaPai() != null && pai != null) {
				planoConta.setNumeroConta(pai.getNumeroConta() + StringUtils.lpad("1", 4, '0'));
			} else {
				planoConta.setNumeroConta(StringUtils.lpad("1", 4, '0'));	
			}
		}
				
		planoConta.setDataAlteracao(new Date());
		return dao.saveAndFlush(planoConta);
	}
	
	public static void main(String[] args) {
//		String value = StringUtils.lpad("10", 4, 'X');
//		
		List<PlanoContaEntity> list = new ArrayList<>();
//        list.add(new PlanoContaEntity(1, "Pea", "0001"));        
//        list.add(new PlanoContaEntity(1, "Pang", "0003"));
//        list.add(new PlanoContaEntity(1, "Ni", "0002"));
        list.add(new PlanoContaEntity(2, "Pang", "00010005"));
        list.add(new PlanoContaEntity(2, "Tum", "00010004"));
        list.add(new PlanoContaEntity(3, "Sombut", "00010005"));
        
        
        PlanoContaEntity planoMax =  Collections.max(list, Comparator.comparing(s -> Integer.valueOf(s.getNumeroConta())));
        
        System.out.println(planoMax);
	}


}
