package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.entity.publico.PagarReceberItemEntity;

public interface PagarReceberItemService {
	List<PagarReceberItemEntity> listarPorPagarReceber(Integer idPagarReceber);
	
//	void salvar(PagarReceberEntity pagarReceber);

	void salvarPorPagarReceber(PagarReceberEntity pagarReceber);

	void salvar(PagarReceberItemEntity pagarReceberItem);

	void excluir(PagarReceberItemEntity pagarReceberItem);
}
