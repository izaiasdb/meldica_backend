package br.com.meldica.service.publico;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.publico.CondicaoPagamentoEntity;

public interface CondicaoPagamentoService {
	List<CondicaoPagamentoEntity> listar();
	List<CondicaoPagamentoEntity> listarAtivos();
	CondicaoPagamentoEntity salvar(@RequestBody CondicaoPagamentoEntity entity);
	List<CondicaoPagamentoEntity> listar(CondicaoPagamentoEntity entity);
	void cancelar(CondicaoPagamentoEntity entity);
}
