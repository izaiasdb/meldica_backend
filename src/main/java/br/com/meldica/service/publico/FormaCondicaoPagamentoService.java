package br.com.meldica.service.publico;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import br.com.meldica.model.entity.publico.FormaCondicaoPagamentoEntity;
public interface FormaCondicaoPagamentoService {
	List<FormaCondicaoPagamentoEntity> listar();
	List<FormaCondicaoPagamentoEntity> listarAtivos();
	FormaCondicaoPagamentoEntity salvar(@RequestBody FormaCondicaoPagamentoEntity entity);
	List<FormaCondicaoPagamentoEntity> listar(FormaCondicaoPagamentoEntity entity);
	void cancelar(FormaCondicaoPagamentoEntity entity);
}
