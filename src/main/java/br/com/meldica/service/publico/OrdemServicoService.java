package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.dto.relatorios.RelatorioResumoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;

public interface OrdemServicoService {
	OrdemServicoEntity obter(Integer id);	
	
	List<OrdemServicoEntity> listar();

	void salvar(OrdemServicoEntity ordemServico);

	List<OrdemServicoEntity> listar(OrdemServicoEntity ordemServico);

	void alterarStatus(OrdemServicoEntity ordemServico);
	
	List<OrdemServicoEntity> listarPorStatus(String statusNota);

	List<OrdemServicoEntity> listarPorCliente(Integer idCliente);

	void gerarFinanceiro(Integer id);

	void deletarFinanceiro(Integer id);

	OrdemServicoEntity obterUltimaCompraCliente(Integer idCliente);

//	List<RelatorioResumoDto> relatorioResumoMensal(OrdemServicoEntity ordemServico);
//
//	List<OrdemServicoEntity> listarDashboard(OrdemServicoEntity filtro);
}
