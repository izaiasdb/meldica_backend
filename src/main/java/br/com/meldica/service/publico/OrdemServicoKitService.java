package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;

public interface OrdemServicoKitService {
	/**
	 * Listar por ordem de serviço
	 * @param idOrdemServico
	 * @return
	 */
	List<OrdemServicoKitEntity> listarPorOrdemServico(Integer idOrdemServico);
	
	void salvar(OrdemServicoEntity ordemServico);
}
