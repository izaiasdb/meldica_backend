package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.TransportadoraEntity;
import br.com.meldica.model.entity.publico.TransportadoraTelefoneEntity;

public interface TransportadoraTelefoneService {
	/**
	 * Listaro telefone pelo sequencial do cliente
	 * @param idTransportadora
	 * @return listarPorTransportadora
	 */
	List<TransportadoraTelefoneEntity> listarPorTransportadora(Integer idTransportadora);
	
	void salvar(TransportadoraEntity cliente);
}
