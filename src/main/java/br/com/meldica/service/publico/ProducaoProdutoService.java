package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ProducaoEntity;
import br.com.meldica.model.entity.publico.ProducaoProdutoEntity;

public interface ProducaoProdutoService {
	List<ProducaoProdutoEntity> listarPorProducao(Integer idProducao);
	
	void salvar(ProducaoEntity producao);
}
