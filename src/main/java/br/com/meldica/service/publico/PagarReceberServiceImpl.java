package br.com.meldica.service.publico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.FormaCondicaoPagamentoDao;
import br.com.meldica.model.dao.publico.PagarReceberDao;
import br.com.meldica.model.dao.publico.PagarReceberItemDao;
import br.com.meldica.model.dao.publico.PagarReceberMapperDao;
import br.com.meldica.model.dto.relatorios.PagarReceberDto;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.entity.publico.PagarReceberItemEntity;
import br.com.meldica.utils.DateUtils;
import br.com.meldica.utils.GeralConst;
import br.com.meldica.utils.StringUtils;

@Service
public class PagarReceberServiceImpl implements PagarReceberService {

	@Autowired PagarReceberDao dao;
	@Autowired PagarReceberItemDao pagarReceberItemDao;
	@Autowired PagarReceberItemService pagarReceberItemService;
	@Autowired private FormaCondicaoPagamentoDao formaCondicaoPagamentoDao;
	@Autowired private PagarReceberMapperDao mapperDao;

	@Override
	public PagarReceberEntity obter(Integer id) {
		PagarReceberEntity item = dao.obter(id);
		
		item.setPagarReceberItemsList(pagarReceberItemService.listarPorPagarReceber(item.getId()));
			
		if (!StringUtils.isNullOrEmpty(item.getCompetencia())) {
			item.setMes(item.getCompetencia().substring(0, 2));
			item.setAno(item.getCompetencia().substring(2, 6));
		}
		
		return item;
	}	
	
	@Override
	public List<PagarReceberEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<PagarReceberEntity> listar(PagarReceberEntity pagarReceber) {
		Assert.isTrue(pagarReceber != null , "Preencha os dados.");
		
		return mapperDao.listar(pagarReceber);
	}	
	
	@Override
	public List<PagarReceberEntity> listarPorOrdemServico(Integer idOrdemServico) {
		Assert.isTrue(idOrdemServico != null , "Preencha os dados.");
		
		return mapperDao.listarPorOrdemServico(idOrdemServico);
	}	

	@Override
	public void salvar(PagarReceberEntity pagarReceber) {
		Date data = new  Date();
		boolean inserindo = false;
		Integer tipoLancamento = pagarReceber.getTipoLancamento();
		List<PagarReceberItemEntity> pagarReceberItemList = pagarReceber.getPagarReceberItemsList();
		
		if (pagarReceber.getCliente() != null && pagarReceber.getCliente().getId() == null){
			pagarReceber.setCliente(null);
		}
		
		if (pagarReceber.getFornecedor() != null && pagarReceber.getFornecedor().getId() == null){
			pagarReceber.setFornecedor(null);
		}
		
		if (pagarReceber.getId() == null){
			pagarReceber.setDataInclusao(data);
			inserindo = true; 
		}		
		
		 // Parcelado
		if (inserindo && tipoLancamento == 2 && pagarReceber.getParcelas() > 0) {
			BigDecimal valor = pagarReceber.getValor().divide(BigDecimal.valueOf(pagarReceber.getParcelas()));
			
			int mes = 0;
			int ano = Integer.parseInt(pagarReceber.getAno());
					
			// Remover o zero
			if (pagarReceber.getMes().charAt(0) == '0') {
				mes = Integer.parseInt(pagarReceber.getMes().substring (1, pagarReceber.getMes().length()));	
			} else {
				mes = Integer.parseInt(pagarReceber.getMes());
			}
			
			for (int i = 1; i <= pagarReceber.getParcelas(); i++) {
				PagarReceberEntity pagarReceberParcelado = new PagarReceberEntity();
				pagarReceberParcelado.setDataInclusao(data);
				pagarReceberParcelado.setDataAtualizacao(data);
				pagarReceberParcelado.setIdUsuarioInclusao(pagarReceber.getIdUsuarioInclusao());
				pagarReceberParcelado.setIdUsuarioAlteracao(pagarReceber.getIdUsuarioInclusao());
				pagarReceberParcelado.setValor(valor);
				pagarReceberParcelado.setValorPago(BigDecimal.ZERO);
				pagarReceberParcelado.setDocumento(pagarReceber.getDocumento() + " " + i + "/" + pagarReceber.getParcelas());
				pagarReceberParcelado.setDescricao(pagarReceber.getDescricao());
				pagarReceberParcelado.setCliente(pagarReceber.getCliente());
				pagarReceberParcelado.setFornecedor(pagarReceber.getFornecedor());
				pagarReceberParcelado.setObservacao(pagarReceber.getObservacao());
				pagarReceberParcelado.setPlanoConta(pagarReceber.getPlanoConta());
				pagarReceberParcelado.setReceitaDespesa(pagarReceber.getReceitaDespesa());
				
				if (i == 1) {
					pagarReceberParcelado.setAno(pagarReceber.getAno());
					pagarReceberParcelado.setMes(pagarReceber.getMes());	
					pagarReceberParcelado.setCompetencia(StringUtils.lpad(pagarReceberParcelado.getMes(), 2, '0') + pagarReceberParcelado.getAno());	
					pagarReceberParcelado.setDataAtualizacao(data);					
					pagarReceberParcelado.setDataVencimento(pagarReceber.getDataVencimento());
				} else {
					Calendar cal = DateUtils.dateToCalendar(pagarReceber.getDataVencimento()); 
					cal.add(Calendar.MONTH, i -1);
					pagarReceberParcelado.setDataVencimento(DateUtils.calendarToDate(cal));
					pagarReceberParcelado.setMes(String.valueOf(mes));
					pagarReceberParcelado.setAno(String.valueOf(ano));
					pagarReceberParcelado.setCompetencia(StringUtils.lpad(String.valueOf(mes), 2, '0') + ano);
				}
				
				mes++;
				
				if (mes > 12) {
					mes = 1;
					ano++;
				}
				
				dao.saveAndFlush(pagarReceberParcelado);
			}
		} else {
			pagarReceber.setCompetencia(StringUtils.lpad(pagarReceber.getMes(), 2, '0') + pagarReceber.getAno());	
			pagarReceber.setDataAtualizacao(data);
			pagarReceber = dao.saveAndFlush(pagarReceber);
			
			if (inserindo && tipoLancamento == 1) { // Á vista
				if (pagarReceberItemList == null) {
					pagarReceberItemList = new ArrayList<>();
				}
				
				PagarReceberItemEntity pagarReceberItem = new PagarReceberItemEntity();
				pagarReceberItem.setIdPagarReceber(pagarReceber.getId());
				pagarReceberItem.setValor(pagarReceber.getValor());
				pagarReceberItem.setIdUsuarioInclusao(pagarReceber.getIdUsuarioInclusao());
				pagarReceberItem.setIdUsuarioAlteracao(pagarReceber.getIdUsuarioInclusao());
				pagarReceberItem.setDataPagamento(data);
				pagarReceberItem.setFormaCondicaoPagamento(formaCondicaoPagamentoDao.obter(GeralConst.FORMA_CONDICAO_A_VISTA));
				pagarReceberItem.setJuros(BigDecimal.ZERO);
				pagarReceberItem.setAcrescimo(BigDecimal.ZERO);
				pagarReceberItem.setDesconto(BigDecimal.ZERO);
				pagarReceberItemList.add(pagarReceberItem);
			} 
			
//			if (!inserindo && pagarReceber.getValor() < pagarReceber.getValorPago()) {
//				
//			}
			
			pagarReceber.setPagarReceberItemsList(pagarReceberItemList);
			pagarReceberItemService.salvarPorPagarReceber(pagarReceber);
		}
	}

	@Override
	public void pagar(PagarReceberEntity pagarReceber) {
		Date data = new  Date();
		pagarReceber.setDataAtualizacao(new Date());
		
		List<PagarReceberItemEntity> pagarReceberItemList = pagarReceber.getPagarReceberItemsList();
		
		// Salvar quem atualizou
		pagarReceber = dao.saveAndFlush(pagarReceber);
		
		if (pagarReceberItemList == null) {
			pagarReceberItemList = new ArrayList<>();
		}
		
		PagarReceberItemEntity pagarReceberItem = new PagarReceberItemEntity();
		pagarReceberItem.setIdPagarReceber(pagarReceber.getId());
		pagarReceberItem.setFormaCondicaoPagamento(formaCondicaoPagamentoDao.obter(GeralConst.FORMA_CONDICAO_A_VISTA));
		pagarReceberItem.setValor(pagarReceber.getValor().subtract(pagarReceber.getValorPago()));
		pagarReceberItem.setIdUsuarioInclusao(pagarReceber.getIdUsuarioInclusao());
		pagarReceberItem.setIdUsuarioAlteracao(pagarReceber.getIdUsuarioInclusao());
		pagarReceberItem.setDataPagamento(data);
		pagarReceberItem.setJuros(BigDecimal.ZERO);
		pagarReceberItem.setAcrescimo(BigDecimal.ZERO);
		pagarReceberItem.setDesconto(BigDecimal.ZERO);
		pagarReceberItemList.add(pagarReceberItem);
		
		pagarReceber.setPagarReceberItemsList(pagarReceberItemList);
		pagarReceberItemService.salvarPorPagarReceber(pagarReceber);
	}
	
	@Transactional
	@Override
	public void excluir(PagarReceberEntity pagarReceber) {
		PagarReceberEntity excluir = dao.findById(pagarReceber.getId()).orElseThrow(() -> new IllegalArgumentException("Pagamento não encontrado."));
		excluir.setCancelado(true);
		excluir.setDataAtualizacao(new Date());
		dao.saveAndFlush(excluir);
		
//		List<PagarReceberItemEntity> pagarReceberItemList = pagarReceberItemService.listarPorPagarReceber(pagarReceber.getId());
//		
//		if (pagarReceberItemList == null) {
//			pagarReceberItemList = new ArrayList<>();
//		}
//		
//		for (PagarReceberItemEntity item : pagarReceberItemList) {
//			pagarReceberItemDao.delete(item);
//		}
//		
//		dao.delete(pagarReceber);
	}
	
	@Override
	public List<PagarReceberDto> listarRelatorio(PagarReceberDto filtro) {
//		Assert.isTrue(idUnidade != null, "Unidade precisa ser informada.");
		
		filtro.setDataInicio(DateUtils.dateToString(filtro.getPeriodo().get(0), "YYYY-MM-DD"));
		filtro.setDataFim(DateUtils.dateToString(filtro.getPeriodo().get(1), "YYYY-MM-DD"));
		
		if (filtro.getTipoRelatorio().equals("A"))
			return mapperDao.listarPagarReceber(filtro);
		else
			return mapperDao.listarPagoRecebido(filtro);
	}

	@Override
	public List<PagarReceberEntity> listarPorOrdemServicoForma(Integer idOsForma) {
		return dao.listarPorOrdemServicoForma(idOsForma);
	}
	

}
