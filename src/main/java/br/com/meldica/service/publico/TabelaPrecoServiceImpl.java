package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.TabelaPrecoDao;
import br.com.meldica.model.entity.publico.TabelaPrecoEntity;
import br.com.meldica.model.entity.publico.TabelaPrecoProdutoEntity;

@Service
public class TabelaPrecoServiceImpl implements TabelaPrecoService {

	@Autowired TabelaPrecoDao dao;
	@Autowired TabelaPrecoProdutoService tabelaPrecoProdutoService;

	@Override
	public List<TabelaPrecoEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<TabelaPrecoEntity> listar(TabelaPrecoEntity produto) {
		Assert.isTrue(produto != null , "Preencha os dados.");

		List<TabelaPrecoEntity> list = new ArrayList<TabelaPrecoEntity>();
		
		list = dao.listar();
		
		for (TabelaPrecoEntity item : list) {
			item.setProdutoItemsList(tabelaPrecoProdutoService.listarPorTabelaPreco(item.getId()));
		}
		
		return list;
	}	

	@Override
	public TabelaPrecoEntity salvar(TabelaPrecoEntity entity) {
		if (entity.getId() == null){
			entity.setDataInclusao(new Date());
		}		
		
		entity.setDataAtualizacao(new Date());
		
		List<TabelaPrecoProdutoEntity> produtoItemsList = entity.getProdutoItemsList();
		
		entity = dao.saveAndFlush(entity);
		
		entity.setProdutoItemsList(produtoItemsList);
		tabelaPrecoProdutoService.salvar(entity);
		
		return entity;
	}

	@Override
	public List<TabelaPrecoEntity> listarAtivos() {
		return dao.listarAtivos();
	}


}
