package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.CondicaoPagamentoDao;
import br.com.meldica.model.entity.publico.CondicaoPagamentoEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class CondicaoPagamentoServiceImpl implements CondicaoPagamentoService {

	@Autowired
	CondicaoPagamentoDao dao;

	@Override
	public List<CondicaoPagamentoEntity> listar() {
		return dao.listar();
	}
	

	@Override
	public List<CondicaoPagamentoEntity> listarAtivos() {
		return dao.listarAtivos();
	}
	
	@Override
	public List<CondicaoPagamentoEntity> listar(CondicaoPagamentoEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar();
		else
			return dao.listar(entity.getNome());
	}		
	
	@Override
	public CondicaoPagamentoEntity salvar(CondicaoPagamentoEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}
	
	@Override
	public void cancelar(CondicaoPagamentoEntity entity) {
		Integer idUsuarioAlteracao = entity.getIdUsuarioAlteracao();
		CondicaoPagamentoEntity obj = dao.obter(entity.getId());

		if (obj != null) {
			obj.setIdUsuarioAlteracao(idUsuarioAlteracao);
			obj.setCancelado(true);
			dao.saveAndFlush(obj);
		}
	}

}
