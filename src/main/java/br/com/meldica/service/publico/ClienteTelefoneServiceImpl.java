package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.ClienteTelefoneDao;
import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteTelefoneEntity;

@Service
public class ClienteTelefoneServiceImpl implements ClienteTelefoneService {

	@Autowired
	ClienteTelefoneDao dao;

	@Override
	public List<ClienteTelefoneEntity> listarPorCliente(Integer idCliente) { 
		return dao.listarPorCliente(idCliente);
	}

	@Override
	@Transactional
	public void salvar(ClienteEntity cliente) {
		List<ClienteTelefoneEntity> newList = cliente.getClienteTelefoneList();
		
		if(newList != null && !newList.isEmpty()) {
			List<ClienteTelefoneEntity> oldList = listarPorCliente(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ClienteTelefoneEntity telefone : newList) {
				telefone.setIdCliente(cliente.getId());
				
				if(Objects.isNull(telefone.getId())) {
					telefone.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					telefone.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				telefone.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				telefone.setDataAtualizacao(now);
				dao.save(telefone);
			}
		}
	}
	
}
