package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.FormaCondicaoPagamentoDao;
import br.com.meldica.model.entity.publico.FormaCondicaoPagamentoEntity;

@Service
public class FormaCondicaoPagamentoServiceImpl implements FormaCondicaoPagamentoService {

	@Autowired
	FormaCondicaoPagamentoDao dao;

	@Override
	public List<FormaCondicaoPagamentoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<FormaCondicaoPagamentoEntity> listarAtivos() {
		return dao.listarAtivos();
	}
	
	@Override
	public List<FormaCondicaoPagamentoEntity> listar(FormaCondicaoPagamentoEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
//		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar(entity);
//		else
//			return dao.listar(entity.getNome());
	}		
	
	@Override
	public FormaCondicaoPagamentoEntity salvar(FormaCondicaoPagamentoEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}
	
	@Override
	public void cancelar(FormaCondicaoPagamentoEntity entity) {
		Integer idUsuarioAlteracao = entity.getIdUsuarioAlteracao();
		FormaCondicaoPagamentoEntity obj = dao.obter(entity.getId());

		if (obj != null) {
			obj.setIdUsuarioAlteracao(idUsuarioAlteracao);
			obj.setCancelado(true);
			dao.saveAndFlush(obj);
		}
	}

}
