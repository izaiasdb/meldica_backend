package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.TabelaPrecoEntity;
import br.com.meldica.model.entity.publico.TabelaPrecoProdutoEntity;

public interface TabelaPrecoProdutoService {
	void salvar(TabelaPrecoEntity tabelaPreco);

	List<TabelaPrecoProdutoEntity> listarPorTabelaPreco(Integer idTabelaPreco);
	List<TabelaPrecoProdutoEntity> listar();
}
