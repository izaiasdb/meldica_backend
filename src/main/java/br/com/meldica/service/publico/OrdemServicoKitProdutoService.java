package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.OrdemServicoKitEntity;
import br.com.meldica.model.entity.publico.OrdemServicoKitProdutoEntity;

public interface OrdemServicoKitProdutoService {
	/**
	 * Listar por ordem de serviço
	 * @param idOrdemServico
	 * @return
	 */
	List<OrdemServicoKitProdutoEntity> listarPorOrdemServicoKit(Integer idOrdemServicoKit);
	
	void salvar(OrdemServicoKitEntity idOrdemServicoKit);
}
