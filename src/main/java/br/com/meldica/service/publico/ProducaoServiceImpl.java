package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.ProducaoDao;
import br.com.meldica.model.dao.publico.ProducaoMapperDao;
import br.com.meldica.model.entity.publico.ProducaoEntity;
import br.com.meldica.model.entity.publico.ProducaoProdutoEntity;
import br.com.meldica.model.enums.StatusNotaEnum;

@Service
public class ProducaoServiceImpl implements ProducaoService {

	@Autowired ProducaoDao dao;
	@Autowired ProducaoMapperDao mapperDao;
	@Autowired ProducaoProdutoService producaoProdutoService;

	@Override
	public ProducaoEntity obter(Integer id) {
		ProducaoEntity item = dao.obter(id);
		
		item.setProdutoItemsList(producaoProdutoService.listarPorProducao(item.getId()));
		
		return item;
	}	
	
	@Override
	public List<ProducaoEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<ProducaoEntity> listar(ProducaoEntity filtro) {
		Assert.isTrue(filtro != null , "Preencha os dados.");
		filtro.setPeriodoProducao(filtro.getPeriodoProducao() == null || (filtro.getPeriodoProducao() != null && filtro.getPeriodoProducao().size() > 1 && filtro.getPeriodoProducao().get(0) == null) ? null : filtro.getPeriodoProducao());
		
		return mapperDao.listar(filtro);
	}
	
	@Override
	@Transactional
	public void salvar(ProducaoEntity producao) {
		boolean incluindo = false;
		String statusAnterior = "";
		
		if (producao.getId() == null){
			producao.setDataInclusao(new Date());
			producao.setStatus(StatusNotaEnum.ABERTO.getId());
			incluindo = true;
		} else {
			ProducaoEntity producaoOld = dao.obter(producao.getId());
		}
		
		producao.setDataAtualizacao(new Date());
		
		List<ProducaoProdutoEntity> producaoProdutoList = producao.getProdutoItemsList();

		producao = dao.saveAndFlush(producao);
		
		producao.setProdutoItemsList(producaoProdutoList);
		
		producaoProdutoService.salvar(producao);
	}
	

}
