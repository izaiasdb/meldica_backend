package br.com.meldica.service.publico;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.PagarReceberItemDao;
import br.com.meldica.model.entity.publico.PagarReceberEntity;
import br.com.meldica.model.entity.publico.PagarReceberItemEntity;

@Service
public class PagarReceberItemServiceImpl implements PagarReceberItemService {

	private @Autowired PagarReceberItemDao dao;

	@Override
	public List<PagarReceberItemEntity> listarPorPagarReceber(Integer idPagarReceber) { 
		return dao.listarPorPagarReceber(idPagarReceber);
	}

	@Override
	@Transactional
	public void salvarPorPagarReceber(PagarReceberEntity pagarReceber) {
		List<PagarReceberItemEntity> novoList = pagarReceber.getPagarReceberItemsList();
		
		if(novoList != null && !novoList.isEmpty()) {
			List<PagarReceberItemEntity> oldList = listarPorPagarReceber(pagarReceber.getId());
			oldList.removeAll(novoList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(PagarReceberItemEntity novo : novoList) {
				novo.setIdPagarReceber(pagarReceber.getId());
				
				if(Objects.isNull(novo.getId())) {
					novo.setDataInclusao(now);
				}
				
				if(!Objects.isNull(pagarReceber.getIdUsuarioInclusao())) {
					novo.setIdUsuarioInclusao(pagarReceber.getIdUsuarioInclusao());
				}
				
				novo.setIdUsuarioAlteracao(pagarReceber.getIdUsuarioAlteracao());
				novo.setDataAtualizacao(now);
				dao.save(novo);
			}
		}
	}
	
	@Override
	@Transactional
	public void salvar(PagarReceberItemEntity pagarReceberItem) {
		Date now = new Date();
		
		if(Objects.isNull(pagarReceberItem.getId())) {
			pagarReceberItem.setDataInclusao(now);
		}
		
		pagarReceberItem.setJuros(BigDecimal.ZERO);
		pagarReceberItem.setAcrescimo(BigDecimal.ZERO);
		pagarReceberItem.setDesconto(BigDecimal.ZERO);
		pagarReceberItem.setDataAtualizacao(now);
		dao.save(pagarReceberItem);
	}
	
	@Transactional
	@Override
	public void excluir(PagarReceberItemEntity pagarReceberItem) {
		dao.delete(pagarReceberItem);
	}
	
}
