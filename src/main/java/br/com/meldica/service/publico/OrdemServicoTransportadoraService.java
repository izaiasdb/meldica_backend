package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.model.entity.publico.OrdemServicoTransportadoraEntity;

public interface OrdemServicoTransportadoraService {
	/**
	 * Listar por ordem de serviço
	 * @param idOrdemServico
	 * @return
	 */
	List<OrdemServicoTransportadoraEntity> listarPorOrdemServico(Integer idOrdemServico);
	
	void salvar(OrdemServicoEntity ordemServico);
}
