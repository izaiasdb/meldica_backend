package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.FornecedorEnderecoEntity;
import br.com.meldica.model.entity.publico.FornecedorEntity;

public interface FornecedorEnderecoService {
	/**
	 * Listaro endereço pelo sequencial do fornecedor
	 * @param idFornecedor
	 * @return listarPorFornecedor
	 */
	List<FornecedorEnderecoEntity> listarPorFornecedor(Integer idFornecedor);
	
	void salvar(FornecedorEntity fornecedor);
}
