package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.FormaPagamentoDao;
import br.com.meldica.model.entity.publico.FormaPagamentoEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class FormaPagamentoServiceImpl implements FormaPagamentoService {

	@Autowired
	FormaPagamentoDao dao;

	@Override
	public List<FormaPagamentoEntity> listar() {
		return dao.listar();
	}
	
	@Override
	public List<FormaPagamentoEntity> listarAtivos() {
		return dao.listarAtivos();
	}
	
	@Override
	public List<FormaPagamentoEntity> listar(FormaPagamentoEntity entity) {
		Assert.isTrue(entity != null , "Preencha os dados.");
		
		if (StringUtils.isNullOrEmpty(entity.getNome())) 
			return dao.listar();
		else
			return dao.listar(entity.getNome());
	}		
	
	@Override
	public FormaPagamentoEntity salvar(FormaPagamentoEntity entity) {
		entity.setDataAtualizacao(new Date());
		return dao.saveAndFlush(entity);
	}
	
	@Override
	public void cancelar(FormaPagamentoEntity entity) {
		Integer idUsuarioAlteracao = entity.getIdUsuarioAlteracao();
		FormaPagamentoEntity obj = dao.obter(entity.getId());

		if (obj != null) {
			obj.setIdUsuarioAlteracao(idUsuarioAlteracao);
			obj.setCancelado(true);
			dao.saveAndFlush(obj);
		}
	}

}
