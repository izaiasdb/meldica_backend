package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.dto.relatorios.PagarReceberDto;
import br.com.meldica.model.entity.publico.PagarReceberEntity;

public interface PagarReceberService {
	PagarReceberEntity obter(Integer id);

	List<PagarReceberEntity> listar();

	void salvar(PagarReceberEntity pagarReceber);

	List<PagarReceberEntity> listar(PagarReceberEntity pagarReceber);
	
	List<PagarReceberEntity> listarPorOrdemServicoForma(Integer idOsForma);

	void pagar(PagarReceberEntity pagarReceber);

	void excluir(PagarReceberEntity pagarReceber);

	List<PagarReceberDto> listarRelatorio(PagarReceberDto filtro);

	List<PagarReceberEntity> listarPorOrdemServico(Integer idOrdemServico);
}
