package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ClienteEnderecoEntity;
import br.com.meldica.model.entity.publico.ClienteEntity;

public interface ClienteEnderecoService {
	/**
	 * Listaro endereço pelo sequencial do cliente
	 * @param idCliente
	 * @return listarPorCliente
	 */
	List<ClienteEnderecoEntity> listarPorCliente(Integer idCliente);
	
	void salvar(ClienteEntity cliente);

	List<ClienteEnderecoEntity> listar();
}
