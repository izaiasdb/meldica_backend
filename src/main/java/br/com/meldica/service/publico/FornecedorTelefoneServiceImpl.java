package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.FornecedorTelefoneDao;
import br.com.meldica.model.entity.publico.FornecedorEntity;
import br.com.meldica.model.entity.publico.FornecedorTelefoneEntity;

@Service
public class FornecedorTelefoneServiceImpl implements FornecedorTelefoneService {

	@Autowired
	FornecedorTelefoneDao dao;

	@Override
	public List<FornecedorTelefoneEntity> listarPorFornecedor(Integer idFornecedor) { 
		return dao.listarPorFornecedor(idFornecedor);
	}

	@Override
	@Transactional
	public void salvar(FornecedorEntity cliente) {
		List<FornecedorTelefoneEntity> newList = cliente.getFornecedorTelefoneList();
		
		if(newList != null && !newList.isEmpty()) {
			List<FornecedorTelefoneEntity> oldList = listarPorFornecedor(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(FornecedorTelefoneEntity telefone : newList) {
				telefone.setIdFornecedor(cliente.getId());
				
				if(Objects.isNull(telefone.getId())) {
					telefone.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					telefone.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				telefone.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				telefone.setDataAtualizacao(now);
				dao.save(telefone);
			}
		}
	}
	
}
