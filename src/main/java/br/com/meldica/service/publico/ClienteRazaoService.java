package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteRazaoEntity;

public interface ClienteRazaoService {
	ClienteRazaoEntity obter(Integer id);
	
	List<ClienteRazaoEntity> listarPorCliente(Integer idCliente);
	
	void salvar(ClienteEntity cliente);

	List<ClienteRazaoEntity> listar();
}
