package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.ClienteRazaoDao;
import br.com.meldica.model.entity.publico.ClienteEntity;
import br.com.meldica.model.entity.publico.ClienteRazaoEntity;

@Service
public class ClienteRazaoServiceImpl implements ClienteRazaoService {

	@Autowired
	ClienteRazaoDao dao;

	@Override
	public List<ClienteRazaoEntity> listarPorCliente(Integer idCliente) { 
		return dao.listarPorCliente(idCliente);
	}
	
	@Override
	public List<ClienteRazaoEntity> listar() { 
		return dao.findAll();
	}

	@Override
	@Transactional
	public void salvar(ClienteEntity cliente) {
		List<ClienteRazaoEntity> newList = cliente.getClienteRazaoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<ClienteRazaoEntity> oldList = listarPorCliente(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(ClienteRazaoEntity endereco : newList) {
				endereco.setIdCliente(cliente.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}

	@Override
	public ClienteRazaoEntity obter(Integer id) {
		return dao.obter(id);
	}
	
}
