package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.FuncionarioTelefoneDao;
import br.com.meldica.model.entity.publico.FuncionarioEntity;
import br.com.meldica.model.entity.publico.FuncionarioTelefoneEntity;

@Service
public class FuncionarioTelefoneServiceImpl implements FuncionarioTelefoneService {

	@Autowired
	FuncionarioTelefoneDao dao;

	@Override
	public List<FuncionarioTelefoneEntity> listarPorFuncionario(Integer idFuncionario) { 
		return dao.listarPorFuncionario(idFuncionario);
	}

	@Override
	@Transactional
	public void salvar(FuncionarioEntity funcionario) {
		List<FuncionarioTelefoneEntity> newList = funcionario.getFuncionarioTelefoneList();
		
		if(newList != null && !newList.isEmpty()) {
			List<FuncionarioTelefoneEntity> oldList = listarPorFuncionario(funcionario.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(FuncionarioTelefoneEntity telefone : newList) {
				telefone.setIdFuncionario(funcionario.getId());
				
				if(Objects.isNull(telefone.getId())) {
					telefone.setDataInclusao(now);
				}
				
				if(!Objects.isNull(funcionario.getIdUsuarioInclusao())) {
					telefone.setIdUsuarioInclusao(funcionario.getIdUsuarioInclusao());
				}
				
				telefone.setIdUsuarioAlteracao(funcionario.getIdUsuarioAlteracao());
				telefone.setDataAtualizacao(now);
				dao.save(telefone);
			}
		}
	}
	
}
