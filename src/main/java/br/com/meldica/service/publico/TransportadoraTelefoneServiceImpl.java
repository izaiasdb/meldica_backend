package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.TransportadoraTelefoneDao;
import br.com.meldica.model.entity.publico.TransportadoraEntity;
import br.com.meldica.model.entity.publico.TransportadoraTelefoneEntity;

@Service
public class TransportadoraTelefoneServiceImpl implements TransportadoraTelefoneService {

	private @Autowired TransportadoraTelefoneDao dao;

	@Override
	public List<TransportadoraTelefoneEntity> listarPorTransportadora(Integer idTransportadora) { 
		return dao.listarPorTransportadora(idTransportadora);
	}

	@Override
	@Transactional
	public void salvar(TransportadoraEntity cliente) {
		List<TransportadoraTelefoneEntity> newList = cliente.getTransportadoraTelefoneList();
		
		if(newList != null && !newList.isEmpty()) {
			List<TransportadoraTelefoneEntity> oldList = listarPorTransportadora(cliente.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(TransportadoraTelefoneEntity telefone : newList) {
				telefone.setIdTransportadora(cliente.getId());
				
				if(Objects.isNull(telefone.getId())) {
					telefone.setDataInclusao(now);
				}
				
				if(!Objects.isNull(cliente.getIdUsuarioInclusao())) {
					telefone.setIdUsuarioInclusao(cliente.getIdUsuarioInclusao());
				}
				
				telefone.setIdUsuarioAlteracao(cliente.getIdUsuarioAlteracao());
				telefone.setDataAtualizacao(now);
				dao.save(telefone);
			}
		}
	}
	
}
