package br.com.meldica.service.publico;

import java.util.List;

import br.com.meldica.model.dto.publico.FichaDto;
import br.com.meldica.model.entity.publico.ClienteEntity;

public interface ClienteService {
	List<ClienteEntity> listar();

	ClienteEntity salvar(ClienteEntity cliente);

	List<ClienteEntity> listar(ClienteEntity cliente);

	ClienteEntity obter(Integer id);
	
	ClienteEntity obterFicha(Integer id);
//	ClienteEntity obter(FiltroFichaDto filtro);

//	ClienteEntity obterFicha(FichaDto filtro);

	void cancelar(ClienteEntity cliente);
}
