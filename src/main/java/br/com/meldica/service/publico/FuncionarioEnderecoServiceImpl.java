package br.com.meldica.service.publico;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.meldica.model.dao.publico.FuncionarioEnderecoDao;
import br.com.meldica.model.entity.publico.FuncionarioEnderecoEntity;
import br.com.meldica.model.entity.publico.FuncionarioEntity;

@Service
public class FuncionarioEnderecoServiceImpl implements FuncionarioEnderecoService {

	@Autowired
	FuncionarioEnderecoDao dao;

	@Override
	public List<FuncionarioEnderecoEntity> listarPorFuncionario(Integer idFuncionario) { 
		return dao.listarPorFuncionario(idFuncionario);
	}

	@Override
	@Transactional
	public void salvar(FuncionarioEntity funcionario) {
		List<FuncionarioEnderecoEntity> newList = funcionario.getFuncionarioEnderecoList();
		
		if(newList != null && !newList.isEmpty()) {
			List<FuncionarioEnderecoEntity> oldList = listarPorFuncionario(funcionario.getId());
			oldList.removeAll(newList);
			dao.deleteAll(oldList);

			Date now = new Date();
			
			for(FuncionarioEnderecoEntity endereco : newList) {
				endereco.setIdFuncionario(funcionario.getId());
				
				if(Objects.isNull(endereco.getId())) {
					endereco.setDataInclusao(now);
				}
				
				if(!Objects.isNull(funcionario.getIdUsuarioInclusao())) {
					endereco.setIdUsuarioInclusao(funcionario.getIdUsuarioInclusao());
				}
				
				endereco.setIdUsuarioAlteracao(funcionario.getIdUsuarioAlteracao());
				endereco.setDataAtualizacao(now);
				dao.save(endereco);
			}
		}
	}
	
}
