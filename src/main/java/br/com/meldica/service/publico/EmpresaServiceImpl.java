package br.com.meldica.service.publico;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.meldica.model.dao.publico.EmpresaDao;
import br.com.meldica.model.entity.publico.EmpresaEntity;
import br.com.meldica.utils.StringUtils;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired EmpresaDao dao;

	@Override
	public List<EmpresaEntity> listar() {
		return dao.findAll();
	}
	
	@Override
	public List<EmpresaEntity> listar(EmpresaEntity empresa) {
		Assert.isTrue(empresa != null , "Preencha os dados.");
		
		List<EmpresaEntity> list = new ArrayList<EmpresaEntity>();
		
		if (StringUtils.isNullOrEmpty(empresa.getNome())) {
			list = dao.listar();
		} else
		{
			list = dao.listar(empresa.getNome());
		}
		
//		for (EmpresaEntity item : list) {
//			item.setEmpresaEnderecoList(empresaEnderecoService.listarPorEmpresa(item.getId()));
//			item.setEmpresaTelefoneList(empresaTelefoneService.listarPorEmpresa(item.getId()));
//			item.setEmpresaTabelaPrecoList(empresaTabelaPrecoService.listarPorEmpresa(item.getId()));
//			item.setEmpresaRazaoList(empresaRazaoService.listarPorEmpresa(item.getId()));
//		}
		
		return list;
	}	

	@Override
	public EmpresaEntity salvar(EmpresaEntity empresa) {
		if (empresa.getId() == null){
			empresa.setDataInclusao(new Date());
		}		
		
		empresa.setDataAtualizacao(new Date());
		
//		List<EmpresaEnderecoEntity> empresaEnderecoList = empresa.getEmpresaEnderecoList();
//		List<EmpresaTelefoneEntity> empresaTelefoneList = empresa.getEmpresaTelefoneList();
//		List<EmpresaTabelaPrecoEntity> empresaTabelaPrecoList = empresa.getEmpresaTabelaPrecoList();
//		List<EmpresaRazaoEntity> empresaRazaoList = empresa.getEmpresaRazaoList();
//		
		empresa = dao.saveAndFlush(empresa);
		
//		empresa.setEmpresaEnderecoList(empresaEnderecoList);
//		empresa.setEmpresaTelefoneList(empresaTelefoneList);
//		empresa.setEmpresaRazaoList(empresaRazaoList);
//		empresa.setEmpresaTabelaPrecoList(empresaTabelaPrecoList);
//		empresaEnderecoService.salvar(empresa);
//		empresaTelefoneService.salvar(empresa);
//		empresaRazaoService.salvar(empresa);
//		empresaTabelaPrecoService.salvar(empresa);
		
		return empresa;
	}


}
