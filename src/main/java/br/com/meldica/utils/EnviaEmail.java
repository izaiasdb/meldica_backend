package br.com.meldica.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.validator.routines.EmailValidator;

import com.sun.mail.smtp.SMTPTransport;

import br.com.meldica.model.entity.acesso.UsuarioEntity;

public class EnviaEmail {

	public static final String USUARIO = "idbvenda@gmail.com";//"sistemas.sap@sap.ce.gov.br";
	public static final String SENHA = "Idb210184";//"Sap@2020";
	public static final String SMTP_HOST = "172.18.4.20";
	public static final String SMTP_PORT = "25";
	public static final boolean HABILITA_DEBUG = false;

	public void enviarEmail(List<String> destinatarios, String assunto, String texto) {
		new Thread() {

			@Override
			public void run() {
				try {
					Properties props = new Properties();
					/** Parametros de conex�o com servidor */
					props.setProperty("mail.smtps.host", SMTP_HOST);
					// props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
					props.setProperty("mail.smtp.socketFactory.fallback", "false");
					props.setProperty("mail.smtp.port", SMTP_PORT);
					props.setProperty("mail.smtp.socketFactory.port", SMTP_PORT);
					props.setProperty("mail.smtps.auth", "true");
					props.setProperty("mail.mime.charset", "UTF-8");

					Session session = Session.getInstance(props, null);
					/** Ativa Debug para sess�o somente ativar em desenvolvimento */
					session.setDebug(HABILITA_DEBUG);

					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(USUARIO)); // Remetente

					// Destinat�rio(s)
					EmailValidator validator = EmailValidator.getInstance();
					for (String d : destinatarios) {
						if (validator.isValid(d)) {
							message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(d));
						}
					}
					message.setSubject(assunto);// Assunto
					message.setText(texto);

					message.setContent(texto, "text/html; charset=UTF-8");

					/** M�todo para enviar a mensagem criada */
					// Transport.send(message);

					SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

					t.connect(SMTP_HOST, USUARIO, SENHA);
					t.sendMessage(message, message.getAllRecipients());
					t.close();

				} catch (MessagingException e) {
					e.printStackTrace();

					throw new RuntimeException(e);

				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}.start();
	}
	
	public void enviarCodigoVerificacaoEsqueceuSenha(UsuarioEntity usuario) {
		List<String> destinatarios = new ArrayList<>();
		destinatarios.add(usuario.getEmail());
		
		StringBuilder body = new StringBuilder();
		body.append(" Foi <b>gerado um código de verificação, após ser informado esquecimento de senha do acesso ao SISPEN.</b>");
		body.append(" Caso tenha realmente solicitado, informe o código gerado abaixo no SISPEN: ");
		body.append(" </br> ");
		body.append(" Código: <b>" + usuario.getCodigoVerificacao() + "</b> ");
		body.append(" </br> ");
		body.append(" <p> ");
		body.append(" Observação: <b>Sua nova senha deverá ser informada após a confirmação do código gerado.</b> ");
		body.append(" </p> ");

		enviarEmail(destinatarios , "Código de verificação para acesso ao SISPEN", body.toString());
	}
	
	public void enviarCodigoVerificacaoCadastro(UsuarioEntity usuario) {
		List<String> destinatarios = new ArrayList<>();
		destinatarios.add(usuario.getEmail());
		
		StringBuilder body = new StringBuilder();
		body.append(" Foi <b>gerado um código de verificação, que será utilizado para cadastrar a sua senha acesso ao SISPEN.</b> ");
		body.append(" </br> ");
		body.append(" Código: <b>" + usuario.getCodigoVerificacao() + "</b> ");
		body.append(" </br> ");
		body.append(" <p> ");
		body.append(" Observação: <b>Sua nova senha deverá ser informada após a confirmação do código gerado.</b> ");
		body.append(" </p> ");

		enviarEmail(destinatarios , "Código de verivicação do SISPEN", body.toString());
	}
	
//	public void enviarEmailSenha(UsuarioEntity usuario) {
//		List<String> destinatarios = new ArrayList<>();
//		destinatarios.add(usuario.getEmail());
//		
//		StringBuilder body = new StringBuilder();
//		body.append(" Foi <b>criado um usuário de acesso ao SISPEN</b> ");
//		body.append(" com o seu e-mail. Clique no link abaixo, e informe os dados para o seu primeiro acesso ao sistema: ");
//		body.append(" </br> ");
//		//body.append(" <b><a href=\"http://172.18.4.10:8081/alterarSenha\" target=\"_blank\">Link de acesso</a> </b> ");
//		body.append(" <b><a href=\"http://localhost:8081/alterarSenha\" target=\"_blank\">Link de acesso</a> </b> ");
//		body.append(" </br> ");
//		body.append(" Login: <b>" + usuario.getLogin() + "</b> ");
//		body.append(" </br> ");
//		body.append(" Senha: <b>" + usuario.getSenha() + "</b> ");
//		body.append(" <p> ");
//		body.append(" Observação: <b>Sua senha deve ser redefinida no seu primeiro acesso.</b> ");
//		body.append(" </p> ");
//
//		enviarEmail(destinatarios , "Sua senha de acesso ao SISPEN", body.toString());
//	}

}