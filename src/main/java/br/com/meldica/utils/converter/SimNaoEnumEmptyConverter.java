package br.com.meldica.utils.converter;

import javax.persistence.AttributeConverter;

import br.com.meldica.model.enums.SimNaoEnum;

public class SimNaoEnumEmptyConverter implements AttributeConverter<SimNaoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SimNaoEnum sn) {
		if(sn == null) {
			return null;
		}
		return sn.name();
	}

	@Override
	public SimNaoEnum convertToEntityAttribute(String sn) {
		if(sn == null || sn.isEmpty()) {
			return null;
		}
		return SimNaoEnum.valueOf(sn);
	}

}
