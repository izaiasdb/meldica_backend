package br.com.meldica.utils.converter;

import javax.persistence.AttributeConverter;

import br.com.meldica.model.enums.SexoEnum;

public class SexoEnumEmptyConverter implements AttributeConverter<SexoEnum, String> {

	@Override
	public String convertToDatabaseColumn(SexoEnum sexo) {
		if(sexo == null) {
			return null;
		}
		return sexo.name();
	}

	@Override
	public SexoEnum convertToEntityAttribute(String sexo) {
		if(sexo == null || sexo.isEmpty()) {
			return null;
		}
		return SexoEnum.valueOf(sexo);
	}

}
