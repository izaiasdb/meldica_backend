package br.com.meldica.utils.converter;

import javax.persistence.AttributeConverter;

import br.com.meldica.model.enums.PfPjEnum;

public class PfPjEnumEmptyConverter implements AttributeConverter<PfPjEnum, String> {

	@Override
	public String convertToDatabaseColumn(PfPjEnum pfPj) {
		if(pfPj == null) {
			return null;
		}
		return pfPj.name();
	}

	@Override
	public PfPjEnum convertToEntityAttribute(String pfPj) {
		if(pfPj == null || pfPj.isEmpty()) {
			return null;
		}
		return PfPjEnum.valueOf(pfPj);
	}

}
