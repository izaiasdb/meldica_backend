package br.com.meldica.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {
	public final static String[] diaSemana = { "Domingo", "Segunda-Feira", "Terca-Feira", "Quarta-Feira",
			"Quinta-Feira", "Sexta-Feira", "Sabado" };
	private static final String DD_MM_YYYY = "dd/MM/yyyy";	
	public final static long HORAS_8_EM_SEGS = 28800; //Mais de 8 horas de conexão (Segundos)
	public final static long MINUTOS_10_EM_SEGS = 600; //600 em segundos (10min) 
	
//	public static int getIdade(Date date) throws Exception {
//		return getIdade(dateToCalendar(date));
//	}
//	
//	public static int getIdade(Calendar date) throws Exception {
//        Calendar today = Calendar.getInstance();
// 
//        int curYear = today.get(Calendar.YEAR);
//        int dobYear = date.get(Calendar.YEAR);
// 
//        int age = curYear - dobYear;
// 
//        // if dob is month or day is behind today's month or day
//        // reduce age by 1
//        int curMonth = today.get(Calendar.MONTH);
//        int dobMonth = date.get(Calendar.MONTH);
//        if (dobMonth > curMonth) { // this year can't be counted!
//            age--;
//        } else if (dobMonth == curMonth) { // same month? check for day
//            int curDay = today.get(Calendar.DAY_OF_MONTH);
//            int dobDay = date.get(Calendar.DAY_OF_MONTH);
//            if (dobDay > curDay) { // this year can't be counted!
//                age--;
//            }
//        }
// 
//        return age;
//    }
	
	public synchronized static boolean isBlank(Date date) {
		boolean b = isNull(date);

		if (b) {
			return b;
		}

		b = date.equals("");

		return b;
	}

	public synchronized static boolean isNotBlank(Date date) {
		boolean b = isNotNull(date);

		if (!b) {
			return b;
		}

		b = date.equals("");

		return !b;
	}

	public synchronized static boolean isNotNull(Date date) {
		return date != null;
	}

	public synchronized static boolean isNull(Date date) {
		return date == null;
	}

	public synchronized static String parseString(GregorianCalendar dateSync) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = sdf.format(dateSync.getTime());

		return s;
	}

	public synchronized static Long diffBetweenNowAndAnyDate(Long timeInMillis) {
		long diff = new Date().getTime() - timeInMillis;

		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public synchronized static String toString(long date, String mask) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(date);

		SimpleDateFormat sdf;

//		if (StringUtils.isNotBlank(mask)) {
//			sdf = new SimpleDateFormat(mask);
//		} else {
//			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		}

//		String s = sdf.format(gc.getTime());

//		return s;
		return null;
	}
	
	public synchronized static Long toLong(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);

		return gc.getTimeInMillis();
	}

	public synchronized static Long toLong(String s) {
		String[] array = s.split("/");

		int day = Integer.parseInt(array[0]);
		int month = Integer.parseInt(array[1]);
		int year = Integer.parseInt(array[2]);

		Calendar c = Calendar.getInstance();
		c.set(year, month, day);

		return c.getTimeInMillis();
	}
	
	public static String getTimeFromDate(Date date) {
		if (date != null) {
			try {
				SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
			    return localDateFormat.format(date);
			} catch (Exception e) {
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * http://stackoverflow.com/questions/226910/how-to-sanity-check-a-date-in-java
	 * http://stackoverflow.com/questions/20231539/java-check-the-date-format-of-current-string-is-according-to-required-format-or
	 * @param date
	 * @param format
	 * @return
	 */
	public static boolean isDateValid(String date, String format) {
	        try {
	            DateFormat df = new SimpleDateFormat(format);
	            df.setLenient(false);
	            df.parse(date);
	            return true;
	        } catch (ParseException e) {
	            return false;
	        }
	}
	
	/**
	 * Converte uma String para um objeto Date. Caso a String seja vazia ou
	 * nula, retorna null - para facilitar em casos onde formulárrios podem ter
	 * campos de datas vazios.
	 * 
	 * @param data String no formato dd/MM/yyyy a ser formatada
	 * @return Date Objeto Date ou null caso receba uma String vazia ou nula
	 * @throws Exception Caso a String esteja no formato errado
	 * @author izaias.barreto
	 * @see http://www.guj.com.br/java/41935-converter-string-para-date
	 */
	public static Date stringToDate(String data, boolean hora) throws Exception {
		if (data == null || data.equals(""))
			return null;

		Date date = null;

		try {
			DateFormat formatter = null;

			/*
			 * kk 24hrs
			 * hh 12hrs
			 */
			if (hora)
				formatter = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
			else
				formatter = new SimpleDateFormat("dd/MM/yyyy");

			date = (java.util.Date) formatter.parse(data);
		} catch (ParseException e) {
			throw e;
		}

		return date;
	}
	
	public static Date stringToDate(String data, DateFormat formatter) throws Exception {
		if (data == null || data.equals(""))
			return null;

		Date date = null;

		try {
//			DateFormat formatter = null;
//
//			/*
//			 * kk 24hrs
//			 * hh 12hrs
//			 */
//			if (hora)
//				formatter = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
//			else
//				formatter = new SimpleDateFormat("dd/MM/yyyy");

			date = (java.util.Date) formatter.parse(data);
		} catch (ParseException e) {
			throw e;
		}

		return date;
	}
	
	public static Calendar stringToDateYYYYMMDD(String data) throws Exception {
		if (data == null || data.equals(""))
			return null;

		Date date = null;
		Calendar dateConvert = null;
		
		try {
			DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			date = (java.util.Date) formatter.parse(data);
			dateConvert = dateToCalendar(date);
		} catch (ParseException e) {
			throw e;
		}

		return dateConvert;
	}
	
	/**
	 * Converte uma string em um objeto calendar
	 * 
	 * @param data
	 * @param hora
	 * @return
	 * @throws Exception
	 */
	public static Calendar stringToCalendar(String data, boolean hora) throws Exception {
		if (data == null || data.equals(""))
			return null;

		Date date = null;

		Calendar cal = null;

		try {

			date = stringToDate(data, hora);
			cal = dateToCalendar(date);
		} catch (ParseException e) {
			throw e;
		}

		return cal;
	}
	
	public static String calendarToString(Calendar calendar, boolean hora) {
		SimpleDateFormat s = null;

		if (hora)
			s = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		else
			s = new SimpleDateFormat("dd/MM/yyyy");

		//Retornando 24:00:00 -> 00:00:00
		String date = s.format(calendar.getTime()).replace("24:00:00", "00:00:00"); //Não funciona no caso de 24:10:00
		
		if (hora && date.substring(11, 13).equals("24")) //19/10/2016 24:12:00
			date = date.substring(0, 11) + "00" + date.substring(13, 19);
		
		return date;
	}

	/**
	 * Converte um objeto date em uma string
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToString(Date date, String format) {
		if (date == null) {
			return null;
		}

		TimeZone.setDefault(TimeZone.getTimeZone("America/Fortaleza"));
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.format(date);
	}
	
	public static String dateToString(Date date) {
		if (date == null) {
			return null;
		}

		TimeZone.setDefault(TimeZone.getTimeZone("America/Fortaleza"));
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return simpleDateFormat.format(date);
	}

	public static String calendarToStringYYYYMMDDHH24MISS(Calendar date) {
		return String.valueOf(date.get(Calendar.YEAR))
				+ StringUtils.lpad(String.valueOf(date.get(Calendar.MONTH) + 1), 2, '0')
				+ StringUtils.lpad(String.valueOf(date.get(Calendar.DAY_OF_MONTH)), 2, '0')
				+ StringUtils.lpad(String.valueOf(date.get(Calendar.HOUR_OF_DAY)), 2, '0')
				+ StringUtils.lpad(String.valueOf(date.get(Calendar.MINUTE)), 2, '0')
				+ StringUtils.lpad(String.valueOf(date.get(Calendar.SECOND)), 2, '0');
	}

	public static String getNowExtenseDate() {
		String dataAtual = "";

		GregorianCalendar c = new GregorianCalendar();
		c.setTimeZone(TimeZone.getTimeZone("GMT-03:00"));

		// Data
		dataAtual = diaSemana[c.get(Calendar.DAY_OF_WEEK) - 1] + " - "
				+ StringUtils.lpad(String.valueOf(c.get(Calendar.DAY_OF_MONTH)), 2, '0') + "/"
				+ StringUtils.lpad(String.valueOf(c.get(Calendar.MONTH) + 1), 2, '0') + "/" + c.get(Calendar.YEAR);

		return dataAtual;
	}
	
	public static String getExtenseDate(Calendar cal) {
		return diaSemana[cal.get(Calendar.DAY_OF_WEEK) - 1] + " - "
				+ StringUtils.lpad(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)), 2, '0') + "/"
				+ StringUtils.lpad(String.valueOf(cal.get(Calendar.MONTH) + 1), 2, '0') + "/" + cal.get(Calendar.YEAR);
	}

	/**
	 * Converte um objeto Calendar em um objeto date
	 * 
	 * @author izaias.barreto
	 * @param date
	 * @return
	 */
	public static Calendar dateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal;
	}
	
	/**
	 * Converte um objeto date em um objeto Calendar
	 * 
	 * @author izaias.barreto
	 * @param date
	 * @return
	 */
	public static Date calendarToDate(Calendar calendar) {
		return new Date(calendar.getTimeInMillis());
	}
//	public Date calendarToDate(Calendar calendar) {
//	return calendar.getTime();
//}

	/**
	 * Alterar o tempo do objeto calendar
	 * 
	 * @param date
	 * @param time
	 * @return
	 * @throws Exception
	 */
	public static Calendar setTime(Calendar date, String time) throws Exception {
		if (date == null)
			throw new Exception("Data precisa ser informada");
		
		if (StringUtils.isNullOrEmpty(time))
			throw new Exception("Tempo precisa ser informado");
		
		try {
			String valores[] = time.split(":");

//			dtInicial = new GregorianCalendar(2015, Calendar.FEBRUARY, 2);
			date.set(Calendar.HOUR_OF_DAY, Integer.parseInt(valores[0]));
			date.set(Calendar.MINUTE, Integer.parseInt(valores[1]));
			date.set(Calendar.SECOND, Integer.parseInt(valores[2]));
		} catch (Exception e) {
			throw new Exception("Tempo inválido ser informado");
		}

		return date;
	}
	
	public static Calendar setTime(Calendar date, boolean increase, int tipo, int tempo) throws Exception {
		if (date == null)
			throw new Exception("Data precisa ser informada");
		
		if (tipo == 0)
			throw new Exception("Tipo precisa ser informado");
		
		if (tempo == 0)
			throw new Exception("Tempo precisa ser informado");		
		
		try {
			if (tipo == Calendar.MONTH)
				if (increase)
					date.set(Calendar.MONTH, date.get(Calendar.MONTH) + tempo);
				else
					date.set(Calendar.MONTH, date.get(Calendar.MONTH) - tempo);
			else if (tipo == Calendar.DAY_OF_MONTH)
				if (increase)
					date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) + tempo);
				else
					date.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH) - tempo);
			else if (tipo == Calendar.HOUR_OF_DAY)
				if (increase)
					date.set(Calendar.HOUR_OF_DAY, date.get(Calendar.HOUR_OF_DAY) + tempo);
				else
					date.set(Calendar.HOUR_OF_DAY, date.get(Calendar.HOUR_OF_DAY) - tempo);
			else if (tipo == Calendar.MINUTE)
				if (increase)
					date.set(Calendar.MINUTE, date.get(Calendar.MINUTE) + tempo);
				else
					date.set(Calendar.MINUTE, date.get(Calendar.MINUTE) - tempo);
			else if (tipo == Calendar.SECOND)
				if (increase)
					date.set(Calendar.SECOND, date.get(Calendar.SECOND) + tempo);
				else
					date.set(Calendar.SECOND, date.get(Calendar.SECOND) - tempo);
			else
				throw new Exception("Tipo inválido!");
		} catch (Exception e) {
			throw new Exception("Tempo inválido!");
		}

		return date;
	}
	

	/**
	 * Retorna a diferença em dias entre duas datas
	 * @see http://stackoverflow.com/questions/20165564/calculating-days-between-two-
	 * dates-with-in-java
	 * 
	 * @param dtInicial
	 * @param dtFinal
	 * @return
	 */
	public static long diffDays(Calendar dtInicial, Calendar dtFinal) {
		Date date1 = calendarToDate(dtInicial);
		Date date2 = calendarToDate(dtFinal);

		long diff = date2.getTime() - date1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Retorna a diferença em dias entre duas datas
	 * 
	 * @param dtInicial
	 * @param dtFinal
	 * @return
	 */
	public static long diffDays(Date dtInicial, Date dtFinal) {
		String dtInis = dateToString(dtInicial, "dd/MM/yyyy" + " 00:00:00");
		String dtFims = dateToString(dtFinal, "dd/MM/yyyy" + " 00:00:00");
		
		Date dtIni = null;
		Date dtFim = null;
		
		try {
			dtIni = stringToDate(dtInis, true);
			dtFim = stringToDate(dtFims, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//long diff = dtFinal.getTime() - dtInicial.getTime();
		long diff = dtFim.getTime() - dtIni.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Retorna a diferença em segundos entre duas datas
	 * 
	 * @see http://stackoverflow.com/questions/20165564/calculating-days-between-two-
	 * dates-with-in-java
	 * 
	 * @param dtInicial
	 * @param dtFinal
	 * @return
	 */
	public static long diffSeconds(Calendar dtInicial, Calendar dtFinal) {
		Date d1 = calendarToDate(dtInicial);
		Date d2 = calendarToDate(dtFinal);

//		 System.out.println(calendarToString(dtInicial, true));
//		 System.out.println(calendarToString(dtFinal, true));
		return (d2.getTime() - d1.getTime()) / 1000;
	}

	public static String dateDiffTime(Calendar dtInicial, Calendar dtFinal) {
		long qtdSegundos = diffSeconds(dtInicial, dtFinal);
		long qtdMinutos = 0;
		long resto = 0;

		if (qtdSegundos > 60) {
			qtdMinutos = (qtdSegundos / 60);
			resto = (qtdSegundos % 60);

			return qtdMinutos + " min " + resto + " seg ";
		} else
			return "0 min " + qtdSegundos + " seg ";
	}

	/**
	 * http://stackoverflow.com/questions/6118922/convert-seconds-value-to-hours
	 * -minutes-seconds
	 * 
	 * @param totalSeconds
	 * @return
	 */
	public static String secondsToTime(long totalSeconds, boolean infoDays) {
		long days = totalSeconds / 86400;
		long hours = totalSeconds / 3600;
		long minutes = (totalSeconds % 3600) / 60;
		long seconds = totalSeconds % 60;
		
		if (infoDays) {
			totalSeconds = totalSeconds % 86400;
			hours = totalSeconds / 3600;
			minutes = (totalSeconds % 3600) / 60;
			seconds = totalSeconds % 60;			
		}

		if (infoDays)
			return String.format("%01d dias %02d:%02d:%02d", days, hours, minutes, seconds);
		else
			return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}
	
	/**
	 * Retorna o início do dia atual
	 * @return
	 */
	public static Calendar getInicioDiaAtual() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		return calendar;
	}
	
	public static Date getPrimeiroDiaMes() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);		
		return calendar.getTime();
	}
	
	public static Date getUltimoDiaMes() {
		Calendar calendar = Calendar.getInstance();
		int lastDate = calendar.getActualMaximum(Calendar.DATE);
		calendar.set(Calendar.DATE, lastDate);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static Date getUltimoDiaMes(final Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int lastDate = calendar.getActualMaximum(Calendar.DATE);
		calendar.set(Calendar.DATE, lastDate);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static Integer getIdade(final Date date) {
		Calendar dateBirth = Calendar.getInstance();
		dateBirth.setTime(date);
		Calendar today = Calendar.getInstance(); 
		
		Integer diferencaMes = today.get(Calendar.MONTH) - dateBirth.get(Calendar.MONTH);  
	    Integer diferencaDia = today.get(Calendar.DAY_OF_MONTH) - dateBirth.get(Calendar.DAY_OF_MONTH);  
	    Integer age = (today.get(Calendar.YEAR) - dateBirth.get(Calendar.YEAR));  
	  
	    if(diferencaMes < 0  || (diferencaMes == 0 && diferencaDia < 0)) {  
	        age--;  
	    }  
	      
	    return age; 
	}
	
	public static Integer getIdade(final Calendar date) {
		Calendar today = Calendar.getInstance(); 
		
		Integer diferencaMes = today.get(Calendar.MONTH) - date.get(Calendar.MONTH);  
	    Integer diferencaDia = today.get(Calendar.DAY_OF_MONTH) - date.get(Calendar.DAY_OF_MONTH);  
	    Integer age = (today.get(Calendar.YEAR) - date.get(Calendar.YEAR));  
	  
	    if(diferencaMes < 0  || (diferencaMes == 0 && diferencaDia < 0)) {  
	        age--;  
	    }  
	      
	    return age; 
	}
	
	public static String formatDayMonthYear(Date date) {
		String dateFormat = null;
		if (date !=null) {
			SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);
			dateFormat = sdf.format(date);
			return dateFormat;
		}
		return dateFormat;
	}
	
	public static String formatDayMonthYear(Calendar calendar) {
		String dateFormat = null;
		
		if (calendar !=null) {
			Date date = calendarToDate(calendar);
			return formatDayMonthYear(date);
		}
		
		return dateFormat;
	}
	
	private static int calcularIdade(Date dataNasc) {
		GregorianCalendar dateOfBirth = new GregorianCalendar();
		dateOfBirth.setTime(dataNasc);

		// Cria um objeto calendar com a data atual
		Calendar today = Calendar.getInstance();

		// Obtém a idade baseado no ano
		int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);

		dateOfBirth.add(Calendar.YEAR, age);

		// se a data de hoje é antes da data de Nascimento, então diminui 1(um)
		if (today.before(dateOfBirth)) {
			age--;
		}
		
		return age;
	}
	
	public static Date adicionarAnos(int qtdAnos){
		Calendar cal = Calendar.getInstance();
		Date f = new Date();
		cal.setTime(f);
		cal.add(Calendar.YEAR, qtdAnos);
		return cal.getTime();
	}

	//https://www.guj.com.br/t/mes-por-extenso/52360/2
	public static String getMes(Date date){
//		String s = "31/01/2009";
//		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
//		Date dt = df.parse (s);
		DateFormat df2 = new SimpleDateFormat ("MMMMM", new Locale ("pt", "BR"));
		return df2.format (date); // "Janeiro"
	}
	
	//https://www.guj.com.br/t/mes-por-extenso/52360/2
	public static String getMesAno(Date date){
//		String s = "31/01/2009";
//		DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");
//		Date dt = df.parse (s);
		Locale local = new Locale("pt","BR");
		DateFormat dateFormat = new SimpleDateFormat("MMMM 'de' yyyy",local); 
		return dateFormat.format(date);
	}	
	
	public static void main(String[] args) {
		Date date = new Date();
		Date date2 = new Date();
		System.out.println();
		String hora = getTimeFromDate(date);
		String dateStr = dateToString(date2);
		
		try {
			System.out.println(stringToDate(dateStr+ " " + hora, true));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		GregorianCalendar c = new GregorianCalendar(2000, 8, 19, 11, 52, 58);
//
//		System.out.println(c.getTime().getTime());
//		c.setTimeInMillis(new Long("969375178000"));
//		System.out.println(c.getTime());
		
//		System.out.println(getPrimeiroDiaMes());
//		System.out.println(getUltimoDiaMes());

		// "2000-09-19 11:52:58.056"

//		System.out.println(new Date(2000, 8, 19, 11, 52, 58).getTime());
		
//		String data = "15021972";
//		data = data.substring(0, 2) + "/" + data.substring(2, 4) + "/" + data.substring(4, 8);
//		Calendar dtTransformado = Calendar.getInstance();
//		
//		try {
//			dtTransformado = stringToCalendar(data, false);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		System.err.println(calendarToString(dtTransformado, false));
//		
//		data = StringUtils.lpad(String.valueOf(dtTransformado.get(Calendar.DAY_OF_MONTH)), 2, '0')
//		+ StringUtils.lpad(String.valueOf(dtTransformado.get(Calendar.MONTH) + 1), 2, '0')
//		+ String.valueOf(dtTransformado.get(Calendar.YEAR));		
//		
//		System.err.println(data);
		
	}
}
