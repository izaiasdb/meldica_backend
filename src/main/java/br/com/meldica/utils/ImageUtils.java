package br.com.meldica.utils;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageUtils {

//	public static byte[] getImageBytesFromUrl(String urlImage) throws IOException {
//		URL url = new URL(urlImage);
//		InputStream in = new BufferedInputStream(url.openStream());
//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		byte[] buf = new byte[1024];
//		int n = 0;
//
//		while (-1 != (n = in.read(buf))) {
//			out.write(buf, 0, n);
//		}
//		out.close();
//		in.close();
//
//		return out.toByteArray();
//	}
	
	public static byte[] getImageBytesFromUrl(String urlImage) throws IOException {
		URL url = new URL(urlImage);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;

		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
			int n;

			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}
		
		return baos.toByteArray();
	}
	
	public static Image  getImageFromUrl(String urlImage) throws IOException {
		URL url = new URL(urlImage);
	    return ImageIO.read(url);
	}
}
