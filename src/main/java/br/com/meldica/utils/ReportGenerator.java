package br.com.meldica.utils;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.Collection;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import br.com.meldica.model.enums.ExtensaoEnum;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

@Service
public class ReportGenerator {

	private @Autowired DataSource dataSource;
	
	public byte[] getReport(String jasper, Map<String, Object> params, ExtensaoEnum extensao, Collection<? extends Object> collection) throws Exception {
		byte[] content = null;
		JasperPrint jasperPrint = null;
//		JasperReport jasper = JasperCompileManager.compileReport(jasper);
		if (collection == null) {
			jasperPrint = (JasperPrint) JasperFillManager.fillReport(jasper, params, dataSource.getConnection());
		}else {
			jasperPrint = (JasperPrint) JasperFillManager.fillReport(jasper, params, new JRBeanCollectionDataSource(collection));
		}
		if (ExtensaoEnum.PDF.equals(extensao)) {
			try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
				JRPdfExporter exporter = new JRPdfExporter();				
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
				SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
				exporter.setConfiguration(configuration);
				exporter.exportReport();
				
				content = outputStream.toByteArray();				
			} catch (JRException e) {
				e.printStackTrace();
				throw new JRException(e);
			}
		} else if (ExtensaoEnum.XLS.equals(extensao)) {
			try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
				JRXlsExporter exporter = new JRXlsExporter();
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
				SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
				configuration.setOnePagePerSheet(true);
				configuration.setDetectCellType(true);
				configuration.setCollapseRowSpan(false);
				exporter.setConfiguration(configuration);
				exporter.exportReport();
				
				content = outputStream.toByteArray();		
			} catch (JRException e) {
				e.printStackTrace();
				throw new JRException(e);
			}
		}
		
		return Base64.getEncoder().encode(content);
	}
	
	 public HttpHeaders getHttpHeaders(ExtensaoEnum extensao) {
		String mediaType = "";
		if (ExtensaoEnum.PDF.equals(extensao)) {
			mediaType = "application/pdf"; 
		} else {
			mediaType = "application/vnd.ms-excel";
		}
		
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(mediaType));
        headers.add("Content-Disposition", "attachment;filename=relatorio." + extensao.getDescricao());
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return headers;
    }
	
}
