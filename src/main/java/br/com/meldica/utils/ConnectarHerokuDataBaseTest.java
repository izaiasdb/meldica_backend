package br.com.meldica.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp2.BasicDataSource;

public class ConnectarHerokuDataBaseTest {

	private static BasicDataSource connectionPool;
	
	// https://devcenter.heroku.com/articles/database-connection-pooling-with-java
	public static void main(String[] args) {
		String host = "ec2-100-25-231-126.compute-1.amazonaws.com";
		String port = "5432";
		String path = "d8spumnd4d12fj";
		String username = "anfgqpuqselqiv";
		String password = "ad48a38c418199f3fa88e93f419f05953860e914fa7a94e855bb66138943a4a8";
		
        String dbUrl = "jdbc:postgresql://" + host + ':' + port +"/" + path + "?sslmode=require";
        
//		URI dbUri = new URI(System.getenv("DATABASE_URL"));
//		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
        
		connectionPool = new BasicDataSource();

        connectionPool.setUsername(username);
        connectionPool.setPassword(password);
		connectionPool.setDriverClassName("org.postgresql.Driver");
		connectionPool.setUrl(dbUrl);
		connectionPool.setInitialSize(1);
		
		Connection connection;
		
		try {
			connection = connectionPool.getConnection();
			
			Statement stmt = connection.createStatement();
//			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS ticks (tick timestamp)");
//			stmt.executeUpdate("INSERT INTO ticks VALUES (now())");
			ResultSet rs = stmt.executeQuery("SELECT id, nome FROM acesso.usuario ");
			
			while (rs.next()) {
				System.out.println("Read from DB: " + rs.getInt("id") + "\n");
				System.out.println("Read from DB: " + rs.getString("nome") + "\n");
//			    System.out.println("Read from DB: " + rs.getTimestamp("tick") + "\n");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		 
}
