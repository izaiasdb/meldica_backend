package br.com.meldica.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CurrencyUtils {
	
	// Não usar o Double pq fica com muitas casas decimais e dá erro na conversão do truncate
	// ex. 102.55000305175781 , para converter para float
//	float totalForma = truncate(ordemServico.getFormaItemsList().stream().mapToDouble(o -> o.getValor()).sum(), 2);
//	float totalFrete = truncate(ordemServico.getTransportadoraItemsList().stream().mapToDouble(o -> o.getValorFrete()).sum(), 2);
//	
//	float totalProduto = truncate(ordemServico.getProdutoItemsList().stream().filter(o -> !o.isBonificacao())
//			.mapToDouble(o -> o.getTotal().doubleValue()).sum(), 2);
//	
//	float total = totalProduto + totalFrete; 
	//totalForma = truncate(totalForma, 2);
	
//	float totalProduto = 0.00f;
//	float totalKit = 0.00f;
//	float totalForma = 0.00f;
//	float totalFrete = 0.00f;
//	float total = 0.00f;
	public float truncate(float valor, int precisao) {
		float novoValor = 0;
		String valorS = String.valueOf(valor);
		Integer index = null;
		Integer ultNumero = 0; 
		index = valorS.indexOf(".");
		
		if (index != -1 && valorS.length() > 5) {
			try {
				//valorS = valorS.substring(0, index + 3);
				//novoValor = Float.valueOf(valorS);
				
				valorS = valorS.substring(0, index + 4);
				// Pego o último número depois 102.229
				ultNumero = Integer.valueOf(String.valueOf(valorS.charAt(valorS.length() -1)));
				DecimalFormat df = new DecimalFormat("##.##");
				
				if (ultNumero > 5) {
					//df.setRoundingMode(RoundingMode.UP);
					valorS = valorS.substring(0, index + 3);
					novoValor = Float.valueOf(valorS) + Float.valueOf("0.01");
				} else {
					//df.setRoundingMode(RoundingMode.DOWN);
					valorS = valorS.substring(0, index + 3);
					novoValor = Float.valueOf(valorS);
				}
				
//				novoValor = Float.valueOf(df.format(valorS));
			} catch (Exception e) {
				novoValor = Float.valueOf(String.valueOf(valor));
			}				
		} else {
			novoValor = valor;
			// Não funcionou
//			try {
//				DecimalFormat df = new DecimalFormat("##.##");
//				df.setRoundingMode(RoundingMode.DOWN);
//				novoValor = Float.valueOf(df.format(valor));	
//			} catch (Exception e) {
//				novoValor = Float.valueOf(String.valueOf(valor));
//			}
		}
		
		return novoValor;
	}
	
	public float truncate(Double valor, int precisao) {
		float novoValor = 0;
		String valorS = String.valueOf(valor);
		Integer index = null;
		index = valorS.indexOf(".");
		
		if (index != -1 && valorS.length() > 5) {
			try {
				valorS = valorS.substring(0, index + 3);
				novoValor = Float.valueOf(valorS);
			} catch (Exception e) {
				novoValor = valor.floatValue();
			}				
		} else {
			novoValor = valor.floatValue();;
		}
		
		return novoValor;
	}
	
	public static void main(String[] args) {
		float valor = 102.5500099999999999f;
		Integer index = null;
		String valorS = String.valueOf(valor);
		float novoValor = 0;
		
		index = valorS.indexOf(".");
		
		if (index != -1) {
			try {
				valorS = valorS.substring(0, index + 3);
				novoValor = Float.valueOf(valorS);
			} catch (Exception e) {
			}
		}
		
		BigDecimal bd = BigDecimal.valueOf(valor);
//		bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
		bd = bd.setScale(2, BigDecimal.ROUND_UP);
		
		double value = new BigDecimal(Number.class.cast(valor).toString()).doubleValue();
		
		
		System.out.println(value);
	}
}
