package br.com.meldica.utils.aspects;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Aspect
@Configuration
public class LoggingAspect {

	@Pointcut("execution(* br.com.meldica.controller.*.*(..))")
	public void controllers() {}

	@Pointcut("execution(* br.com.meldica.service.*.*(..))")
	public void services() {}

	@Pointcut("execution(* br.com.meldica.model.dao.*.*(..))")
	public void daos() {}

	private Logger logger = LoggerFactory.getLogger(this.getClass()); 

	@Before("services()")
	public void before(JoinPoint joinPoint) throws Throwable{
		String usuario = getUsuarioLogado();
		
		if(usuario != null) {
			logger.info("[EXEC] " + usuario + joinPoint.toShortString().replace("execution(", "").replace("))",")") + ", Args: " + extraiParametros(joinPoint));
		}
	}

	@AfterThrowing("controllers() || services() || daos()")
	public void onError(JoinPoint joinPoint) {
		String usuario = getUsuarioLogado();
		if(usuario != null) {
			logger.error("[ERRO] " + usuario + joinPoint.toShortString() + ", Args: " + extraiParametros(joinPoint));
		}
	}

	private String extraiParametros(JoinPoint joinPoint) {
		return Arrays.asList(joinPoint.getArgs()).stream()
												 .filter(p -> !p.getClass().getName().contains("org.springframework.security"))
												 .collect(Collectors.toList()).toString();
	}

	private String getUsuarioLogado() {
		String currentUserName = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
		    currentUserName = authentication.getName();
		}
		
		return (currentUserName != null ? currentUserName + " -> " : null);
	}
}
