package br.com.meldica.utils;

import javax.naming.NamingException;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

/**
 * Pesq: 
 * Spring Boot with datasource when testing
 * spring boot junit test jndi datasource
 * spring boot service test jndi datasource
 * 
 * Spring boot Mock
 * https://spring.io/guides/gs/testing-web/ 
 * https://www.baeldung.com/spring-mock-jndi-datasource
 * @author sap
 *
 */
public class DataSourceUtils {

	/**
	 * https://www.codota.com/code/java/classes/org.springframework.mock.jndi.SimpleNamingContextBuilder
	 * https://www.codota.com/code/java/methods/org.springframework.mock.jndi.SimpleNamingContextBuilder/activate
	 * https://www.programcreek.com/java-api-examples/?api=org.springframework.mock.jndi.SimpleNamingContextBuilder
	 * https://www.javatips.net/api/org.springframework.mock.jndi.simplenamingcontextbuilder
	 * https://www.journaldev.com/2509/java-datasource-jdbc-datasource-example
	 */
	public static void getDataSourceJNDI() {
		try {
		    SimpleNamingContextBuilder builder = SimpleNamingContextBuilder.emptyActivatedContextBuilder();
		    
		    BasicDataSource dsMeldicaDev = new BasicDataSource();
		    dsMeldicaDev.setDriverClassName("org.postgresql.Driver");
		    dsMeldicaDev.setUrl("jdbc:postgresql://localhost:5432/meldica_bkp?autoReconnect=true");
		    dsMeldicaDev.setUsername("postgres");
		    dsMeldicaDev.setPassword("210184");
//		    
//		    BasicDataSource dsMeldicaProd = new BasicDataSource();
//		    dsMeldicaProd.setDriverClassName("org.postgresql.Driver");
//		    dsMeldicaProd.setUrl("jdbc:postgresql://localhost:5432/meldica?autoReconnect=true");
//		    dsMeldicaProd.setUsername("postgres");
//		    dsMeldicaProd.setPassword("210184");

		    builder.bind("java:comp/env/jdbc/meldica", dsMeldicaDev);
//		    builder.bind("java:comp/env/jdbc/meldica", dsMeldicaProd);
		    builder.activate();
	   } catch (IllegalStateException e) {
	        e.printStackTrace();
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }	
	}
}
