package br.com.meldica.dao.dominio;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.meldica.model.dao.dominio.CargoDao;
import br.com.meldica.model.dao.dominio.CargoDao2;
import br.com.meldica.model.entity.dominio.CargoEntity;
import br.com.meldica.utils.DataSourceUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CargoDaoTest {
	
	@Autowired private CargoDao2 dao;
	
	@BeforeClass
	public static void setUpDataSource() {
		DataSourceUtils.getDataSourceJNDI();
	}

	@Ignore
	@Test
	public void obter() {
		CargoEntity entity = dao.obter(1);
		
		assertFalse(entity == null);
		// ou
//		assertThat(list, is(list.size() > 0));
	}
	
	@Ignore
	@Test
	public void listar() {
		List<CargoEntity> list = dao.listar();
		
		assertFalse(list.isEmpty());
		// ou
//		assertThat(list, is(list.size() > 0));
	}

}
