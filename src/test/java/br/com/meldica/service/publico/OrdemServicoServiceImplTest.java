package br.com.meldica.service.publico;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.meldica.model.dto.relatorios.RelatorioResumoDto;
import br.com.meldica.model.entity.publico.OrdemServicoEntity;
import br.com.meldica.service.relatorio.RelatorioOrdemServicoService;
import br.com.meldica.utils.DataSourceUtils;
import br.com.meldica.utils.DateUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdemServicoServiceImplTest {
	
	@Autowired private OrdemServicoService service;
	@Autowired private RelatorioOrdemServicoService relatorioOrdemServicoService;
	
	@BeforeClass
	public static void setUpDataSource() {
		DataSourceUtils.getDataSourceJNDI();
	}
	
	@Ignore
	@Test
	public void listar() {
		OrdemServicoEntity ordemServico = new OrdemServicoEntity();
		ordemServico.setPeriodoVenda(new ArrayList<Date>());
		
		Date dtInicio = null;
		Date dtFim = null;
		
		try {
			dtInicio = DateUtils.stringToDate("01/02/2021", false);
			dtFim = DateUtils.stringToDate("28/02/2021", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ordemServico.getPeriodoVenda().add(dtInicio);
		ordemServico.getPeriodoVenda().add(dtFim);
		
		List<OrdemServicoEntity> list = service.listar(ordemServico);
		
		assertFalse(list.isEmpty());
		// ou
//		assertThat(list, is(list.size() > 0));
	}

//	@Ignore
	@Test
	public void relatorioResumoMensal() {
		OrdemServicoEntity ordemServico = new OrdemServicoEntity();
		ordemServico.setPeriodoVenda(new ArrayList<Date>());
		
		Date dtInicio = null;
		Date dtFim = null;
		
		try {
			dtInicio = DateUtils.stringToDate("01/03/2021", false);
			dtFim = DateUtils.stringToDate("01/03/2021", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ordemServico.getPeriodoVenda().add(dtInicio);
		ordemServico.getPeriodoVenda().add(dtFim);
		
		List<RelatorioResumoDto> list = relatorioOrdemServicoService.relatorioResumoMensal(ordemServico);
		
		assertFalse(list.isEmpty());
		// ou
//		assertThat(list, is(list.size() > 0));
	}

}
