package br.com.meldica.service.dominio;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.meldica.model.entity.dominio.CargoEntity;
import br.com.meldica.utils.DataSourceUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CargoServiceImplTest {
	
	@Autowired private CargoServiceImpl service;
	
	@BeforeClass
	public static void setUpDataSource() {
		DataSourceUtils.getDataSourceJNDI();
	}

//	@Ignore
	@Test
	public void listar() {
		List<CargoEntity> list = service.listar();
		
		assertFalse(list.isEmpty());
		// ou
//		assertThat(list, is(list.size() > 0));
	}

}
